import { Component } from '@stencil/core';

@Component({
  tag: 'app-header',
  styleUrl: 'app-header.scss'
})
export class AppHeader {

  render() {
    return <ion-header>
            <ion-toolbar color='primary'>
              <ion-title>
                <ion-icon name="planet" class="logo-icon" size="lg"/>
                LocoHome
              </ion-title>
            </ion-toolbar>
          </ion-header>;
  }
}
