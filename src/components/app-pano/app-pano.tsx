import { Component, Prop } from '@stencil/core';
import { Apartment } from '../../classes/Apartment';

const PANOLENS = window['PANOLENS'];

@Component({
  tag: 'app-pano',
})
export class AppPano {

  @Prop() id: string;
  public apartment:Apartment;
  public currentApartmentInd:number = -1;

  handler () {
      console.log(this);
    this.loadPano();
  }

  loadPano () {
    var elem = document.querySelector('.panolens-container');
    if (elem) elem.remove();
    this.currentApartmentInd = (this.currentApartmentInd + 1) % this.apartment.panos.length;
    var panorama, viewer;
    
    panorama = new PANOLENS.ImagePanorama( '/assets/' + this.apartment.panos[this.currentApartmentInd] );

    viewer = new PANOLENS.Viewer({ controlBar: false });
    viewer.control = viewer.DeviceOrientationControls;
    viewer.DeviceOrientationControls.enabled = true;
    viewer.add( panorama );
  }

  componentWillLoad() {
    this.apartment = Apartment.get(this.id);
    if (!window['hb']) return location.href = `/apartment/${this.id}`;
    window['hb']['handler'] = () => this.handler();
    
    PANOLENS['Controls']['ORBIT'] = PANOLENS['Controls']['ORBIT'];
    PANOLENS['Modes']['NORMAL'] = PANOLENS['Modes']['CARDBOARD'];
    
    this.loadPano();
  }



  render() {
    return <div class="loader-wrapper">
        <div class="loader"></div>
    </div>;
  }
}
