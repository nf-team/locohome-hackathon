import { Component, State } from '@stencil/core';
import { APARTMENTS, Apartment } from '../../classes/Apartment';

function commas(n) {
  if (n < 1000) {
      return String(n);
  } else {
      let whole:any = Math.floor(n);
      let fraction:any = n - whole;
      whole = String(whole).split('');
      let i = whole.length;
      while (i > 3) whole.splice((i -= 3), 0, ' ');
      return whole.join('') + String(fraction).slice(1);
  }
}

@Component({
  tag: 'app-favs',
  styleUrl: 'app-favs.scss'
})
export class AppFavs {
  @State() favCounter:number = 0;
  openApartment (id:number) {
    window['router'].push('/apartment/'+id);
  }

  favorites () {
    window['onFavsChange'] = () => { this.favCounter++; };
    return localStorage.getItem('favs') ? JSON.parse(localStorage.getItem('favs')) : [];
  }

  renderCard (ap:Apartment) {
    return <ion-card onClick={() => {this.openApartment(ap.id)}}>
      <ion-header>
        <img src={ap.images[0]} alt={ap.name}/>
      </ion-header>
      <ion-card-content>
      <ion-card-subtitle>
        {ap.name}
      </ion-card-subtitle>
      <ion-card-subtitle>
        {commas(ap.price)} RUB
      </ion-card-subtitle>
      <p>{ap.description.slice(0, 100)}...</p>
      </ion-card-content>
    </ion-card>;
  }

  getFavs () {
    return APARTMENTS.filter(x => { return this.favorites().indexOf(x.id.toString()) > -1 });
  }

  renderCards () {
    return this.getFavs().map(x => this.renderCard(x));
  }

  render() {
    return [
      <ion-tabs>
        <ion-tab href="/" active={true} selected={true} label="Search" icon="search" >
          <app-header/>

          <ion-content>
            { this.favorites().length == 0 && <h2 class="empty-list"><div>You have no favorites yet :(</div></h2> }
            { this.renderCards() }
            
          </ion-content>
        </ion-tab>
        <ion-tab href="/favs" label="Favorites" icon="star">
        </ion-tab>
        <ion-tab href="/settings" label="Settings" icon="settings">
        </ion-tab>
      </ion-tabs>
      
      
      
    ];
  }
}
