import { Component } from '@stencil/core';
import { Modal } from '@ionic/core';
// import { AppCardModal } from './app-card-modal'

@Component({
  tag: 'app-card-modal'
  // styleUrl: 'app-checkout.scss'
})



export class AppCardModal {
  public modal:Modal;

  // close() {
  //   console.log('Close clicked!', this);
  // }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-buttons slot="start">
            <ion-button class="dismiss">Cancel</ion-button>
          </ion-buttons>

          <ion-title>Pay with Credit Card</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        <p class='intro'>
          You need to enter your phone number here to use our service
        </p>
      </ion-content>
    ];
  }
}
