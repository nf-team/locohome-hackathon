import { Component } from '@stencil/core';
import QR from '../../classes/QR';

@Component({
  tag: 'qr-scanner',
  styleUrl: 'qr-scanner.scss'
})

export class QRScanner {

  qrReader:QR;
  elemId:string = Math.floor(Math.random()*1000*1000*1000).toString(32);
  element:HTMLElement;

  setComponentElement () {
    this.element = document.getElementById(this.elemId).parentElement;
  }

  async componentDidLoad () {
    this.setComponentElement();
    console.log(this.element);
    this.qrReader = new QR({
      video: this.element.querySelector('video'),
      canvas: document.createElement('canvas')
    });
    await this.qrReader.init();

    let processResult = async (data) => {
      console.log(data)

      // if (data.indexOf('https://skyqr.com') != 0) {
      //   console.warn('Wrong code')
      //   this.qr.scan(processResult);
      //   return;
      // }
      // this.$router.push(data.replace("https://skyqr.com", ""));
    };

    this.qrReader.scan(processResult);

  }

  componentDidUnload () {
    console.log('unload')
    this.qrReader.destroy();
  }



  render () {
    return <div id={this.elemId}>
      <video playsinline autoplay></video>
      <div class='overflow'>
        <img src='/assets/qr_code.png'/>
        <div class='text'>Scan QR code</div>
      </div>
    </div>;
  }
}