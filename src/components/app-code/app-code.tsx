import { Component, State } from '@stencil/core';
import vanillaTextMask from 'vanilla-text-mask';



@Component({
  tag: 'app-code'
})
export class AppCode {
  @State() loading:boolean = false;
  @State() phone:string = '';

  onSubmit() {
    console.log(this)
    this.loading = true;
  }

  handleInput(e) {
    console.log(vanillaTextMask)
    console.log(this, arguments)
    this.phone = e.target.value;
  }

  attachMask() {
    console.log(this);
    // var phoneMask:any[] = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
   
    // vanillaTextMask({
    //   inputElement: this,
    //   mask: phoneMask
    // });
    
    // // Calling `vanillaTextMask.maskInput` adds event listeners to the input element. 
    // // If you need to remove those event listeners, you can call
    // maskedInputController.destroy()
  }

  componentDidLoad() {
    this.attachMask();
  }

  render() {
    const buttonText = this.loading ? <ion-spinner name="crescent" color='light'></ion-spinner> : 'Next';

    return [
      <ion-header>
        <ion-toolbar color='primary'>
          {/*<ion-buttons slot="start">
            <ion-back-button defaultHref='/'></ion-back-button>
          </ion-buttons>*/}

          <ion-title>SMS Verification</ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content padding>
        <p class='intro'>
          You need to enter your phone number here to use our service
        </p>
        <ion-list>
          <ion-item>
            <ion-input type="tel" value={this.phone} onInput={this.handleInput.bind(this)} onLoad={this.attachMask} placeholder="+X (XXX) XXX-XX-XX"></ion-input>
          </ion-item>
        </ion-list>
        <ion-icon name="qr-scanner"></ion-icon>
        <ion-button expand='block' onClick={this.onSubmit.bind(this)} disabled={this.loading}>
          {buttonText}
        </ion-button>
        
        

          {/*<div padding>
            <button ion-button>Sign In</button>
          </div>*/}
      </ion-content>
    ];
  }
}
