import { Component, Prop, Listen } from '@stencil/core';
import { Token } from '../../services/AuthService';
import CONFIG from '../../config';

(async () => {
  // await Token.get();
  console.log(await Token.axios.get(`${CONFIG.apiUrl}/users/users`));
})();

@Component({
  tag: 'my-app',
  styleUrl: 'my-app.scss'
})

export class MyApp {

  @Prop({ connect: 'ion-toast-controller' }) toastCtrl: HTMLIonToastControllerElement;

  /**
   * Handle service worker updates correctly.
   * This code will show a toast letting the
   * user of the PWA know that there is a
   * new version available. When they click the
   * reload button it then reloads the page
   * so that the new service worker can take over
   * and serve the fresh content
   */
  @Listen('window:swUpdate')
  async onSWUpdate() {
    const toast = await this.toastCtrl.create({
      message: 'New version available',
      showCloseButton: true,
      closeButtonText: 'Reload'
    });
    await toast.present();
    await toast.onWillDismiss()
    window.location.reload();
  }

  async init() {
    // const toast = await this.toastCtrl.create({
    //   message: 'New version available',
    //   showCloseButton: true,
    //   closeButtonText: 'Reload'
    // });
    // await toast.present();
    // await toast.onWillDismiss();
    // window.location.reload();
  }

  componentWillLoad() {
    this.init();
  }

  render() {
    return (
      <ion-app>
        <ion-router onIonRouteChanged={function () { window['router'] = this; }} useHash={false}>
          <ion-route url='/' component='app-home'></ion-route>
          <ion-route url='/favs' component='app-favs'></ion-route>
          <ion-route url='/profile/:name' component='app-profile'></ion-route>
          <ion-route url='/apartment/:id' component='app-details'></ion-route>
          <ion-route url='/panos/:id' component='app-pano'></ion-route>
        </ion-router>
        <ion-nav></ion-nav>
      </ion-app>
    );
  }
}

