import { Component, Prop, State } from '@stencil/core';

import { Apartment } from '../../classes/Apartment';

import { HeadphonesButton } from '../../classes/HeadphonesButton';

const google = window['google'];

function commas(n) {
    if (n < 1000) {
        return String(n);
    } else {
        let whole:any = Math.floor(n);
        let fraction:any = n - whole;
        whole = String(whole).split('');
        let i = whole.length;
        while (i > 3) whole.splice((i -= 3), 0, ' ');
        return whole.join('') + String(fraction).slice(1);
    }
}

function initMap(coords) {
    var uluru = {lat: coords.latitude, lng: coords.longitude};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 17,
      center: uluru,
      scrollwheel: false,
      navigationControl: false,
      mapTypeControl: false,
      scaleControl: false,
      draggable: false,
    });
    new google.maps.Marker({
      position: uluru,
      map: map
    });
  }

@Component({
  tag: 'app-details',
  styleUrl: 'app-details.scss'
})
export class AppDetails {
  private apartment:Apartment;
  @Prop() id:string;
  @State() favorites:any[];
  hb:HeadphonesButton;

  getApartment () {
    if(this.apartment) return this.apartment;
    

    if (!this.hb) {
        // window['hb'] = this.hb;
        this.hb = new HeadphonesButton();
        this.hb.init();
    }

    return this.apartment = Apartment.get(this.id);
  }
  
  play() {
      this.hb.play();
  }

  inFav() {
      return this.favorites.indexOf(this.id.toString()) > -1;
  }

  addToFav() {
      if (this.inFav()) return;
      this.favorites = [this.id.toString(), ...this.favorites];
      this.saveFavs();
  }

  removeFromFav() {
      if (!this.inFav()) return;
      this.favorites.splice(this.favorites.indexOf(this.id.toString()), 1);
      this.favorites = [...this.favorites];
      this.saveFavs();
  }

  saveFavs() {
      if(window['onFavsChange']) window['onFavsChange']();
      localStorage.setItem('favs', JSON.stringify(this.favorites));
  }

  componentWillLoad() {
      this.favorites = localStorage.getItem('favs') ? JSON.parse(localStorage.getItem('favs')) : [];
  }

  componentDidLoad() {
      initMap(this.getApartment().coords);
  }

  render() {
    return [
      <ion-tabs>
        <ion-tab href="/" active={true} selected={true} label="Search" icon="search" >
          <app-header/>

          <ion-content class="details-content">
            <ion-slides>

            { this.getApartment().images.map(x => (
                <ion-slide class="slider-image">
                    <img src={x} alt={this.getApartment().name}/>
                </ion-slide>
            )) }
                    


            </ion-slides>
            <ion-button onClick={() => { this.play() }} href={'/panos/' + this.getApartment().id} expand="full">Посмотреть</ion-button>
            {this.inFav()}
            {!this.inFav() && <ion-button onClick={() => { this.addToFav() }} expand="full" color="secondary">Добавить в избранное</ion-button>}
            {this.inFav() && <ion-button onClick={() => { this.removeFromFav() }} expand="full" color="secondary">Убрать из избранного</ion-button>}
            
            
            <ion-card>
            <ion-card-content>
            <ion-card-subtitle>
                <h1>{ commas(this.getApartment().price) } RUB</h1>
                <h4>{ this.getApartment().name }</h4>
            </ion-card-subtitle>
            </ion-card-content>
            </ion-card>
            <ion-card>
            <ion-card-content>
            <ion-card-subtitle>
                <h2 class="mb">
                    <b>Общая информация</b>
                </h2>
                <h4>
                    Комнат
                    <div class="pull-right">
                        <b>{this.getApartment().meta.rooms}</b>
                    </div>
                </h4>
                <h4>
                    Этаж
                    <div class="pull-right">
                        <b>{this.getApartment().meta.floor}</b>
                    </div>
                </h4>
                <h4>
                    Площадь
                    <div class="pull-right">
                        <b>{this.getApartment().meta.area} м<sup>2</sup></b>
                    </div>
                </h4>
            </ion-card-subtitle>
            </ion-card-content>
            </ion-card>

            <ion-card class="last-card">
                    <div id="map"></div>
            </ion-card>
            
          </ion-content>
        </ion-tab>
        <ion-tab href="/favs" label="Favorites" icon="star">
        </ion-tab>
        <ion-tab href="/settings" label="Settings" icon="settings">
        </ion-tab>
      </ion-tabs>      
    ];
  }
}
