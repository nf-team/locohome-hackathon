import { Component, Prop, State } from '@stencil/core';
import { ModalController, LoadingController, AlertController,  } from '@ionic/core';
import { isIOS, isAndroid } from '@ionic/core/dist/collection/utils/platform';
import { Token } from '../../services/AuthService';
import CONFIG from '../../config';
// import { AppCardModal } from '../app-card-modal/app-card-modal';

const Stripe = window['Stripe'];

@Component({
  tag: 'app-checkout',
  styleUrl: 'app-checkout.scss'
})

export class AppCheckout {
  @Prop({ connect: 'ion-modal-controller' }) modalCtrl: ModalController;
  @Prop({ connect: 'ion-loading-controller' }) loadingCtrl: LoadingController;
  @Prop({ connect: 'ion-alert-controller' }) alertCtrl: AlertController;
  @Prop() id:string;
  @State() transactionLoading:boolean = true;
  @State() transaction:any;
  @State() product:any = {
    id: 0,
    name: 'Snickers'
  };
  @State() canMakePayment = false;

  paymentRequest:any;

  products:any[] = [
    {
      id: 0,
      name: 'Snickers',
      price: 54522,
      quantity: 1,
      image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
    },
    {
      id: 1,
      name: 'Mars',
      price: 23233,
      quantity: 3,
      image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
    },
    {
      id: 2,
      name: 'Alpen Gold',
      price: 12322,
      quantity: 5,
      image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
    },
    {
      id: 0,
      name: 'Snickers',
      price: 54522,
      quantity: 1,
      image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
    },
    {
      id: 1,
      name: 'Mars',
      price: 23233,
      quantity: 3,
      image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
    },
    {
      id: 2,
      name: 'Alpen Gold',
      price: 12322,
      quantity: 5,
      image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
    },
    {
      id: 0,
      name: 'Snickers',
      price: 54522,
      quantity: 1,
      image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
    },
    {
      id: 1,
      name: 'Mars',
      price: 23233,
      quantity: 3,
      image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
    },
    {
      id: 2,
      name: 'Alpen Gold',
      price: 12322,
      quantity: 5,
      image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
    },
    {
      id: 0,
      name: 'Snickers',
      price: 54522,
      quantity: 1,
      image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
    },
    {
      id: 1,
      name: 'Mars',
      price: 23233,
      quantity: 3,
      image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
    },
    {
      id: 2,
      name: 'Alpen Gold',
      price: 12322,
      quantity: 5,
      image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
    },
    {
      id: 0,
      name: 'Snickers',
      price: 54522,
      quantity: 1,
      image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
    },
    {
      id: 1,
      name: 'Mars',
      price: 23233,
      quantity: 3,
      image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
    },
    {
      id: 2,
      name: 'Alpen Gold',
      price: 12322,
      quantity: 5,
      image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
    },
    {
      id: 0,
      name: 'Snickers',
      price: 54522,
      quantity: 1,
      image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
    },
    {
      id: 1,
      name: 'Mars',
      price: 23233,
      quantity: 3,
      image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
    },
    {
      id: 2,
      name: 'Alpen Gold',
      price: 12322,
      quantity: 5,
      image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
    },
    {
      id: 0,
      name: 'Snickers',
      price: 54522,
      quantity: 1,
      image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
    },
    {
      id: 1,
      name: 'Mars',
      price: 23233,
      quantity: 3,
      image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
    },
    {
      id: 2,
      name: 'Alpen Gold',
      price: 12322,
      quantity: 5,
      image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
    },
  ];

  handleSelect(e) {
    for (let product of this.products) {
      if (product.id == e.detail.value) {
        this.product = product;
        console.log(product);
        return;
      }
    }
  }

  async patWithCard() {
    let element = document.createElement('app-card-modal');
    let modal:any = await this.modalCtrl.create({
      component: element,
      cssClass: 'on-top'
    });
    console.log(element);
    await modal.present();
    element.querySelector('.dismiss').addEventListener('click', () => {
      modal.dismiss();
    });
  }

  renderProducts() {
    const ret:any[] = [];
    for (let product of this.transaction.products) {
      ret.push(
        <ion-item class="product">
          { product.img && false && <ion-thumbnail slot="start">
            <img src={product.img}/>
          </ion-thumbnail> }
          <ion-label>{product.name}</ion-label>
          <ion-label slot='end' class='text-right price'>
            {product.quantity} x
            ${(product.price/100).toFixed(2)}
          </ion-label>
          {/*<ion-radio value={product.id}></ion-radio>*/}
        </ion-item>

      );
      // ret.push(<ion-item-divider color="light"></ion-item-divider>);
    }
    return ret;
  }

  getTotalAmount() {
    return this.transaction.amount;
  }

  async componentDidLoad() {
    let loading = await this.loadingCtrl.create({
      spinner: 'crescent',
      duration: 5000
    });
    await loading.present();
    try {
      await Token.axios.put(`${CONFIG.apiUrl}/users/transactions/${this.id}/reserve`);
    } catch (err) {
      console.warn(err);
    }
    try {
      let res = await Token.axios.get(`${CONFIG.apiUrl}/users/transactions/${this.id}`);
      let transaction = res.data.transaction;
      transaction.products = transaction.product_lists.map((x) => ({
        quantity: parseInt(x.quantity),
        ...x.product
      }));
      console.log(transaction);
      this.transaction = transaction;
      await loading.dismiss();
      this.transactionLoading = false;
      const stripe = Stripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh');
      this.paymentRequest = stripe.paymentRequest({
        country: 'US',
        currency: 'usd',
        total: {
          label: 'Total',
          amount: this.transaction.amount,
        },
      });
      this.canMakePayment = await this.paymentRequest.canMakePayment();


      
    } catch (err) {
      await Promise.all([ loading.dismiss(), this.error(err, true) ]);
    }

    
  }

  async error (err, fatal:boolean = false) {
    let buttons = [];
    if (!fatal) buttons.push('OK');
    if (err.response && err.response.data && typeof err.response.data.error == 'string')
      err.message = err.response.data.error;
    let alert = await this.alertCtrl.create({
      header: 'Error occured',
      message: err.message,
      buttons: buttons,
      enableBackdropDismiss: !fatal
    });
    return await alert.present();
  }

  pay () {
    if (this.canMakePayment)
      this.paymentRequest.show();
  }

  render() {
    if (this.transactionLoading) return [];
    return [
      <ion-header>
        <ion-toolbar color='primary'>
          {/*<ion-buttons slot="start">
            <ion-back-button defaultHref='/'></ion-back-button>
          </ion-buttons>*/}

          <ion-title>
            {  `Checkout #${this.id}` }
          </ion-title>
        </ion-toolbar>
      </ion-header>,
      <ion-content>
        <div class='fill-all'>
          <div>
            <ion-list no-lines>
              <ion-list-header>
                Seller info
              </ion-list-header>
              <ion-item class='seller-info'>
                {this.transaction.cashbox.name}
                <br/>
                {this.transaction.cashbox.location}
                {/*<br/>
                +7 (917) 293-63-45*/}
              </ion-item>

              {/*<ion-radio-group value={this.product.id} onIonChange={this.handleSelect.bind(this)}>*/}

                <ion-list-header>
                  Products
                </ion-list-header>

                { this.renderProducts() }

              {/*</ion-radio-group>*/}
              

            </ion-list>
          </div>
          {/*<div id="payment-request-button"></div>*/}
          <div class='padding'>
            <ion-item class="total">
              <ion-label>
                <h2>
                  Total
                </h2>
              </ion-label>
              <ion-label class="amount text-right" slot='end'>${(this.getTotalAmount()/100).toFixed(2)}</ion-label>
              {/*<ion-radio value={product.id}></ion-radio>*/}
            </ion-item>
            { this.canMakePayment && isIOS(window) && <ion-button onClick={async () => {this.pay();}} expand='block' color='dark'>
              <ion-icon class="icon-pr" name="logo-apple"></ion-icon>
              Pay with Apple Pay
            </ion-button> }
            
            { this.canMakePayment && isAndroid(window) && <ion-button onClick={async () => {this.pay();}} expand='block' color='primary'>
              <ion-icon class="icon-pr" name="logo-google"></ion-icon>
              Pay with Google
            </ion-button> }
            <ion-button onClick={this.patWithCard.bind(this)} expand='block' size='default' fill='clear'>
              <ion-icon class="icon-pr" name="card"></ion-icon>
              Pay with Credit Card
            </ion-button>
          </div>
        </div>
      </ion-content>
    ];
  }
}
