interface QRElements {
  canvas:HTMLCanvasElement;
  video:HTMLVideoElement;
}

export default class QRReader {

  public captureConstraints:any = {
    video: {
      facingMode: { exact: 'environment' },
      mandatory: {
        width: 320,
        height: 240
      }
    },
    audio: false
  };
  public baseurl:string = '/';
  public workerFilename:string = 'decoder.min.js';

  protected active:boolean = false;
  protected webcam:HTMLVideoElement;
  protected canvas:HTMLCanvasElement;
  protected ctx:CanvasRenderingContext2D;
  protected decoder:Worker;
  protected streaming:boolean = false;


  constructor (elements:QRElements) {
    this.canvas = elements.canvas;
    this.webcam = elements.video;
  }

  protected setCanvas () {
    this.ctx = this.canvas.getContext('2d');
  }

  protected setCanvasProperties () {
    this.canvas.width = window.innerWidth;
    this.canvas.height = window.innerHeight;
  }

  public async startCapture (constraints = this.captureConstraints) {
    let stream = await navigator.mediaDevices.getUserMedia(constraints);
    this.webcam.srcObject = stream;
    this.webcam.play();
  }

  public async init () {
    this.decoder = new Worker(this.baseurl + this.workerFilename);
    this.setCanvas();
    this.setCanvasProperties();
    this.webcam.addEventListener('play', () => {
      if (!this.streaming) {
        this.setCanvasProperties();
        this.streaming = true;
      }
    }, false);

    await this.startCapture();
  }

  public destroy () {
    this.decoder.terminate();
    this.active = false;
    this.webcam.pause();
  }

  public scan (callback:Function) {
    this.active = true;
    let onDecoderMessage = async (e) => {
      if (e.data.length > 0) {
        let qrid = e.data[0][2];
        await callback(qrid);
      }
      setTimeout(newDecoderFrame, 0);
    };

    this.decoder.onmessage = onDecoderMessage;


    let newDecoderFrame = () => {
      if (!this.active) return;
      try {
        this.ctx.drawImage(this.webcam, 0, 0, this.canvas.width, this.canvas.height);
        let imgData = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);

        if (imgData.data) {
          this.decoder.postMessage(imgData);
        }
      } catch (err) {
        if (err.name == 'NS_ERROR_NOT_AVAILABLE') setTimeout(newDecoderFrame, 0);
      }
    };

    newDecoderFrame();
  }

}