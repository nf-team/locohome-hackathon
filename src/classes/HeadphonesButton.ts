export class HeadphonesButton {
    private audio:HTMLAudioElement;
    private played:boolean;

    handler() {
        
    }

    public init () {
        this.audio = new Audio('/assets/audio.mp3');
        // document.body.appendChild(this.audio);
        
        this.audio.controls = true;
        this.audio.autoplay = true;
        this.audio.loop = true;
        window['hb'] = this;
        this.audio.onplay = () => {
            if (this.played) this.handler();
            this.played = true;
            
        };
        this.audio.onpause = () => {
            if (this.played) this.handler();
            this.played = true;
            
        };
    }

    public play() {
        this.audio.play();
    }
}