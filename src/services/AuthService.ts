import axios from 'axios';
import CONFIG from '../config';

export class Token {
  public static axios = axios.create();
  public static STORAGE_KEY = 'jwt';
  public static init () {
    this.axios.interceptors.request.use(async (config) => {
      console.log(config);
      if (config['noAuth']) return config;
      let token = await this.get();
      config.headers.authorization = `Bearer ${token}`;
      return config;
    }, async (error) => {
      throw error;
    });
  }

  public static async get ():Promise<string> {
    let token = this.getFromStorage();
    if (token) return token;
    // let res = await axios.get('');
    // let rConfig = {};
    // rConfig['noAuth'] = true;

    let res = await axios.post(`${CONFIG.apiUrl}/users/one_time_token`);
    token = res.data.jwt;
    this.save(token);
    return token;
  }

  public static getFromStorage () {
    return JSON.parse(localStorage.getItem(this.STORAGE_KEY) + '');
  }

  public static save (token) {
    localStorage.setItem(this.STORAGE_KEY, JSON.stringify(token));
  }
}

Token.init();