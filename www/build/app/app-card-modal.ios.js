/*! Built with http://stenciljs.com */
const { h } = window.App;

import { a as isIOS, b as isAndroid, c as Token, d as CONFIG } from './chunk-3d470e50.js';
import { f as createOverlay, g as dismissOverlay, h as getTopOverlay, i as removeLastOverlay } from './chunk-3b0e1c7b.js';
import { a as debounceEvent, b as deferEvent } from './chunk-63df273d.js';
import { a as createThemedClasses, c as getElementClassMap, d as openURL } from './chunk-e901a817.js';

// import { AppCardModal } from './app-card-modal'
class AppCardModal {
    // close() {
    //   console.log('Close clicked!', this);
    // }
    render() {
        return [
            h("ion-header", null,
                h("ion-toolbar", null,
                    h("ion-buttons", { slot: "start" },
                        h("ion-button", { class: "dismiss" }, "Cancel")),
                    h("ion-title", null, "Pay with Credit Card"))),
            h("ion-content", null,
                h("p", { class: 'intro' }, "You need to enter your phone number here to use our service"))
        ];
    }
    static get is() { return "app-card-modal"; }
}

// import { AppCardModal } from '../app-card-modal/app-card-modal';
const Stripe = window['Stripe'];
class AppCheckout {
    constructor() {
        this.transactionLoading = true;
        this.product = {
            id: 0,
            name: 'Snickers'
        };
        this.canMakePayment = false;
        this.products = [
            {
                id: 0,
                name: 'Snickers',
                price: 54522,
                quantity: 1,
                image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
            },
            {
                id: 1,
                name: 'Mars',
                price: 23233,
                quantity: 3,
                image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
            },
            {
                id: 2,
                name: 'Alpen Gold',
                price: 12322,
                quantity: 5,
                image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
            },
            {
                id: 0,
                name: 'Snickers',
                price: 54522,
                quantity: 1,
                image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
            },
            {
                id: 1,
                name: 'Mars',
                price: 23233,
                quantity: 3,
                image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
            },
            {
                id: 2,
                name: 'Alpen Gold',
                price: 12322,
                quantity: 5,
                image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
            },
            {
                id: 0,
                name: 'Snickers',
                price: 54522,
                quantity: 1,
                image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
            },
            {
                id: 1,
                name: 'Mars',
                price: 23233,
                quantity: 3,
                image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
            },
            {
                id: 2,
                name: 'Alpen Gold',
                price: 12322,
                quantity: 5,
                image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
            },
            {
                id: 0,
                name: 'Snickers',
                price: 54522,
                quantity: 1,
                image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
            },
            {
                id: 1,
                name: 'Mars',
                price: 23233,
                quantity: 3,
                image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
            },
            {
                id: 2,
                name: 'Alpen Gold',
                price: 12322,
                quantity: 5,
                image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
            },
            {
                id: 0,
                name: 'Snickers',
                price: 54522,
                quantity: 1,
                image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
            },
            {
                id: 1,
                name: 'Mars',
                price: 23233,
                quantity: 3,
                image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
            },
            {
                id: 2,
                name: 'Alpen Gold',
                price: 12322,
                quantity: 5,
                image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
            },
            {
                id: 0,
                name: 'Snickers',
                price: 54522,
                quantity: 1,
                image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
            },
            {
                id: 1,
                name: 'Mars',
                price: 23233,
                quantity: 3,
                image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
            },
            {
                id: 2,
                name: 'Alpen Gold',
                price: 12322,
                quantity: 5,
                image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
            },
            {
                id: 0,
                name: 'Snickers',
                price: 54522,
                quantity: 1,
                image: 'https://cdn0.woolworths.media/content/wowproductimages/large/785455.jpg'
            },
            {
                id: 1,
                name: 'Mars',
                price: 23233,
                quantity: 3,
                image: 'https://5.imimg.com/data5/DH/AP/MY-41081178/mars-chocolate-bar-500x500.jpg'
            },
            {
                id: 2,
                name: 'Alpen Gold',
                price: 12322,
                quantity: 5,
                image: 'https://www.barista-ltd.ru/components/com_jshopping/files/img_products/full_shokolad-alpen_gold-funduk-90g.jpg'
            },
        ];
    }
    handleSelect(e) {
        for (let product of this.products) {
            if (product.id == e.detail.value) {
                this.product = product;
                console.log(product);
                return;
            }
        }
    }
    async patWithCard() {
        let element = document.createElement('app-card-modal');
        let modal = await this.modalCtrl.create({
            component: element,
            cssClass: 'on-top'
        });
        console.log(element);
        await modal.present();
        element.querySelector('.dismiss').addEventListener('click', () => {
            modal.dismiss();
        });
    }
    renderProducts() {
        const ret = [];
        for (let product of this.transaction.products) {
            ret.push(h("ion-item", { class: "product" },
                product.img && false && h("ion-thumbnail", { slot: "start" },
                    h("img", { src: product.img })),
                h("ion-label", null, product.name),
                h("ion-label", { slot: 'end', class: 'text-right price' },
                    product.quantity,
                    " x $",
                    (product.price / 100).toFixed(2))));
            // ret.push(<ion-item-divider color="light"></ion-item-divider>);
        }
        return ret;
    }
    getTotalAmount() {
        return this.transaction.amount;
    }
    async componentDidLoad() {
        let loading = await this.loadingCtrl.create({
            spinner: 'crescent',
            duration: 5000
        });
        await loading.present();
        try {
            await Token.axios.put(`${CONFIG.apiUrl}/users/transactions/${this.id}/reserve`);
        }
        catch (err) {
            console.warn(err);
        }
        try {
            let res = await Token.axios.get(`${CONFIG.apiUrl}/users/transactions/${this.id}`);
            let transaction = res.data.transaction;
            transaction.products = transaction.product_lists.map((x) => (Object.assign({ quantity: parseInt(x.quantity) }, x.product)));
            console.log(transaction);
            this.transaction = transaction;
            await loading.dismiss();
            this.transactionLoading = false;
            const stripe = Stripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh');
            this.paymentRequest = stripe.paymentRequest({
                country: 'US',
                currency: 'usd',
                total: {
                    label: 'Total',
                    amount: this.transaction.amount,
                },
            });
            this.canMakePayment = await this.paymentRequest.canMakePayment();
        }
        catch (err) {
            await Promise.all([loading.dismiss(), this.error(err, true)]);
        }
    }
    async error(err, fatal = false) {
        let buttons = [];
        if (!fatal)
            buttons.push('OK');
        if (err.response && err.response.data && typeof err.response.data.error == 'string')
            err.message = err.response.data.error;
        let alert = await this.alertCtrl.create({
            header: 'Error occured',
            message: err.message,
            buttons: buttons,
            enableBackdropDismiss: !fatal
        });
        return await alert.present();
    }
    pay() {
        if (this.canMakePayment)
            this.paymentRequest.show();
    }
    render() {
        if (this.transactionLoading)
            return [];
        return [
            h("ion-header", null,
                h("ion-toolbar", { color: 'primary' },
                    h("ion-title", null, `Checkout #${this.id}`))),
            h("ion-content", null,
                h("div", { class: 'fill-all' },
                    h("div", null,
                        h("ion-list", { "no-lines": true },
                            h("ion-list-header", null, "Seller info"),
                            h("ion-item", { class: 'seller-info' },
                                this.transaction.cashbox.name,
                                h("br", null),
                                this.transaction.cashbox.location),
                            h("ion-list-header", null, "Products"),
                            this.renderProducts())),
                    h("div", { class: 'padding' },
                        h("ion-item", { class: "total" },
                            h("ion-label", null,
                                h("h2", null, "Total")),
                            h("ion-label", { class: "amount text-right", slot: 'end' },
                                "$",
                                (this.getTotalAmount() / 100).toFixed(2))),
                        this.canMakePayment && isIOS(window) && h("ion-button", { onClick: async () => { this.pay(); }, expand: 'block', color: 'dark' },
                            h("ion-icon", { class: "icon-pr", name: "logo-apple" }),
                            "Pay with Apple Pay"),
                        this.canMakePayment && isAndroid(window) && h("ion-button", { onClick: async () => { this.pay(); }, expand: 'block', color: 'primary' },
                            h("ion-icon", { class: "icon-pr", name: "logo-google" }),
                            "Pay with Google"),
                        h("ion-button", { onClick: this.patWithCard.bind(this), expand: 'block', size: 'default', fill: 'clear' },
                            h("ion-icon", { class: "icon-pr", name: "card" }),
                            "Pay with Credit Card"))))
        ];
    }
    static get is() { return "app-checkout"; }
    static get properties() { return {
        "alertCtrl": {
            "connect": "ion-alert-controller"
        },
        "canMakePayment": {
            "state": true
        },
        "id": {
            "type": String,
            "attr": "id"
        },
        "loadingCtrl": {
            "connect": "ion-loading-controller"
        },
        "modalCtrl": {
            "connect": "ion-modal-controller"
        },
        "product": {
            "state": true
        },
        "transaction": {
            "state": true
        },
        "transactionLoading": {
            "state": true
        }
    }; }
    static get style() { return ".scroll-inner {\n  min-height: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n  -ms-flex-direction: column;\n  flex-direction: column; }\n\n.fill-all {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  min-height: 100%;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n  -ms-flex-direction: column;\n  flex-direction: column;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n  -ms-flex-pack: justify;\n  justify-content: space-between; }\n\n.padding {\n  padding: 1rem; }\n\n.icon-pr {\n  padding-right: .5rem; }\n\n.total {\n  text-transform: uppercase;\n  font-size: 1.2rem; }\n  .total .amount {\n    font-weight: 200;\n    font-size: 1.5rem; }\n  .total .item-inner {\n    border-bottom: 0; }\n\n.product {\n  border-bottom: 1px solid #ddd; }\n  .product:last-child {\n    border-bottom: 0; }\n  .product .price {\n    font-weight: 200;\n    font-size: 1.2rem; }\n\n.seller-info {\n  font-size: 0.8rem;\n  opacity: .8; }"; }
}

class AlertController {
    constructor() {
        this.alerts = new Map();
    }
    alertWillPresent(ev) {
        this.alerts.set(ev.target.overlayId, ev.target);
    }
    alertWillDismiss(ev) {
        this.alerts.delete(ev.target.overlayId);
    }
    escapeKeyUp() {
        removeLastOverlay(this.alerts);
    }
    /*
     * Create an alert overlay with alert options.
     */
    create(opts) {
        return createOverlay(this.doc.createElement('ion-alert'), opts);
    }
    /*
     * Dismiss the open alert overlay.
     */
    dismiss(data, role, alertId = -1) {
        return dismissOverlay(data, role, this.alerts, alertId);
    }
    /*
     * Get the most recently opened alert overlay.
     */
    getTop() {
        return getTopOverlay(this.alerts);
    }
    static get is() { return "ion-alert-controller"; }
    static get properties() { return {
        "create": {
            "method": true
        },
        "dismiss": {
            "method": true
        },
        "doc": {
            "context": "document"
        },
        "getTop": {
            "method": true
        }
    }; }
    static get listeners() { return [{
            "name": "body:ionAlertWillPresent",
            "method": "alertWillPresent"
        }, {
            "name": "body:ionAlertWillDismiss",
            "method": "alertWillDismiss"
        }, {
            "name": "body:ionAlertDidUnload",
            "method": "alertWillDismiss"
        }, {
            "name": "body:keyup.escape",
            "method": "escapeKeyUp"
        }]; }
}

class Input {
    constructor() {
        this.didBlurAfterEdit = false;
        /**
         * Indicates whether and how the text value should be automatically capitalized as it is entered/edited by the user. Defaults to `"none"`.
         */
        this.autocapitalize = 'none';
        /**
         * Indicates whether the value of the control can be automatically completed by the browser. Defaults to `"off"`.
         */
        this.autocomplete = 'off';
        /**
         * Whether autocorrection should be enabled when the user is entering/editing the text value. Defaults to `"off"`.
         */
        this.autocorrect = 'off';
        /**
         * This Boolean attribute lets you specify that a form control should have input focus when the page loads. Defaults to `false`.
         */
        this.autofocus = false;
        /**
         * If true, a clear icon will appear in the input when there is a value. Clicking it clears the input. Defaults to `false`.
         */
        this.clearInput = false;
        /**
         * Set the amount of time, in milliseconds, to wait to trigger the `ionChange` event after each keystroke. Default `0`.
         */
        this.debounce = 0;
        /**
         * If true, the user cannot interact with the input. Defaults to `false`.
         */
        this.disabled = false;
        /**
         * If true, the user cannot modify the value. Defaults to `false`.
         */
        this.readonly = false;
        /**
         * If true, the user must fill in a value before submitting a form.
         */
        this.required = false;
        /**
         * If true, the element will have its spelling and grammar checked. Defaults to `false`.
         */
        this.spellcheck = false;
        /**
         * The type of control to display. The default type is text. Possible values are: `"text"`, `"password"`, `"email"`, `"number"`, `"search"`, `"tel"`, or `"url"`.
         */
        this.type = 'text';
        /**
         * The value of the input.
         */
        this.value = '';
    }
    debounceChanged() {
        this.ionChange = debounceEvent(this.ionChange, this.debounce);
    }
    disabledChanged() {
        this.emitStyle();
    }
    /**
     * Update the native input element when the value changes
     */
    valueChanged() {
        const inputEl = this.nativeInput;
        const value = this.value;
        if (inputEl && inputEl.value !== value) {
            inputEl.value = value;
        }
        this.emitStyle();
        this.ionChange.emit({ value });
    }
    componentWillLoad() {
        // By default, password inputs clear after focus when they have content
        if (this.clearOnEdit === undefined && this.type === 'password') {
            this.clearOnEdit = true;
        }
    }
    componentDidLoad() {
        this.ionStyle = deferEvent(this.ionStyle);
        this.debounceChanged();
        this.emitStyle();
        this.ionInputDidLoad.emit();
    }
    componentDidUnload() {
        this.nativeInput = undefined;
        this.ionInputDidUnload.emit();
    }
    emitStyle() {
        this.ionStyle.emit({
            'input': true,
            'input-disabled': this.disabled,
            'input-has-value': this.hasValue(),
            'input-has-focus': this.hasFocus()
        });
    }
    onInput(ev) {
        const input = ev.target;
        if (input) {
            this.value = ev.target && ev.target.value || '';
        }
        this.ionInput.emit(ev);
    }
    onBlur() {
        this.focusChanged();
        this.emitStyle();
        this.ionBlur.emit();
    }
    onFocus() {
        this.focusChanged();
        this.emitStyle();
        this.ionFocus.emit();
    }
    focusChanged() {
        // If clearOnEdit is enabled and the input blurred but has a value, set a flag
        if (this.clearOnEdit && !this.hasFocus() && this.hasValue()) {
            this.didBlurAfterEdit = true;
        }
    }
    inputKeydown() {
        this.checkClearOnEdit();
    }
    /**
     * Check if we need to clear the text input if clearOnEdit is enabled
     */
    checkClearOnEdit() {
        if (!this.clearOnEdit) {
            return;
        }
        // Did the input value change after it was blurred and edited?
        if (this.didBlurAfterEdit && this.hasValue()) {
            // Clear the input
            this.clearTextInput();
        }
        // Reset the flag
        this.didBlurAfterEdit = false;
    }
    clearTextInput() {
        this.value = '';
    }
    hasFocus() {
        // check if an input has focus or not
        return this.nativeInput === document.activeElement;
    }
    hasValue() {
        return (this.value !== '');
    }
    render() {
        const themedClasses = createThemedClasses(this.mode, this.color, 'native-input');
        // TODO aria-labelledby={this.item.labelId}
        return [
            h("input", { ref: input => this.nativeInput = input, "aria-disabled": this.disabled ? 'true' : false, accept: this.accept, autoCapitalize: this.autocapitalize, autoComplete: this.autocomplete, autoCorrect: this.autocorrect, autoFocus: this.autofocus, class: themedClasses, disabled: this.disabled, inputMode: this.inputmode, min: this.min, max: this.max, minLength: this.minlength, maxLength: this.maxlength, multiple: this.multiple, name: this.name, pattern: this.pattern, placeholder: this.placeholder, results: this.results, readOnly: this.readonly, required: this.required, spellCheck: this.spellcheck, step: this.step, size: this.size, type: this.type, value: this.value, onInput: this.onInput.bind(this), onBlur: this.onBlur.bind(this), onFocus: this.onFocus.bind(this), onKeyDown: this.inputKeydown.bind(this) }),
            h("button", { type: "button", class: "input-clear-icon", hidden: !this.clearInput, onClick: this.clearTextInput.bind(this), onMouseDown: this.clearTextInput.bind(this) })
        ];
    }
    static get is() { return "ion-input"; }
    static get host() { return {
        "theme": "input"
    }; }
    static get properties() { return {
        "accept": {
            "type": String,
            "attr": "accept"
        },
        "autocapitalize": {
            "type": String,
            "attr": "autocapitalize"
        },
        "autocomplete": {
            "type": String,
            "attr": "autocomplete"
        },
        "autocorrect": {
            "type": String,
            "attr": "autocorrect"
        },
        "autofocus": {
            "type": Boolean,
            "attr": "autofocus"
        },
        "clearInput": {
            "type": Boolean,
            "attr": "clear-input"
        },
        "clearOnEdit": {
            "type": Boolean,
            "attr": "clear-on-edit",
            "mutable": true
        },
        "debounce": {
            "type": Number,
            "attr": "debounce",
            "watchCallbacks": ["debounceChanged"]
        },
        "disabled": {
            "type": Boolean,
            "attr": "disabled",
            "watchCallbacks": ["disabledChanged"]
        },
        "el": {
            "elementRef": true
        },
        "inputmode": {
            "type": String,
            "attr": "inputmode"
        },
        "max": {
            "type": String,
            "attr": "max"
        },
        "maxlength": {
            "type": Number,
            "attr": "maxlength"
        },
        "min": {
            "type": String,
            "attr": "min"
        },
        "minlength": {
            "type": Number,
            "attr": "minlength"
        },
        "multiple": {
            "type": Boolean,
            "attr": "multiple"
        },
        "name": {
            "type": String,
            "attr": "name"
        },
        "pattern": {
            "type": String,
            "attr": "pattern"
        },
        "placeholder": {
            "type": String,
            "attr": "placeholder"
        },
        "readonly": {
            "type": Boolean,
            "attr": "readonly"
        },
        "required": {
            "type": Boolean,
            "attr": "required"
        },
        "results": {
            "type": Number,
            "attr": "results"
        },
        "size": {
            "type": Number,
            "attr": "size"
        },
        "spellcheck": {
            "type": Boolean,
            "attr": "spellcheck"
        },
        "step": {
            "type": String,
            "attr": "step"
        },
        "type": {
            "type": String,
            "attr": "type"
        },
        "value": {
            "type": String,
            "attr": "value",
            "mutable": true,
            "watchCallbacks": ["valueChanged"]
        }
    }; }
    static get events() { return [{
            "name": "ionInput",
            "method": "ionInput",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionChange",
            "method": "ionChange",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionStyle",
            "method": "ionStyle",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionBlur",
            "method": "ionBlur",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionFocus",
            "method": "ionFocus",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionInputDidLoad",
            "method": "ionInputDidLoad",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionInputDidUnload",
            "method": "ionInputDidUnload",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return ".input {\n  position: relative;\n  display: block;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  width: 100%; }\n\n.item-input .input {\n  position: static; }\n\n.native-input {\n  -moz-appearance: none;\n  -ms-appearance: none;\n  -webkit-appearance: none;\n  appearance: none;\n  border-radius: 0;\n  display: inline-block;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  width: 92%;\n  width: calc(100% - 10px);\n  border: 0;\n  background: transparent; }\n  .native-input:active, .native-input:focus {\n    outline: none; }\n\n.native-input[disabled] {\n  opacity: .4; }\n\ninput.native-input:-webkit-autofill {\n  background-color: transparent; }\n\n.input-cover {\n  left: 0;\n  top: 0;\n  position: absolute;\n  width: 100%;\n  height: 100%; }\n\n.input[disabled] .input-cover {\n  pointer-events: none; }\n\n.item-input-has-focus .input-cover {\n  display: none; }\n\n.item-input-has-focus {\n  pointer-events: none; }\n\n.item-input-has-focus input,\n.item-input-has-focus a,\n.item-input-has-focus button {\n  pointer-events: auto; }\n\n[next-input] {\n  padding: 0;\n  position: absolute;\n  bottom: 20px;\n  width: 1px;\n  height: 1px;\n  border: 0;\n  background: transparent;\n  pointer-events: none; }\n\n.input-clear-icon {\n  margin: 0;\n  padding: 0;\n  background-position: center;\n  position: absolute;\n  top: 0;\n  display: none;\n  height: 100%;\n  background-repeat: no-repeat; }\n\n.item-input-has-focus.item-input-has-value .input-clear-icon {\n  display: block; }\n\n.native-input-ios {\n  margin: 11px 8px 11px 0;\n  padding: 0;\n  width: calc(100% - 8px);\n  font-family: -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", \"Roboto\", sans-serif;\n  font-size: inherit; }\n  .native-input-ios::-moz-placeholder {\n    color: var(--ion-placeholder-text-ios-color, var(--ion-placeholder-text-color, #999)); }\n  .native-input-ios:-ms-input-placeholder {\n    color: var(--ion-placeholder-text-ios-color, var(--ion-placeholder-text-color, #999)); }\n  .native-input-ios::-webkit-input-placeholder {\n    text-indent: 0;\n    color: var(--ion-placeholder-text-ios-color, var(--ion-placeholder-text-color, #999)); }\n\n.input-ios .inset-input {\n  padding: 5.5px 8px;\n  margin: 5.5px 16px 5.5px 0; }\n\n.label-ios + .input .native-input,\n.label-ios + .input + .cloned-input {\n  margin-left: 16px; }\n\n.item-ios.item-label-stacked .native-input,\n.item-ios.item-label-floating .native-input {\n  margin-left: 0;\n  margin-top: 8px;\n  margin-bottom: 8px;\n  width: calc(100% - 8px); }\n\n.item-ios.item-label-stacked .label-ios + .input + .cloned-input,\n.item-ios.item-label-floating .label-ios + .input + .cloned-input {\n  margin-left: 0; }\n\n.input-ios[clear-input] {\n  position: relative; }\n\n.input-ios[clear-input] .native-input {\n  padding-right: 46px; }\n\n.input-ios .input-clear-icon {\n  right: 0;\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%20512%20512'><path%20fill='var(--ion-text-ios-color-step-400,%20var(--ion-text-color-step-400,%20%23666666))'%20d='M403.1,108.9c-81.2-81.2-212.9-81.2-294.2,0s-81.2,212.9,0,294.2c81.2,81.2,212.9,81.2,294.2,0S484.3,190.1,403.1,108.9z%20M352,340.2L340.2,352l-84.4-84.2l-84,83.8L160,339.8l84-83.8l-84-83.8l11.8-11.8l84,83.8l84.4-84.2l11.8,11.8L267.6,256L352,340.2z'/></svg>\");\n  width: 30px;\n  background-color: transparent;\n  background-size: 18px; }"; }
    static get styleMode() { return "ios"; }
}

class Item {
    constructor() {
        this.itemStyles = {};
        /**
         * If true, a button tag will be rendered and the item will be tappable. Defaults to `false`.
         */
        this.button = false;
        /**
         * If true, the user cannot interact with the item. Defaults to `false`.
         */
        this.disabled = false;
    }
    itemStyle(ev) {
        ev.stopPropagation();
        const tagName = ev.target.tagName;
        const updatedStyles = ev.detail;
        const updatedKeys = Object.keys(ev.detail);
        const newStyles = {};
        const childStyles = this.itemStyles[tagName] || {};
        let hasStyleChange = false;
        for (const key of updatedKeys) {
            const itemKey = `item-${key}`;
            const newValue = updatedStyles[key];
            if (newValue !== childStyles[itemKey]) {
                hasStyleChange = true;
            }
            newStyles[itemKey] = newValue;
        }
        if (hasStyleChange) {
            this.itemStyles[tagName] = newStyles;
            this.el.forceUpdate();
        }
    }
    componentDidLoad() {
        // Change the button size to small for each ion-button in the item
        // unless the size is explicitly set
        const buttons = this.el.querySelectorAll('ion-button');
        for (let i = 0; i < buttons.length; i++) {
            if (!buttons[i].size) {
                buttons[i].size = 'small';
            }
        }
    }
    render() {
        const childStyles = {};
        for (const key in this.itemStyles) {
            Object.assign(childStyles, this.itemStyles[key]);
        }
        const clickable = !!(this.href || this.el.onclick || this.button);
        const TagType = clickable
            ? this.href ? 'a' : 'button'
            : 'div';
        const attrs = (TagType === 'button')
            ? { type: 'button' }
            : { href: this.href };
        const showDetail = this.detail != null ? this.detail : (this.mode === 'ios' && clickable);
        const themedClasses = Object.assign({}, childStyles, createThemedClasses(this.mode, this.color, 'item'), getElementClassMap(this.el.classList), { 'item-disabled': this.disabled, 'item-detail-push': showDetail });
        return (h(TagType, Object.assign({}, attrs, { class: themedClasses, onClick: (ev) => openURL(this.win, this.href, ev, this.routerDirection) }),
            h("slot", { name: "start" }),
            h("div", { class: "item-inner" },
                h("div", { class: "input-wrapper" },
                    h("slot", null)),
                h("slot", { name: "end" })),
            clickable && this.mode === 'md' && h("ion-ripple-effect", { tapClick: true })));
    }
    static get is() { return "ion-item"; }
    static get properties() { return {
        "button": {
            "type": Boolean,
            "attr": "button"
        },
        "color": {
            "type": String,
            "attr": "color"
        },
        "detail": {
            "type": Boolean,
            "attr": "detail"
        },
        "disabled": {
            "type": Boolean,
            "attr": "disabled"
        },
        "el": {
            "elementRef": true
        },
        "href": {
            "type": String,
            "attr": "href"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        },
        "routerDirection": {
            "type": String,
            "attr": "router-direction"
        },
        "win": {
            "context": "window"
        }
    }; }
    static get listeners() { return [{
            "name": "ionStyle",
            "method": "itemStyle"
        }]; }
    static get style() { return "ion-item {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  display: block; }\n\n.item {\n  border-radius: 0;\n  margin: 0;\n  padding: 0;\n  text-align: initial;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  overflow: hidden;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n  -ms-flex-pack: justify;\n  justify-content: space-between;\n  width: 100%;\n  min-height: 44px;\n  border: 0;\n  outline: none;\n  font-weight: normal;\n  line-height: normal;\n  text-decoration: none;\n  color: inherit; }\n\n.item-inner {\n  margin: 0;\n  padding: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  overflow: hidden;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  -webkit-box-orient: inherit;\n  -webkit-box-direction: inherit;\n  -webkit-flex-direction: inherit;\n  -ms-flex-direction: inherit;\n  flex-direction: inherit;\n  -webkit-box-align: inherit;\n  -webkit-align-items: inherit;\n  -ms-flex-align: inherit;\n  align-items: inherit;\n  -webkit-align-self: stretch;\n  -ms-flex-item-align: stretch;\n  align-self: stretch;\n  min-height: inherit;\n  border: 0; }\n\n.input-wrapper {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  overflow: hidden;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  -webkit-box-orient: inherit;\n  -webkit-box-direction: inherit;\n  -webkit-flex-direction: inherit;\n  -ms-flex-direction: inherit;\n  flex-direction: inherit;\n  -webkit-box-align: inherit;\n  -webkit-align-items: inherit;\n  -ms-flex-align: inherit;\n  align-items: inherit;\n  -webkit-align-self: stretch;\n  -ms-flex-item-align: stretch;\n  align-self: stretch;\n  text-overflow: ellipsis; }\n\n.item[no-lines],\n.item.item[no-lines] .item-inner {\n  border: 0; }\n\nion-item-group {\n  display: block; }\n\n[vertical-align-top],\n.input.item {\n  -webkit-box-align: start;\n  -webkit-align-items: flex-start;\n  -ms-flex-align: start;\n  align-items: flex-start; }\n\n.item-cover {\n  left: 0;\n  top: 0;\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  background: transparent;\n  cursor: pointer; }\n\n.item > ion-icon,\n.item-inner > ion-icon {\n  font-size: 1.6em; }\n\n.item .button {\n  margin: 0; }\n\n.item-disabled {\n  cursor: default;\n  opacity: .4;\n  pointer-events: none; }\n\n.item-ios {\n  padding-left: 16px;\n  padding-left: calc(constant(safe-area-inset-left) + 16px);\n  padding-left: calc(env(safe-area-inset-left) + 16px);\n  border-radius: 0;\n  position: relative;\n  font-family: -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", \"Roboto\", sans-serif;\n  font-size: 17px;\n  color: var(--ion-item-ios-text-color, var(--ion-item-text-color, var(--ion-text-color, #000)));\n  background-color: var(--ion-item-ios-background-color, var(--ion-background-ios-color, var(--ion-background-color, #fff)));\n  -webkit-transition: background-color 200ms linear;\n  transition: background-color 200ms linear; }\n\n.item-ios.activated {\n  background-color: var(--ion-item-ios-background-color-active, var(--ion-item-background-color-active, #d9d9d9));\n  -webkit-transition-duration: 0ms;\n  transition-duration: 0ms; }\n\n.item-ios h1 {\n  margin: 0 0 2px;\n  font-size: 24px;\n  font-weight: normal; }\n\n.item-ios h2 {\n  margin: 0 0 2px;\n  font-size: 17px;\n  font-weight: normal; }\n\n.item-ios h3,\n.item-ios h4,\n.item-ios h5,\n.item-ios h6 {\n  margin: 0 0 3px;\n  font-size: 14px;\n  font-weight: normal;\n  line-height: normal; }\n\n.item-ios p {\n  margin: 0 0 2px;\n  overflow: inherit;\n  font-size: 14px;\n  line-height: normal;\n  text-overflow: inherit;\n  color: var(--ion-text-ios-color-step-600, var(--ion-text-color-step-600, #999999)); }\n\n.item-ios h2:last-child,\n.item-ios h3:last-child,\n.item-ios h4:last-child,\n.item-ios h5:last-child,\n.item-ios h6:last-child,\n.item-ios p:last-child {\n  margin-bottom: 0; }\n\n.item-ios .item-inner {\n  padding-right: 8px;\n  border-bottom: 0.55px solid var(--ion-item-ios-border-color, var(--ion-item-border-color, #c8c7cc)); }\n  \@media screen and (orientation: landscape) {\n    .item-ios .item-inner {\n      padding-right: calc(constant(safe-area-inset-right) + 8px);\n      padding-right: calc(env(safe-area-inset-right) + 8px); } }\n\nion-list > ion-item:first-child .item-ios,\nion-list > ion-item-sliding:first-child .item-ios,\nion-reorder-group > ion-gesture > ion-item:first-child .item-ios,\nion-reorder-group > ion-gesture > ion-item-sliding:first-child .item-ios {\n  border-top: 0.55px solid var(--ion-item-ios-border-color, var(--ion-item-border-color, #c8c7cc)); }\n\nion-list > ion-item:last-child .item-ios,\nion-list > ion-item-sliding:last-child .item-ios,\nion-reorder-group > ion-gesture > ion-item:last-child .item-ios,\nion-reorder-group > ion-gesture > ion-item-sliding:last-child .item-ios {\n  border-bottom: 0.55px solid var(--ion-item-ios-border-color, var(--ion-item-border-color, #c8c7cc)); }\n\nion-list > ion-item:last-child .item-ios .item-inner,\nion-list > ion-item-sliding:last-child .item-ios .item-inner,\nion-reorder-group > ion-gesture > ion-item:last-child .item-ios .item-inner,\nion-reorder-group > ion-gesture > ion-item-sliding:last-child .item-ios .item-inner {\n  border-bottom: 0; }\n\n.item-ios [slot=\"start\"] {\n  margin: 8px 16px 8px 0; }\n\n.item-ios [slot=\"end\"] {\n  margin-left: 8px;\n  margin-right: 8px; }\n\n.item-ios > ion-icon[slot=\"start\"],\n.item-ios > ion-icon[slot=\"end\"] {\n  margin-left: 0;\n  margin-top: 9px;\n  margin-bottom: 8px; }\n\n.item-ios.item-label-stacked [slot=\"end\"],\n.item-ios.item-label-floating [slot=\"end\"] {\n  margin-top: 6px;\n  margin-bottom: 6px; }\n\n.item-ios .button-small-ios {\n  padding: 0 0.5em;\n  height: 24px;\n  font-size: 13px; }\n\n.item-ios .button-small-ios ion-icon[slot=\"icon-only\"] {\n  padding: 0 1px; }\n\n.item-ios ion-avatar {\n  width: 36px;\n  height: 36px; }\n\n.item-ios ion-thumbnail {\n  width: 56px;\n  height: 56px; }\n\n.item-ios ion-avatar[slot=\"end\"],\n.item-ios ion-thumbnail[slot=\"end\"] {\n  margin: 8px; }\n\n.item-ios.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-item-ios-border-color,%20var(--ion-item-border-color,%20%23c8c7cc))'/></svg>\");\n  padding-right: 32px;\n  background-position: right 14px center;\n  background-position: right calc(14px + constant(safe-area-inset-right)) center;\n  background-position: right calc(14px + env(safe-area-inset-right)) center;\n  background-repeat: no-repeat;\n  background-size: 14px 14px; }\n\n.item-ios-primary {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .item-ios-primary .item-inner {\n    border-bottom-color: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n  .item-ios-primary p {\n    color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n  .item-ios-primary.activated {\n    background-color: var(--ion-color-ios-primary-tint, var(--ion-color-primary-tint, #4c8dff)); }\n\n.item-ios-primary.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-color-ios-primary-shade,%20var(--ion-color-primary-shade,%20%233171e0))'/></svg>\"); }\n\n.item-ios-secondary {\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .item-ios-secondary .item-inner {\n    border-bottom-color: var(--ion-color-ios-secondary-shade, var(--ion-color-secondary-shade, #0bb8cc)); }\n  .item-ios-secondary p {\n    color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n  .item-ios-secondary.activated {\n    background-color: var(--ion-color-ios-secondary-tint, var(--ion-color-secondary-tint, #24d6ea)); }\n\n.item-ios-secondary.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-color-ios-secondary-shade,%20var(--ion-color-secondary-shade,%20%230bb8cc))'/></svg>\"); }\n\n.item-ios-tertiary {\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .item-ios-tertiary .item-inner {\n    border-bottom-color: var(--ion-color-ios-tertiary-shade, var(--ion-color-tertiary-shade, #633ce0)); }\n  .item-ios-tertiary p {\n    color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n  .item-ios-tertiary.activated {\n    background-color: var(--ion-color-ios-tertiary-tint, var(--ion-color-tertiary-tint, #7e57ff)); }\n\n.item-ios-tertiary.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-color-ios-tertiary-shade,%20var(--ion-color-tertiary-shade,%20%23633ce0))'/></svg>\"); }\n\n.item-ios-success {\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .item-ios-success .item-inner {\n    border-bottom-color: var(--ion-color-ios-success-shade, var(--ion-color-success-shade, #0ec254)); }\n  .item-ios-success p {\n    color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff)); }\n  .item-ios-success.activated {\n    background-color: var(--ion-color-ios-success-tint, var(--ion-color-success-tint, #28e070)); }\n\n.item-ios-success.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-color-ios-success-shade,%20var(--ion-color-success-shade,%20%230ec254))'/></svg>\"); }\n\n.item-ios-warning {\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .item-ios-warning .item-inner {\n    border-bottom-color: var(--ion-color-ios-warning-shade, var(--ion-color-warning-shade, #e0b500)); }\n  .item-ios-warning p {\n    color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n  .item-ios-warning.activated {\n    background-color: var(--ion-color-ios-warning-tint, var(--ion-color-warning-tint, #ffd31a)); }\n\n.item-ios-warning.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-color-ios-warning-shade,%20var(--ion-color-warning-shade,%20%23e0b500))'/></svg>\"); }\n\n.item-ios-danger {\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .item-ios-danger .item-inner {\n    border-bottom-color: var(--ion-color-ios-danger-shade, var(--ion-color-danger-shade, #d33939)); }\n  .item-ios-danger p {\n    color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n  .item-ios-danger.activated {\n    background-color: var(--ion-color-ios-danger-tint, var(--ion-color-danger-tint, #f25454)); }\n\n.item-ios-danger.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-color-ios-danger-shade,%20var(--ion-color-danger-shade,%20%23d33939))'/></svg>\"); }\n\n.item-ios-light {\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .item-ios-light .item-inner {\n    border-bottom-color: var(--ion-color-ios-light-shade, var(--ion-color-light-shade, #d7d8da)); }\n  .item-ios-light p {\n    color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000)); }\n  .item-ios-light.activated {\n    background-color: var(--ion-color-ios-light-tint, var(--ion-color-light-tint, #f5f6f9)); }\n\n.item-ios-light.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-color-ios-light-shade,%20var(--ion-color-light-shade,%20%23d7d8da))'/></svg>\"); }\n\n.item-ios-medium {\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .item-ios-medium .item-inner {\n    border-bottom-color: var(--ion-color-ios-medium-shade, var(--ion-color-medium-shade, #86888f)); }\n  .item-ios-medium p {\n    color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n  .item-ios-medium.activated {\n    background-color: var(--ion-color-ios-medium-tint, var(--ion-color-medium-tint, #a2a4ab)); }\n\n.item-ios-medium.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-color-ios-medium-shade,%20var(--ion-color-medium-shade,%20%2386888f))'/></svg>\"); }\n\n.item-ios-dark {\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n  .item-ios-dark .item-inner {\n    border-bottom-color: var(--ion-color-ios-dark-shade, var(--ion-color-dark-shade, #1e2023)); }\n  .item-ios-dark p {\n    color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n  .item-ios-dark.activated {\n    background-color: var(--ion-color-ios-dark-tint, var(--ion-color-dark-tint, #383a3e)); }\n\n.item-ios-dark.item-detail-push .item-inner {\n  background-image: url(\"data:image/svg+xml;charset=utf-8,<svg%20xmlns='http://www.w3.org/2000/svg'%20viewBox='0%200%2012%2020'><path%20d='M2,20l-2-2l8-8L0,2l2-2l10,10L2,20z'%20fill='var(--ion-color-ios-dark-shade,%20var(--ion-color-dark-shade,%20%231e2023))'/></svg>\"); }"; }
    static get styleMode() { return "ios"; }
}

class Label {
    getText() {
        return this.el.textContent || '';
    }
    componentDidLoad() {
        this.positionChanged();
    }
    positionChanged() {
        const position = this.position;
        return this.ionStyle.emit({
            [`label-${position}`]: !!position,
        });
    }
    hostData() {
        const position = this.position;
        return {
            class: {
                [`label-${position}`]: !!position,
                [`label-${this.mode}-${position}`]: !!position
            }
        };
    }
    static get is() { return "ion-label"; }
    static get host() { return {
        "theme": "label"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "el": {
            "elementRef": true
        },
        "getText": {
            "method": true
        },
        "mode": {
            "type": String,
            "attr": "mode"
        },
        "position": {
            "type": String,
            "attr": "position",
            "watchCallbacks": ["positionChanged"]
        }
    }; }
    static get events() { return [{
            "name": "ionStyle",
            "method": "ionStyle",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return "ion-label {\n  margin: 0;\n  display: block;\n  overflow: hidden;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  font-size: inherit;\n  text-overflow: ellipsis;\n  white-space: nowrap; }\n\n.item-input ion-label {\n  -webkit-box-flex: initial;\n  -webkit-flex: initial;\n  -ms-flex: initial;\n  flex: initial;\n  max-width: 200px;\n  pointer-events: none; }\n\n[text-wrap] ion-label {\n  white-space: normal; }\n\n.label-fixed {\n  -webkit-box-flex: 0;\n  -webkit-flex: 0 0 100px;\n  -ms-flex: 0 0 100px;\n  flex: 0 0 100px;\n  width: 100px;\n  min-width: 100px;\n  max-width: 200px; }\n\n.item-label-stacked ion-label,\n.item-label-floating ion-label {\n  -webkit-align-self: stretch;\n  -ms-flex-item-align: stretch;\n  align-self: stretch;\n  width: auto;\n  max-width: 100%; }\n\n.label-stacked,\n.label-floating {\n  margin-bottom: 0; }\n\n.item-label-stacked .input-wrapper,\n.item-label-floating .input-wrapper {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n  -ms-flex-direction: column;\n  flex-direction: column; }\n\n.item-label-stacked ion-select,\n.item-label-floating ion-select {\n  -webkit-align-self: stretch;\n  -ms-flex-item-align: stretch;\n  align-self: stretch;\n  max-width: 100%; }\n\n.label-ios {\n  margin: 11px 8px 11px 0;\n  font-family: -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", \"Roboto\", sans-serif; }\n\n[text-wrap] .label-ios {\n  font-size: 14px;\n  line-height: 1.5; }\n\n.label-ios-stacked {\n  margin-bottom: 4px;\n  font-size: 12px; }\n\n.label-ios-floating {\n  margin-bottom: 0;\n  -webkit-transform: translate3d(0,  27px,  0);\n  transform: translate3d(0,  27px,  0);\n  -webkit-transform-origin: left top;\n  transform-origin: left top;\n  -webkit-transition: -webkit-transform 150ms ease-in-out;\n  transition: -webkit-transform 150ms ease-in-out;\n  transition: transform 150ms ease-in-out;\n  transition: transform 150ms ease-in-out, -webkit-transform 150ms ease-in-out; }\n\n.item-input-has-focus .label-ios-floating,\n.item-input-has-value .label-ios-floating {\n  -webkit-transform: translate3d(0,  0,  0) scale(0.8);\n  transform: translate3d(0,  0,  0) scale(0.8); }\n\n.label-ios-primary,\n.item-input .label-ios-primary,\n.item-select .label-ios-primary,\n.item-datetime .label-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.label-ios-secondary,\n.item-input .label-ios-secondary,\n.item-select .label-ios-secondary,\n.item-datetime .label-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.label-ios-tertiary,\n.item-input .label-ios-tertiary,\n.item-select .label-ios-tertiary,\n.item-datetime .label-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.label-ios-success,\n.item-input .label-ios-success,\n.item-select .label-ios-success,\n.item-datetime .label-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.label-ios-warning,\n.item-input .label-ios-warning,\n.item-select .label-ios-warning,\n.item-datetime .label-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.label-ios-danger,\n.item-input .label-ios-danger,\n.item-select .label-ios-danger,\n.item-datetime .label-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.label-ios-light,\n.item-input .label-ios-light,\n.item-select .label-ios-light,\n.item-datetime .label-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.label-ios-medium,\n.item-input .label-ios-medium,\n.item-select .label-ios-medium,\n.item-datetime .label-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.label-ios-dark,\n.item-input .label-ios-dark,\n.item-select .label-ios-dark,\n.item-datetime .label-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "ios"; }
}

class List {
    /**
     * Get the [Item Sliding](../../item-sliding/ItemSliding) that is currently opene.
     */
    getOpenItem() {
        return this.openItem;
    }
    /**
     * Set an [Item Sliding](../../item-sliding/ItemSliding) as the open item.
     */
    setOpenItem(itemSliding) {
        this.openItem = itemSliding;
    }
    /**
     * Close the sliding items. Items can also be closed from the [Item Sliding](../../item-sliding/ItemSliding).
     * Returns a boolean value of whether it closed an item or not.
     */
    closeSlidingItems() {
        if (this.openItem) {
            this.openItem.close();
            this.openItem = undefined;
            return true;
        }
        return false;
    }
    static get is() { return "ion-list"; }
    static get host() { return {
        "theme": "list"
    }; }
    static get properties() { return {
        "closeSlidingItems": {
            "method": true
        },
        "getOpenItem": {
            "method": true
        },
        "setOpenItem": {
            "method": true
        }
    }; }
    static get style() { return "ion-list {\n  margin: 0;\n  padding: 0;\n  display: block;\n  contain: content;\n  list-style-type: none; }\n\nion-list[inset] {\n  overflow: hidden;\n  -webkit-transform: translateZ(0);\n  transform: translateZ(0); }\n\n.list-ios {\n  margin: -1px 0 32px; }\n\n.list-ios .item[no-lines],\n.list-ios .item[no-lines] .item-inner {\n  border-width: 0; }\n\n.list-ios:not([inset]) + .list-ios:not([inset]) ion-list-header {\n  margin-top: -10px;\n  padding-top: 0; }\n\n.list-ios[inset] {\n  margin: 16px;\n  border-radius: 4px; }\n\n.list-ios[inset] ion-list-header {\n  background-color: var(--ion-item-ios-background-color, var(--ion-background-ios-color, var(--ion-background-color, #fff))); }\n\n.list-ios[inset] .item {\n  border-bottom: 1px solid var(--ion-item-ios-border-color, var(--ion-item-border-color, #c8c7cc)); }\n\n.list-ios[inset] .item-inner {\n  border-bottom: 0; }\n\n.list-ios[inset] > ion-item:first-child .item,\n.list-ios[inset] > ion-item-sliding:first-child .item {\n  border-top: 0; }\n\n.list-ios[inset] > ion-item:last-child .item,\n.list-ios[inset] > ion-item-sliding:last-child .item {\n  border-bottom: 0; }\n\n.list-ios[inset] + ion-list[inset] {\n  margin-top: 0; }\n\n.list-ios[no-lines] ion-list-header,\n.list-ios[no-lines] .item,\n.list-ios[no-lines] .item-inner {\n  border-width: 0; }"; }
    static get styleMode() { return "ios"; }
}

class ListHeader {
    static get is() { return "ion-list-header"; }
    static get host() { return {
        "theme": "list-header"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        }
    }; }
    static get style() { return "ion-list-header {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  margin: 0;\n  padding: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  overflow: hidden;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n  -ms-flex-pack: justify;\n  justify-content: space-between;\n  width: 100%;\n  min-height: 40px; }\n\n.list-header-ios {\n  padding-left: 16px;\n  position: relative;\n  font-size: 12px;\n  font-weight: 500;\n  letter-spacing: 1px;\n  text-transform: uppercase;\n  color: var(--ion-text-ios-color-step-150, var(--ion-text-color-step-150, #262626));\n  background: transparent; }\n\n.list-header-ios-primary {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.list-header-ios-secondary {\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.list-header-ios-tertiary {\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.list-header-ios-success {\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.list-header-ios-warning {\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.list-header-ios-danger {\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.list-header-ios-light {\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.list-header-ios-medium {\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.list-header-ios-dark {\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "ios"; }
}

class LoadingController {
    constructor() {
        this.loadings = new Map();
    }
    loadingWillPresent(ev) {
        this.loadings.set(ev.target.overlayId, ev.target);
    }
    loadingWillDismiss(ev) {
        this.loadings.delete(ev.target.overlayId);
    }
    escapeKeyUp() {
        removeLastOverlay(this.loadings);
    }
    /*
     * Create a loading overlay with loading options.
     */
    create(opts) {
        return createOverlay(this.doc.createElement('ion-loading'), opts);
    }
    /*
     * Dismiss the open loading overlay.
     */
    dismiss(data, role, loadingId = -1) {
        return dismissOverlay(data, role, this.loadings, loadingId);
    }
    /*
     * Get the most recently opened loading overlay.
     */
    getTop() {
        return getTopOverlay(this.loadings);
    }
    static get is() { return "ion-loading-controller"; }
    static get properties() { return {
        "create": {
            "method": true
        },
        "dismiss": {
            "method": true
        },
        "doc": {
            "context": "document"
        },
        "getTop": {
            "method": true
        }
    }; }
    static get listeners() { return [{
            "name": "body:ionLoadingWillPresent",
            "method": "loadingWillPresent"
        }, {
            "name": "body:ionLoadingWillDismiss",
            "method": "loadingWillDismiss"
        }, {
            "name": "body:ionLoadingDidUnload",
            "method": "loadingWillDismiss"
        }, {
            "name": "body:keyup.escape",
            "method": "escapeKeyUp"
        }]; }
}

class ModalController {
    constructor() {
        this.modals = new Map();
    }
    modalWillPresent(ev) {
        this.modals.set(ev.target.overlayId, ev.target);
    }
    modalWillDismiss(ev) {
        this.modals.delete(ev.target.overlayId);
    }
    escapeKeyUp() {
        removeLastOverlay(this.modals);
    }
    /*
     * Create a modal overlay with modal options.
     */
    create(opts) {
        return createOverlay(this.doc.createElement('ion-modal'), opts);
    }
    /*
     * Dismiss the open modal overlay.
     */
    dismiss(data, role, modalId = -1) {
        return dismissOverlay(data, role, this.modals, modalId);
    }
    /*
     * Get the most recently opened modal overlay.
     */
    getTop() {
        return getTopOverlay(this.modals);
    }
    static get is() { return "ion-modal-controller"; }
    static get properties() { return {
        "create": {
            "method": true
        },
        "dismiss": {
            "method": true
        },
        "doc": {
            "context": "document"
        },
        "getTop": {
            "method": true
        }
    }; }
    static get listeners() { return [{
            "name": "body:ionModalWillPresent",
            "method": "modalWillPresent"
        }, {
            "name": "body:ionModalWillDismiss",
            "method": "modalWillDismiss"
        }, {
            "name": "body:ionModalDidUnload",
            "method": "modalWillDismiss"
        }, {
            "name": "body:keyup.escape",
            "method": "escapeKeyUp"
        }]; }
}

class Thumbnail {
    static get is() { return "ion-thumbnail"; }
    static get host() { return {
        "theme": "thumbnail"
    }; }
    static get style() { return "ion-thumbnail {\n  display: block; }\n\nion-thumbnail img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n  object-fit: cover; }\n\n.thumbnail-ios {\n  border-radius: 0;\n  width: 48px;\n  height: 48px; }\n\n.thumbnail-ios ion-img,\n.thumbnail-ios img {\n  border-radius: 0;\n  overflow: hidden; }"; }
    static get styleMode() { return "ios"; }
}

export { AppCardModal, AppCheckout, AlertController as IonAlertController, Input as IonInput, Item as IonItem, Label as IonLabel, List as IonList, ListHeader as IonListHeader, LoadingController as IonLoadingController, ModalController as IonModalController, Thumbnail as IonThumbnail };
