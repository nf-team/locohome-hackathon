/*! Built with http://stenciljs.com */
const { h } = window.App;

import { a as APARTMENTS } from './chunk-12551cbb.js';

function commas(n) {
    if (n < 1000) {
        return String(n);
    }
    else {
        let whole = Math.floor(n);
        let fraction = n - whole;
        whole = String(whole).split('');
        let i = whole.length;
        while (i > 3)
            whole.splice((i -= 3), 0, ' ');
        return whole.join('') + String(fraction).slice(1);
    }
}
class AppFavs {
    constructor() {
        this.favCounter = 0;
    }
    openApartment(id) {
        window['router'].push('/apartment/' + id);
    }
    favorites() {
        window['onFavsChange'] = () => { this.favCounter++; };
        return localStorage.getItem('favs') ? JSON.parse(localStorage.getItem('favs')) : [];
    }
    renderCard(ap) {
        return h("ion-card", { onClick: () => { this.openApartment(ap.id); } },
            h("ion-header", null,
                h("img", { src: ap.images[0], alt: ap.name })),
            h("ion-card-content", null,
                h("ion-card-subtitle", null, ap.name),
                h("ion-card-subtitle", null,
                    commas(ap.price),
                    " RUB"),
                h("p", null,
                    ap.description.slice(0, 100),
                    "...")));
    }
    getFavs() {
        return APARTMENTS.filter(x => { return this.favorites().indexOf(x.id.toString()) > -1; });
    }
    renderCards() {
        return this.getFavs().map(x => this.renderCard(x));
    }
    render() {
        return [
            h("ion-tabs", null,
                h("ion-tab", { href: "/", active: true, selected: true, label: "Search", icon: "search" },
                    h("app-header", null),
                    h("ion-content", null,
                        this.favorites().length == 0 && h("h2", { class: "empty-list" },
                            h("div", null, "You have no favorites yet :(")),
                        this.renderCards())),
                h("ion-tab", { href: "/favs", label: "Favorites", icon: "star" }),
                h("ion-tab", { href: "/settings", label: "Settings", icon: "settings" }))
        ];
    }
    static get is() { return "app-favs"; }
    static get properties() { return {
        "favCounter": {
            "state": true
        }
    }; }
    static get style() { return ".empty-list {\n  font-weight: 200;\n  text-align: center;\n  padding: 0 1rem;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n  -ms-flex-direction: column;\n  flex-direction: column;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center; }"; }
}

export { AppFavs };
