/*! Built with http://stenciljs.com */
const { h } = window.App;

import { d as now } from './chunk-63df273d.js';

class Backdrop {
    constructor() {
        this.lastClick = -10000;
        /**
         * If true, the backdrop will be visible. Defaults to `true`.
         */
        this.visible = true;
        /**
         * If true, the backdrop will can be clicked and will emit the `ionBackdropTap` event. Defaults to `true`.
         */
        this.tappable = true;
        /**
         * If true, the backdrop will stop propagation on tap. Defaults to `true`.
         */
        this.stopPropagation = true;
    }
    componentDidLoad() {
        registerBackdrop(this.doc, this);
    }
    componentDidUnload() {
        unregisterBackdrop(this.doc, this);
    }
    onTouchStart(ev) {
        this.lastClick = now(ev);
        this.emitTap(ev);
    }
    onMouseDown(ev) {
        if (this.lastClick < now(ev) - 2500) {
            this.emitTap(ev);
        }
    }
    emitTap(ev) {
        if (this.stopPropagation) {
            ev.preventDefault();
            ev.stopPropagation();
        }
        if (this.tappable) {
            this.ionBackdropTap.emit();
        }
    }
    hostData() {
        return {
            tabindex: '-1',
            class: {
                'backdrop-hide': !this.visible,
                'backdrop-no-tappable': !this.tappable,
            }
        };
    }
    static get is() { return "ion-backdrop"; }
    static get host() { return {
        "theme": "backdrop"
    }; }
    static get properties() { return {
        "doc": {
            "context": "document"
        },
        "stopPropagation": {
            "type": Boolean,
            "attr": "stop-propagation"
        },
        "tappable": {
            "type": Boolean,
            "attr": "tappable"
        },
        "visible": {
            "type": Boolean,
            "attr": "visible"
        }
    }; }
    static get events() { return [{
            "name": "ionBackdropTap",
            "method": "ionBackdropTap",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "touchstart",
            "method": "onTouchStart",
            "capture": true
        }, {
            "name": "mousedown",
            "method": "onMouseDown",
            "capture": true
        }]; }
    static get style() { return "ion-backdrop {\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  position: absolute;\n  z-index: 2;\n  display: block;\n  cursor: pointer;\n  opacity: .01;\n  -webkit-transform: translateZ(0);\n  transform: translateZ(0);\n  -ms-touch-action: none;\n  touch-action: none;\n  contain: strict; }\n  ion-backdrop.backdrop-hide {\n    background: transparent; }\n  ion-backdrop.backdrop-no-tappable {\n    cursor: auto; }\n\nbody.backdrop-no-scroll {\n  overflow: hidden; }\n\n.backdrop-ios {\n  background-color: var(--ion-backdrop-ios-color, var(--ion-backdrop-color, #000)); }"; }
    static get styleMode() { return "ios"; }
}
const BACKDROP_NO_SCROLL = 'backdrop-no-scroll';
const activeBackdrops = new Set();
function registerBackdrop(doc, backdrop) {
    activeBackdrops.add(backdrop);
    doc.body.classList.add(BACKDROP_NO_SCROLL);
}
function unregisterBackdrop(doc, backdrop) {
    activeBackdrops.delete(backdrop);
    if (activeBackdrops.size === 0) {
        doc.body.classList.remove(BACKDROP_NO_SCROLL);
    }
}

class RippleEffect {
    constructor() {
        this.lastClick = -10000;
        this.tapClick = false;
    }
    tapClickChanged(tapClick) {
        this.enableListener(this, 'parent:ionActivated', tapClick);
        this.enableListener(this, 'touchstart', !tapClick);
        this.enableListener(this, 'mousedown', !tapClick);
    }
    ionActivated(ev) {
        this.addRipple(ev.detail.x, ev.detail.y);
    }
    touchStart(ev) {
        this.lastClick = now(ev);
        const touches = ev.touches[0];
        this.addRipple(touches.clientX, touches.clientY);
    }
    mouseDown(ev) {
        const timeStamp = now(ev);
        if (this.lastClick < (timeStamp - 1000)) {
            this.addRipple(ev.pageX, ev.pageY);
        }
    }
    componentDidLoad() {
        this.tapClickChanged(this.tapClick);
    }
    addRipple(pageX, pageY) {
        let x, y, size;
        this.queue.read(() => {
            const rect = this.el.getBoundingClientRect();
            const width = rect.width;
            const height = rect.height;
            size = Math.min(Math.sqrt(width * width + height * height) * 2, MAX_RIPPLE_DIAMETER);
            x = pageX - rect.left - (size / 2);
            y = pageY - rect.top - (size / 2);
        });
        this.queue.write(() => {
            const div = this.doc.createElement('div');
            div.classList.add('ripple-effect');
            const style = div.style;
            const duration = Math.max(RIPPLE_FACTOR * Math.sqrt(size), MIN_RIPPLE_DURATION);
            style.top = y + 'px';
            style.left = x + 'px';
            style.width = size + 'px';
            style.height = size + 'px';
            style.animationDuration = duration + 'ms';
            this.el.appendChild(div);
            setTimeout(() => div.remove(), duration + 50);
        });
    }
    static get is() { return "ion-ripple-effect"; }
    static get properties() { return {
        "addRipple": {
            "method": true
        },
        "doc": {
            "context": "document"
        },
        "el": {
            "elementRef": true
        },
        "enableListener": {
            "context": "enableListener"
        },
        "queue": {
            "context": "queue"
        },
        "tapClick": {
            "type": Boolean,
            "attr": "tap-click",
            "watchCallbacks": ["tapClickChanged"]
        }
    }; }
    static get listeners() { return [{
            "name": "parent:ionActivated",
            "method": "ionActivated",
            "disabled": true
        }, {
            "name": "touchstart",
            "method": "touchStart",
            "disabled": true,
            "passive": true
        }, {
            "name": "mousedown",
            "method": "mouseDown",
            "disabled": true,
            "passive": true
        }]; }
    static get style() { return "ion-ripple-effect {\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  position: absolute;\n  contain: strict; }\n\n.ripple-effect {\n  border-radius: 50%;\n  position: absolute;\n  background-color: var(--ion-ripple-background-color, #000);\n  opacity: 0;\n  will-change: transform, opacity;\n  pointer-events: none;\n  -webkit-animation-name: rippleAnimation;\n  animation-name: rippleAnimation;\n  -webkit-animation-duration: 200ms;\n  animation-duration: 200ms;\n  -webkit-animation-timing-function: ease-out;\n  animation-timing-function: ease-out;\n  contain: strict; }\n\n\@-webkit-keyframes rippleAnimation {\n  0% {\n    opacity: .2;\n    -webkit-transform: scale(0.1);\n    transform: scale(0.1); }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale(1);\n    transform: scale(1); } }\n\n\@keyframes rippleAnimation {\n  0% {\n    opacity: .2;\n    -webkit-transform: scale(0.1);\n    transform: scale(0.1); }\n  100% {\n    opacity: 0;\n    -webkit-transform: scale(1);\n    transform: scale(1); } }"; }
}
const RIPPLE_FACTOR = 35;
const MIN_RIPPLE_DURATION = 260;
const MAX_RIPPLE_DIAMETER = 550;

export { Backdrop as IonBackdrop, RippleEffect as IonRippleEffect };
