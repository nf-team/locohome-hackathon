/*! Built with http://stenciljs.com */
const { h } = window.App;

import { e as getButtonClassMap, c as getElementClassMap, d as openURL, a as createThemedClasses } from './chunk-e901a817.js';

class AppHeader {
    render() {
        return h("ion-header", null,
            h("ion-toolbar", { color: 'primary' },
                h("ion-title", null,
                    h("ion-icon", { name: "planet", class: "logo-icon", size: "lg" }),
                    "LocoHome")));
    }
    static get is() { return "app-header"; }
    static get style() { return ".logo-icon {\n  -webkit-transform: scale(1.5) translateY(1px);\n  transform: scale(1.5) translateY(1px);\n  padding: 0 6px; }"; }
}

class Badge {
    static get is() { return "ion-badge"; }
    static get host() { return {
        "theme": "badge"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        }
    }; }
    static get style() { return "ion-badge {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  padding: 3px 8px;\n  text-align: center;\n  display: inline-block;\n  min-width: 10px;\n  font-size: 13px;\n  font-weight: bold;\n  line-height: 1;\n  white-space: nowrap;\n  vertical-align: baseline;\n  contain: content; }\n\nion-badge:empty {\n  display: none; }\n\n.badge-md {\n  border-radius: 4px;\n  font-family: \"Roboto\", \"Helvetica Neue\", sans-serif;\n  color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.badge-md-primary {\n  color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.badge-md-secondary {\n  color: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.badge-md-tertiary {\n  color: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.badge-md-success {\n  color: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.badge-md-warning {\n  color: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.badge-md-danger {\n  color: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.badge-md-light {\n  color: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.badge-md-medium {\n  color: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.badge-md-dark {\n  color: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "md"; }
}

class Button {
    constructor() {
        this.keyFocus = false;
        /**
         * The type of button.
         * Possible values are: `"button"`, `"bar-button"`.
         */
        this.buttonType = 'button';
        /**
         * If true, the user cannot interact with the button. Defaults to `false`.
         */
        this.disabled = false;
        /**
         * Set to `"clear"` for a transparent button, to `"outline"` for a transparent
         * button with a border, or to `"solid"`. The default style is `"solid"` except inside of
         * a toolbar, where the default is `"clear"`.
         */
        this.fill = 'default';
        /**
         * If true, activates a button with rounded corners.
         */
        this.round = false;
        /**
         * If true, activates a button with a heavier font weight.
         */
        this.strong = false;
        /**
         * The type of the button.
         * Possible values are: `"submit"`, `"reset"` and `"button"`.
         * Default value is: `"button"`
         */
        this.type = 'button';
    }
    componentWillLoad() {
        if (this.el.closest('ion-buttons')) {
            this.buttonType = 'bar-button';
        }
    }
    onFocus() {
        this.ionFocus.emit();
    }
    onKeyUp() {
        this.keyFocus = true;
    }
    onBlur() {
        this.keyFocus = false;
        this.ionBlur.emit();
    }
    render() {
        const { buttonType, color, expand, fill, mode, round, size, strong } = this;
        const TagType = this.href ? 'a' : 'button';
        const buttonClasses = Object.assign({}, getButtonClassMap(buttonType, mode), getButtonTypeClassMap(buttonType, expand, mode), getButtonTypeClassMap(buttonType, size, mode), getButtonTypeClassMap(buttonType, round ? 'round' : undefined, mode), getButtonTypeClassMap(buttonType, strong ? 'strong' : undefined, mode), getColorClassMap(buttonType, color, fill, mode), getElementClassMap(this.el.classList), { 'focused': this.keyFocus });
        const attrs = (TagType === 'button')
            ? { type: this.type }
            : { href: this.href };
        return (h(TagType, Object.assign({}, attrs, { class: buttonClasses, disabled: this.disabled, onFocus: this.onFocus.bind(this), onKeyUp: this.onKeyUp.bind(this), onBlur: this.onBlur.bind(this), onClick: (ev) => openURL(this.win, this.href, ev, this.routerDirection) }),
            h("span", { class: "button-inner" },
                h("slot", { name: "icon-only" }),
                h("slot", { name: "start" }),
                h("slot", null),
                h("slot", { name: "end" })),
            this.mode === 'md' && h("ion-ripple-effect", { tapClick: true })));
    }
    static get is() { return "ion-button"; }
    static get properties() { return {
        "buttonType": {
            "type": String,
            "attr": "button-type",
            "mutable": true
        },
        "color": {
            "type": String,
            "attr": "color"
        },
        "disabled": {
            "type": Boolean,
            "attr": "disabled"
        },
        "el": {
            "elementRef": true
        },
        "expand": {
            "type": String,
            "attr": "expand"
        },
        "fill": {
            "type": String,
            "attr": "fill"
        },
        "href": {
            "type": String,
            "attr": "href"
        },
        "keyFocus": {
            "state": true
        },
        "mode": {
            "type": String,
            "attr": "mode"
        },
        "round": {
            "type": Boolean,
            "attr": "round"
        },
        "routerDirection": {
            "type": String,
            "attr": "router-direction"
        },
        "size": {
            "type": String,
            "attr": "size"
        },
        "strong": {
            "type": Boolean,
            "attr": "strong"
        },
        "type": {
            "type": String,
            "attr": "type"
        },
        "win": {
            "context": "window"
        }
    }; }
    static get events() { return [{
            "name": "ionFocus",
            "method": "ionFocus",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionBlur",
            "method": "ionBlur",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return ".button {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  text-align: center;\n  -moz-appearance: none;\n  -ms-appearance: none;\n  -webkit-appearance: none;\n  appearance: none;\n  position: relative;\n  z-index: 0;\n  display: inline-block;\n  border: 0;\n  line-height: 1;\n  text-decoration: none;\n  text-overflow: ellipsis;\n  text-transform: none;\n  white-space: nowrap;\n  cursor: pointer;\n  vertical-align: top;\n  vertical-align: -webkit-baseline-middle;\n  -webkit-transition: background-color, opacity 100ms linear;\n  transition: background-color, opacity 100ms linear;\n  -webkit-font-kerning: none;\n  font-kerning: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  contain: content; }\n\n.button:active,\n.button:focus {\n  outline: none; }\n\n.button-inner {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -webkit-flex-flow: row nowrap;\n  -ms-flex-flow: row nowrap;\n  flex-flow: row nowrap;\n  -webkit-flex-shrink: 0;\n  -ms-flex-negative: 0;\n  flex-shrink: 0;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  width: 100%;\n  height: 100%; }\n\na[disabled],\nbutton[disabled],\n.button[disabled] {\n  cursor: default;\n  pointer-events: none; }\n\n.button-block {\n  display: block;\n  clear: both;\n  width: 100%;\n  contain: strict; }\n\n.button-block::after {\n  clear: both; }\n\n.button-full {\n  display: block;\n  width: 100%;\n  contain: strict; }\n\n.button-full.button-outline {\n  border-radius: 0;\n  border-right-width: 0;\n  border-left-width: 0; }\n\n.button ion-icon {\n  font-size: 1.4em;\n  pointer-events: none; }\n\n.button ion-icon[slot=\"start\"] {\n  margin: 0 0.3em 0 -0.3em; }\n\n.button ion-icon[slot=\"end\"] {\n  margin: 0 -0.2em 0 0.3em; }\n\n.button ion-icon[slot=\"icon-only\"] {\n  font-size: 1.8em; }\n\n.button-md {\n  border-radius: 2px;\n  margin: 4px 2px;\n  padding: 0 1.1em;\n  overflow: hidden;\n  height: 36px;\n  font-family: \"Roboto\", \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  font-weight: 500;\n  text-transform: uppercase;\n  color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff));\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n  -webkit-transition: background-color 300ms cubic-bezier(0.4, 0, 0.2, 1), color 300ms cubic-bezier(0.4, 0, 0.2, 1), -webkit-box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1);\n  transition: background-color 300ms cubic-bezier(0.4, 0, 0.2, 1), color 300ms cubic-bezier(0.4, 0, 0.2, 1), -webkit-box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1);\n  transition: box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1), background-color 300ms cubic-bezier(0.4, 0, 0.2, 1), color 300ms cubic-bezier(0.4, 0, 0.2, 1);\n  transition: box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1), background-color 300ms cubic-bezier(0.4, 0, 0.2, 1), color 300ms cubic-bezier(0.4, 0, 0.2, 1), -webkit-box-shadow 300ms cubic-bezier(0.4, 0, 0.2, 1); }\n\n.enable-hover .button-md:hover {\n  background-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.button-md.activated {\n  background-color: var(--ion-color-md-primary-shade, var(--ion-color-primary-shade, #3171e0));\n  -webkit-box-shadow: 0 3px 5px rgba(0, 0, 0, 0.14), 0 3px 5px rgba(0, 0, 0, 0.21);\n  box-shadow: 0 3px 5px rgba(0, 0, 0, 0.14), 0 3px 5px rgba(0, 0, 0, 0.21); }\n\n.button-md.focused {\n  background-color: var(--ion-color-md-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n\n.button-md .ripple-effect {\n  background-color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\na[disabled],\nbutton[disabled],\n.button[disabled] {\n  opacity: 0.5; }\n\n.button-large-md {\n  padding: 0 1em;\n  height: 2.8em;\n  font-size: 20px; }\n\n.button-small-md {\n  padding: 0 0.9em;\n  height: 2.1em;\n  font-size: 13px; }\n\n.button-block-md {\n  margin-left: 0;\n  margin-right: 0; }\n\n.button-full-md {\n  margin-left: 0;\n  margin-right: 0;\n  border-radius: 0;\n  border-right-width: 0;\n  border-left-width: 0; }\n\n.button-outline-md {\n  border-width: 1px;\n  border-style: solid;\n  border-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff));\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff));\n  background-color: transparent;\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.enable-hover .button-outline-md:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md.activated {\n  background-color: transparent;\n  -webkit-box-shadow: none;\n  box-shadow: none;\n  opacity: 1; }\n\n.button-outline-md.focused {\n  background-color: rgba(var(--ion-color-md-primary-rgb, var(--ion-color-primary-rgb, 56, 128, 255)), 0.1); }\n\n.button-outline-md .ripple-effect {\n  background-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.button-clear-md {\n  border-color: transparent;\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff));\n  background-color: transparent;\n  -webkit-box-shadow: none;\n  box-shadow: none;\n  opacity: 1; }\n\n.button-clear-md.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md.focused {\n  background-color: rgba(var(--ion-color-md-primary-rgb, var(--ion-color-primary-rgb, 56, 128, 255)), 0.1); }\n\n.enable-hover .button-clear-md:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-clear-md .ripple-effect {\n  background-color: var(--ion-text-md-color-step-600, var(--ion-text-color-step-600, #999999)); }\n\n.button-round-md {\n  border-radius: 64px;\n  padding: 0 26px; }\n\n.button-md ion-icon[slot=\"icon-only\"] {\n  padding: 0; }\n\n.button-md-primary {\n  color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.enable-hover .button-md-primary:hover {\n  background-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.button-md-primary.activated {\n  background-color: var(--ion-color-md-primary-shade, var(--ion-color-primary-shade, #3171e0));\n  opacity: 1; }\n\n.button-md-primary.focused {\n  background-color: var(--ion-color-md-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n\n.button-md-primary .ripple-effect {\n  background-color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.button-outline-md-primary {\n  border-color: var(--ion-color-md-primary-tint, var(--ion-color-primary-tint, #4c8dff));\n  color: var(--ion-color-md-primary-tint, var(--ion-color-primary-tint, #4c8dff));\n  background-color: transparent; }\n\n.enable-hover .button-outline-md-primary:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md-primary.activated {\n  background-color: transparent; }\n\n.button-outline-md-primary.focused {\n  background-color: rgba(var(--ion-color-md-primary-rgb, var(--ion-color-primary-rgb, 56, 128, 255)), 0.1); }\n\n.button-outline-md-primary .ripple-effect {\n  background-color: var(--ion-color-md-primary-tint, var(--ion-color-primary-tint, #4c8dff)); }\n\n.button-clear-md-primary {\n  border-color: transparent;\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff));\n  background-color: transparent; }\n\n.button-clear-md-primary.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md-primary.focused {\n  background-color: rgba(var(--ion-color-md-primary-rgb, var(--ion-color-primary-rgb, 56, 128, 255)), 0.1); }\n\n.enable-hover .button-clear-md-primary:hover {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.button-md-secondary {\n  color: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.enable-hover .button-md-secondary:hover {\n  background-color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.button-md-secondary.activated {\n  background-color: var(--ion-color-md-secondary-shade, var(--ion-color-secondary-shade, #0bb8cc));\n  opacity: 1; }\n\n.button-md-secondary.focused {\n  background-color: var(--ion-color-md-secondary-shade, var(--ion-color-secondary-shade, #0bb8cc)); }\n\n.button-md-secondary .ripple-effect {\n  background-color: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.button-outline-md-secondary {\n  border-color: var(--ion-color-md-secondary-tint, var(--ion-color-secondary-tint, #24d6ea));\n  color: var(--ion-color-md-secondary-tint, var(--ion-color-secondary-tint, #24d6ea));\n  background-color: transparent; }\n\n.enable-hover .button-outline-md-secondary:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md-secondary.activated {\n  background-color: transparent; }\n\n.button-outline-md-secondary.focused {\n  background-color: rgba(var(--ion-color-md-secondary-rgb, var(--ion-color-secondary-rgb, 12, 209, 232)), 0.1); }\n\n.button-outline-md-secondary .ripple-effect {\n  background-color: var(--ion-color-md-secondary-tint, var(--ion-color-secondary-tint, #24d6ea)); }\n\n.button-clear-md-secondary {\n  border-color: transparent;\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8));\n  background-color: transparent; }\n\n.button-clear-md-secondary.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md-secondary.focused {\n  background-color: rgba(var(--ion-color-md-secondary-rgb, var(--ion-color-secondary-rgb, 12, 209, 232)), 0.1); }\n\n.enable-hover .button-clear-md-secondary:hover {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.button-md-tertiary {\n  color: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.enable-hover .button-md-tertiary:hover {\n  background-color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.button-md-tertiary.activated {\n  background-color: var(--ion-color-md-tertiary-shade, var(--ion-color-tertiary-shade, #633ce0));\n  opacity: 1; }\n\n.button-md-tertiary.focused {\n  background-color: var(--ion-color-md-tertiary-shade, var(--ion-color-tertiary-shade, #633ce0)); }\n\n.button-md-tertiary .ripple-effect {\n  background-color: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.button-outline-md-tertiary {\n  border-color: var(--ion-color-md-tertiary-tint, var(--ion-color-tertiary-tint, #7e57ff));\n  color: var(--ion-color-md-tertiary-tint, var(--ion-color-tertiary-tint, #7e57ff));\n  background-color: transparent; }\n\n.enable-hover .button-outline-md-tertiary:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md-tertiary.activated {\n  background-color: transparent; }\n\n.button-outline-md-tertiary.focused {\n  background-color: rgba(var(--ion-color-md-tertiary-rgb, var(--ion-color-tertiary-rgb, 112, 68, 255)), 0.1); }\n\n.button-outline-md-tertiary .ripple-effect {\n  background-color: var(--ion-color-md-tertiary-tint, var(--ion-color-tertiary-tint, #7e57ff)); }\n\n.button-clear-md-tertiary {\n  border-color: transparent;\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff));\n  background-color: transparent; }\n\n.button-clear-md-tertiary.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md-tertiary.focused {\n  background-color: rgba(var(--ion-color-md-tertiary-rgb, var(--ion-color-tertiary-rgb, 112, 68, 255)), 0.1); }\n\n.enable-hover .button-clear-md-tertiary:hover {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.button-md-success {\n  color: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.enable-hover .button-md-success:hover {\n  background-color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.button-md-success.activated {\n  background-color: var(--ion-color-md-success-shade, var(--ion-color-success-shade, #0ec254));\n  opacity: 1; }\n\n.button-md-success.focused {\n  background-color: var(--ion-color-md-success-shade, var(--ion-color-success-shade, #0ec254)); }\n\n.button-md-success .ripple-effect {\n  background-color: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.button-outline-md-success {\n  border-color: var(--ion-color-md-success-tint, var(--ion-color-success-tint, #28e070));\n  color: var(--ion-color-md-success-tint, var(--ion-color-success-tint, #28e070));\n  background-color: transparent; }\n\n.enable-hover .button-outline-md-success:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md-success.activated {\n  background-color: transparent; }\n\n.button-outline-md-success.focused {\n  background-color: rgba(var(--ion-color-md-success-rgb, var(--ion-color-success-rgb, 16, 220, 96)), 0.1); }\n\n.button-outline-md-success .ripple-effect {\n  background-color: var(--ion-color-md-success-tint, var(--ion-color-success-tint, #28e070)); }\n\n.button-clear-md-success {\n  border-color: transparent;\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60));\n  background-color: transparent; }\n\n.button-clear-md-success.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md-success.focused {\n  background-color: rgba(var(--ion-color-md-success-rgb, var(--ion-color-success-rgb, 16, 220, 96)), 0.1); }\n\n.enable-hover .button-clear-md-success:hover {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.button-md-warning {\n  color: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.enable-hover .button-md-warning:hover {\n  background-color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.button-md-warning.activated {\n  background-color: var(--ion-color-md-warning-shade, var(--ion-color-warning-shade, #e0b500));\n  opacity: 1; }\n\n.button-md-warning.focused {\n  background-color: var(--ion-color-md-warning-shade, var(--ion-color-warning-shade, #e0b500)); }\n\n.button-md-warning .ripple-effect {\n  background-color: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.button-outline-md-warning {\n  border-color: var(--ion-color-md-warning-tint, var(--ion-color-warning-tint, #ffd31a));\n  color: var(--ion-color-md-warning-tint, var(--ion-color-warning-tint, #ffd31a));\n  background-color: transparent; }\n\n.enable-hover .button-outline-md-warning:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md-warning.activated {\n  background-color: transparent; }\n\n.button-outline-md-warning.focused {\n  background-color: rgba(var(--ion-color-md-warning-rgb, var(--ion-color-warning-rgb, 255, 206, 0)), 0.1); }\n\n.button-outline-md-warning .ripple-effect {\n  background-color: var(--ion-color-md-warning-tint, var(--ion-color-warning-tint, #ffd31a)); }\n\n.button-clear-md-warning {\n  border-color: transparent;\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00));\n  background-color: transparent; }\n\n.button-clear-md-warning.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md-warning.focused {\n  background-color: rgba(var(--ion-color-md-warning-rgb, var(--ion-color-warning-rgb, 255, 206, 0)), 0.1); }\n\n.enable-hover .button-clear-md-warning:hover {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.button-md-danger {\n  color: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.enable-hover .button-md-danger:hover {\n  background-color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.button-md-danger.activated {\n  background-color: var(--ion-color-md-danger-shade, var(--ion-color-danger-shade, #d33939));\n  opacity: 1; }\n\n.button-md-danger.focused {\n  background-color: var(--ion-color-md-danger-shade, var(--ion-color-danger-shade, #d33939)); }\n\n.button-md-danger .ripple-effect {\n  background-color: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.button-outline-md-danger {\n  border-color: var(--ion-color-md-danger-tint, var(--ion-color-danger-tint, #f25454));\n  color: var(--ion-color-md-danger-tint, var(--ion-color-danger-tint, #f25454));\n  background-color: transparent; }\n\n.enable-hover .button-outline-md-danger:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md-danger.activated {\n  background-color: transparent; }\n\n.button-outline-md-danger.focused {\n  background-color: rgba(var(--ion-color-md-danger-rgb, var(--ion-color-danger-rgb, 240, 65, 65)), 0.1); }\n\n.button-outline-md-danger .ripple-effect {\n  background-color: var(--ion-color-md-danger-tint, var(--ion-color-danger-tint, #f25454)); }\n\n.button-clear-md-danger {\n  border-color: transparent;\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141));\n  background-color: transparent; }\n\n.button-clear-md-danger.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md-danger.focused {\n  background-color: rgba(var(--ion-color-md-danger-rgb, var(--ion-color-danger-rgb, 240, 65, 65)), 0.1); }\n\n.enable-hover .button-clear-md-danger:hover {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.button-md-light {\n  color: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.enable-hover .button-md-light:hover {\n  background-color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.button-md-light.activated {\n  background-color: var(--ion-color-md-light-shade, var(--ion-color-light-shade, #d7d8da));\n  opacity: 1; }\n\n.button-md-light.focused {\n  background-color: var(--ion-color-md-light-shade, var(--ion-color-light-shade, #d7d8da)); }\n\n.button-md-light .ripple-effect {\n  background-color: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.button-outline-md-light {\n  border-color: var(--ion-color-md-light-tint, var(--ion-color-light-tint, #f5f6f9));\n  color: var(--ion-color-md-light-tint, var(--ion-color-light-tint, #f5f6f9));\n  background-color: transparent; }\n\n.enable-hover .button-outline-md-light:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md-light.activated {\n  background-color: transparent; }\n\n.button-outline-md-light.focused {\n  background-color: rgba(var(--ion-color-md-light-rgb, var(--ion-color-light-rgb, 244, 245, 248)), 0.1); }\n\n.button-outline-md-light .ripple-effect {\n  background-color: var(--ion-color-md-light-tint, var(--ion-color-light-tint, #f5f6f9)); }\n\n.button-clear-md-light {\n  border-color: transparent;\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8));\n  background-color: transparent; }\n\n.button-clear-md-light.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md-light.focused {\n  background-color: rgba(var(--ion-color-md-light-rgb, var(--ion-color-light-rgb, 244, 245, 248)), 0.1); }\n\n.enable-hover .button-clear-md-light:hover {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.button-md-medium {\n  color: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.enable-hover .button-md-medium:hover {\n  background-color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.button-md-medium.activated {\n  background-color: var(--ion-color-md-medium-shade, var(--ion-color-medium-shade, #86888f));\n  opacity: 1; }\n\n.button-md-medium.focused {\n  background-color: var(--ion-color-md-medium-shade, var(--ion-color-medium-shade, #86888f)); }\n\n.button-md-medium .ripple-effect {\n  background-color: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.button-outline-md-medium {\n  border-color: var(--ion-color-md-medium-tint, var(--ion-color-medium-tint, #a2a4ab));\n  color: var(--ion-color-md-medium-tint, var(--ion-color-medium-tint, #a2a4ab));\n  background-color: transparent; }\n\n.enable-hover .button-outline-md-medium:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md-medium.activated {\n  background-color: transparent; }\n\n.button-outline-md-medium.focused {\n  background-color: rgba(var(--ion-color-md-medium-rgb, var(--ion-color-medium-rgb, 152, 154, 162)), 0.1); }\n\n.button-outline-md-medium .ripple-effect {\n  background-color: var(--ion-color-md-medium-tint, var(--ion-color-medium-tint, #a2a4ab)); }\n\n.button-clear-md-medium {\n  border-color: transparent;\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2));\n  background-color: transparent; }\n\n.button-clear-md-medium.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md-medium.focused {\n  background-color: rgba(var(--ion-color-md-medium-rgb, var(--ion-color-medium-rgb, 152, 154, 162)), 0.1); }\n\n.enable-hover .button-clear-md-medium:hover {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.button-md-dark {\n  color: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.enable-hover .button-md-dark:hover {\n  background-color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.button-md-dark.activated {\n  background-color: var(--ion-color-md-dark-shade, var(--ion-color-dark-shade, #1e2023));\n  opacity: 1; }\n\n.button-md-dark.focused {\n  background-color: var(--ion-color-md-dark-shade, var(--ion-color-dark-shade, #1e2023)); }\n\n.button-md-dark .ripple-effect {\n  background-color: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.button-outline-md-dark {\n  border-color: var(--ion-color-md-dark-tint, var(--ion-color-dark-tint, #383a3e));\n  color: var(--ion-color-md-dark-tint, var(--ion-color-dark-tint, #383a3e));\n  background-color: transparent; }\n\n.enable-hover .button-outline-md-dark:hover {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1); }\n\n.button-outline-md-dark.activated {\n  background-color: transparent; }\n\n.button-outline-md-dark.focused {\n  background-color: rgba(var(--ion-color-md-dark-rgb, var(--ion-color-dark-rgb, 34, 36, 40)), 0.1); }\n\n.button-outline-md-dark .ripple-effect {\n  background-color: var(--ion-color-md-dark-tint, var(--ion-color-dark-tint, #383a3e)); }\n\n.button-clear-md-dark {\n  border-color: transparent;\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428));\n  background-color: transparent; }\n\n.button-clear-md-dark.activated {\n  background-color: rgba(var(--ion-text-md-color-rgb, var(--ion-text-color-rgb, 0, 0, 0)), 0.1);\n  -webkit-box-shadow: none;\n  box-shadow: none; }\n\n.button-clear-md-dark.focused {\n  background-color: rgba(var(--ion-color-md-dark-rgb, var(--ion-color-dark-rgb, 34, 36, 40)), 0.1); }\n\n.enable-hover .button-clear-md-dark:hover {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.button-strong-md {\n  font-weight: bold; }"; }
    static get styleMode() { return "md"; }
}
/**
 * Get the classes based on the type
 * e.g. block, full, round, large
 */
function getButtonTypeClassMap(buttonType, type, mode) {
    if (!type) {
        return {};
    }
    type = type.toLocaleLowerCase();
    return {
        [`${buttonType}-${type}`]: true,
        [`${buttonType}-${type}-${mode}`]: true
    };
}
function getColorClassMap(buttonType, color, fill, mode) {
    let className = buttonType;
    if (buttonType !== 'bar-button' && fill === 'solid') {
        fill = 'default';
    }
    if (fill && fill !== 'default') {
        className += `-${fill.toLowerCase()}`;
    }
    // special case for a default bar button
    // if the bar button is default it should get the fill
    // but if a color is passed the fill shouldn't be added
    if (buttonType === 'bar-button' && fill === 'default') {
        className = buttonType;
        if (!color) {
            className += '-' + fill.toLowerCase();
        }
    }
    const map = {
        [className]: true,
        [`${className}-${mode}`]: true,
    };
    if (color) {
        map[`${className}-${mode}-${color}`] = true;
    }
    return map;
}

class Card {
    static get is() { return "ion-card"; }
    static get host() { return {
        "theme": "card"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        }
    }; }
    static get style() { return "ion-card {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  position: relative;\n  display: block;\n  overflow: hidden; }\n\nion-card img {\n  display: block;\n  width: 100%; }\n\n.card-md {\n  margin: 10px;\n  border-radius: 2px;\n  width: calc(100% - 20px);\n  font-family: \"Roboto\", \"Helvetica Neue\", sans-serif;\n  font-size: 14px;\n  color: var(--ion-text-md-color-step-150, var(--ion-text-color-step-150, #262626));\n  background-color: var(--ion-item-md-background-color, var(--ion-item-background-color, var(--ion-background-color, #fff)));\n  -webkit-box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);\n  box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12); }\n\n.card-md ion-list {\n  margin-bottom: 0; }\n\n.card-md > .item:last-child,\n.card-md > .item:last-child .item-inner,\n.card-md > .item-sliding:last-child .item {\n  border-bottom: 0; }\n\n.card-md .item-md .item-inner {\n  border: 0; }\n\n.card .note-md {\n  font-size: 13px; }\n\n.card-md h1 {\n  margin: 0 0 2px;\n  font-size: 24px;\n  font-weight: normal;\n  color: var(--ion-text-md-color-step-150, var(--ion-text-color-step-150, #262626)); }\n\n.card-md h2 {\n  margin: 2px 0;\n  font-size: 16px;\n  font-weight: normal;\n  color: var(--ion-text-md-color-step-150, var(--ion-text-color-step-150, #262626)); }\n\n.card-md h3,\n.card-md h4,\n.card-md h5,\n.card-md h6 {\n  margin: 2px 0;\n  font-size: 14px;\n  font-weight: normal;\n  color: var(--ion-text-md-color-step-150, var(--ion-text-color-step-150, #262626)); }\n\n.card-md p {\n  margin: 0 0 2px;\n  font-size: 14px;\n  font-weight: normal;\n  line-height: 1.5;\n  color: var(--ion-text-md-color-step-150, var(--ion-text-color-step-150, #262626)); }\n\n.card-md + ion-card {\n  margin-top: 0; }\n\n.card-md .text-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-primary {\n  color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-primary h1,\n  .card-md-primary h2,\n  .card-md-primary h3,\n  .card-md-primary h4,\n  .card-md-primary h5,\n  .card-md-primary h6,\n  .card-md-primary p {\n    color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n  .card-md-primary .text-md-primary {\n    color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-primary .text-md-secondary {\n    color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-primary .text-md-tertiary {\n    color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-primary .text-md-success {\n    color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-primary .text-md-warning {\n    color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-primary .text-md-danger {\n    color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-primary .text-md-light {\n    color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-primary .text-md-medium {\n    color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-primary .text-md-dark {\n    color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md .text-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-secondary {\n  color: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-secondary h1,\n  .card-md-secondary h2,\n  .card-md-secondary h3,\n  .card-md-secondary h4,\n  .card-md-secondary h5,\n  .card-md-secondary h6,\n  .card-md-secondary p {\n    color: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n  .card-md-secondary .text-md-primary {\n    color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-secondary .text-md-secondary {\n    color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-secondary .text-md-tertiary {\n    color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-secondary .text-md-success {\n    color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-secondary .text-md-warning {\n    color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-secondary .text-md-danger {\n    color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-secondary .text-md-light {\n    color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-secondary .text-md-medium {\n    color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-secondary .text-md-dark {\n    color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md .text-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-tertiary {\n  color: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-tertiary h1,\n  .card-md-tertiary h2,\n  .card-md-tertiary h3,\n  .card-md-tertiary h4,\n  .card-md-tertiary h5,\n  .card-md-tertiary h6,\n  .card-md-tertiary p {\n    color: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n  .card-md-tertiary .text-md-primary {\n    color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-tertiary .text-md-secondary {\n    color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-tertiary .text-md-tertiary {\n    color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-tertiary .text-md-success {\n    color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-tertiary .text-md-warning {\n    color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-tertiary .text-md-danger {\n    color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-tertiary .text-md-light {\n    color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-tertiary .text-md-medium {\n    color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-tertiary .text-md-dark {\n    color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md .text-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-success {\n  color: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-success h1,\n  .card-md-success h2,\n  .card-md-success h3,\n  .card-md-success h4,\n  .card-md-success h5,\n  .card-md-success h6,\n  .card-md-success p {\n    color: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff)); }\n  .card-md-success .text-md-primary {\n    color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-success .text-md-secondary {\n    color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-success .text-md-tertiary {\n    color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-success .text-md-success {\n    color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-success .text-md-warning {\n    color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-success .text-md-danger {\n    color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-success .text-md-light {\n    color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-success .text-md-medium {\n    color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-success .text-md-dark {\n    color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md .text-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-warning {\n  color: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-warning h1,\n  .card-md-warning h2,\n  .card-md-warning h3,\n  .card-md-warning h4,\n  .card-md-warning h5,\n  .card-md-warning h6,\n  .card-md-warning p {\n    color: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n  .card-md-warning .text-md-primary {\n    color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-warning .text-md-secondary {\n    color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-warning .text-md-tertiary {\n    color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-warning .text-md-success {\n    color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-warning .text-md-warning {\n    color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-warning .text-md-danger {\n    color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-warning .text-md-light {\n    color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-warning .text-md-medium {\n    color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-warning .text-md-dark {\n    color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md .text-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-danger {\n  color: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-danger h1,\n  .card-md-danger h2,\n  .card-md-danger h3,\n  .card-md-danger h4,\n  .card-md-danger h5,\n  .card-md-danger h6,\n  .card-md-danger p {\n    color: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n  .card-md-danger .text-md-primary {\n    color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-danger .text-md-secondary {\n    color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-danger .text-md-tertiary {\n    color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-danger .text-md-success {\n    color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-danger .text-md-warning {\n    color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-danger .text-md-danger {\n    color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-danger .text-md-light {\n    color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-danger .text-md-medium {\n    color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-danger .text-md-dark {\n    color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md .text-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-light {\n  color: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-light h1,\n  .card-md-light h2,\n  .card-md-light h3,\n  .card-md-light h4,\n  .card-md-light h5,\n  .card-md-light h6,\n  .card-md-light p {\n    color: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000)); }\n  .card-md-light .text-md-primary {\n    color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-light .text-md-secondary {\n    color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-light .text-md-tertiary {\n    color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-light .text-md-success {\n    color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-light .text-md-warning {\n    color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-light .text-md-danger {\n    color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-light .text-md-light {\n    color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-light .text-md-medium {\n    color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-light .text-md-dark {\n    color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md .text-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-medium {\n  color: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-medium h1,\n  .card-md-medium h2,\n  .card-md-medium h3,\n  .card-md-medium h4,\n  .card-md-medium h5,\n  .card-md-medium h6,\n  .card-md-medium p {\n    color: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n  .card-md-medium .text-md-primary {\n    color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-medium .text-md-secondary {\n    color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-medium .text-md-tertiary {\n    color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-medium .text-md-success {\n    color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-medium .text-md-warning {\n    color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-medium .text-md-danger {\n    color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-medium .text-md-light {\n    color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-medium .text-md-medium {\n    color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-medium .text-md-dark {\n    color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md .text-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-dark {\n  color: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n  .card-md-dark h1,\n  .card-md-dark h2,\n  .card-md-dark h3,\n  .card-md-dark h4,\n  .card-md-dark h5,\n  .card-md-dark h6,\n  .card-md-dark p {\n    color: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n  .card-md-dark .text-md-primary {\n    color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n  .card-md-dark .text-md-secondary {\n    color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-md-dark .text-md-tertiary {\n    color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-md-dark .text-md-success {\n    color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n  .card-md-dark .text-md-warning {\n    color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n  .card-md-dark .text-md-danger {\n    color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n  .card-md-dark .text-md-light {\n    color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n  .card-md-dark .text-md-medium {\n    color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n  .card-md-dark .text-md-dark {\n    color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "md"; }
}

class CardContent {
    static get is() { return "ion-card-content"; }
    static get host() { return {
        "theme": "card-content"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        }
    }; }
    static get style() { return "ion-card-content {\n  position: relative;\n  display: block; }\n\n.card-content-md {\n  padding: 13px 16px;\n  font-size: 14px;\n  line-height: 1.5; }\n\n.card-md-primary .card-content-md {\n  color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.card-md-primary .card-content-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-primary .card-content-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-primary .card-content-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-primary .card-content-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-primary .card-content-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-primary .card-content-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-primary .card-content-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-primary .card-content-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-primary .card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-md-primary {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-secondary .card-content-md {\n  color: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.card-md-secondary .card-content-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-secondary .card-content-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-secondary .card-content-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-secondary .card-content-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-secondary .card-content-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-secondary .card-content-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-secondary .card-content-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-secondary .card-content-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-secondary .card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-md-secondary {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-tertiary .card-content-md {\n  color: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.card-md-tertiary .card-content-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-tertiary .card-content-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-tertiary .card-content-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-tertiary .card-content-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-tertiary .card-content-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-tertiary .card-content-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-tertiary .card-content-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-tertiary .card-content-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-tertiary .card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-md-tertiary {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-success .card-content-md {\n  color: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.card-md-success .card-content-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-success .card-content-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-success .card-content-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-success .card-content-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-success .card-content-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-success .card-content-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-success .card-content-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-success .card-content-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-success .card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-md-success {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-warning .card-content-md {\n  color: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.card-md-warning .card-content-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-warning .card-content-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-warning .card-content-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-warning .card-content-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-warning .card-content-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-warning .card-content-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-warning .card-content-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-warning .card-content-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-warning .card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-md-warning {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-danger .card-content-md {\n  color: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.card-md-danger .card-content-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-danger .card-content-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-danger .card-content-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-danger .card-content-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-danger .card-content-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-danger .card-content-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-danger .card-content-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-danger .card-content-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-danger .card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-md-danger {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-light .card-content-md {\n  color: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.card-md-light .card-content-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-light .card-content-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-light .card-content-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-light .card-content-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-light .card-content-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-light .card-content-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-light .card-content-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-light .card-content-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-light .card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-md-light {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-medium .card-content-md {\n  color: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.card-md-medium .card-content-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-medium .card-content-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-medium .card-content-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-medium .card-content-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-medium .card-content-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-medium .card-content-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-medium .card-content-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-medium .card-content-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-medium .card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-md-medium {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-dark .card-content-md {\n  color: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.card-md-dark .card-content-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-dark .card-content-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-dark .card-content-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-dark .card-content-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-dark .card-content-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-dark .card-content-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-dark .card-content-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-dark .card-content-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-dark .card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "md"; }
}

class CardSubtitle {
    hostData() {
        return {
            'role': 'heading',
            'aria-level': '3'
        };
    }
    static get is() { return "ion-card-subtitle"; }
    static get host() { return {
        "theme": "card-subtitle"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        }
    }; }
    static get style() { return "ion-card-subtitle {\n  position: relative;\n  display: block; }\n\n.card-subtitle-md {\n  margin: 0 0 8px;\n  padding: 0;\n  font-size: 14px;\n  color: var(--ion-text-md-color-step-450, var(--ion-text-color-step-450, #737373)); }\n\n.card-md-primary .card-subtitle-md {\n  color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.card-md-primary .card-subtitle-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-primary .card-subtitle-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-primary .card-subtitle-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-primary .card-subtitle-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-primary .card-subtitle-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-primary .card-subtitle-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-primary .card-subtitle-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-primary .card-subtitle-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-primary .card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-md-primary {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-secondary .card-subtitle-md {\n  color: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.card-md-secondary .card-subtitle-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-secondary .card-subtitle-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-secondary .card-subtitle-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-secondary .card-subtitle-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-secondary .card-subtitle-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-secondary .card-subtitle-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-secondary .card-subtitle-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-secondary .card-subtitle-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-secondary .card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-md-secondary {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-tertiary .card-subtitle-md {\n  color: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.card-md-tertiary .card-subtitle-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-tertiary .card-subtitle-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-tertiary .card-subtitle-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-tertiary .card-subtitle-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-tertiary .card-subtitle-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-tertiary .card-subtitle-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-tertiary .card-subtitle-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-tertiary .card-subtitle-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-tertiary .card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-md-tertiary {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-success .card-subtitle-md {\n  color: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.card-md-success .card-subtitle-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-success .card-subtitle-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-success .card-subtitle-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-success .card-subtitle-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-success .card-subtitle-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-success .card-subtitle-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-success .card-subtitle-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-success .card-subtitle-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-success .card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-md-success {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-warning .card-subtitle-md {\n  color: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.card-md-warning .card-subtitle-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-warning .card-subtitle-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-warning .card-subtitle-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-warning .card-subtitle-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-warning .card-subtitle-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-warning .card-subtitle-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-warning .card-subtitle-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-warning .card-subtitle-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-warning .card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-md-warning {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-danger .card-subtitle-md {\n  color: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.card-md-danger .card-subtitle-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-danger .card-subtitle-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-danger .card-subtitle-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-danger .card-subtitle-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-danger .card-subtitle-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-danger .card-subtitle-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-danger .card-subtitle-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-danger .card-subtitle-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-danger .card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-md-danger {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-light .card-subtitle-md {\n  color: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.card-md-light .card-subtitle-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-light .card-subtitle-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-light .card-subtitle-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-light .card-subtitle-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-light .card-subtitle-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-light .card-subtitle-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-light .card-subtitle-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-light .card-subtitle-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-light .card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-md-light {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-medium .card-subtitle-md {\n  color: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.card-md-medium .card-subtitle-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-medium .card-subtitle-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-medium .card-subtitle-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-medium .card-subtitle-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-medium .card-subtitle-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-medium .card-subtitle-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-medium .card-subtitle-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-medium .card-subtitle-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-medium .card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-md-medium {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-md-dark .card-subtitle-md {\n  color: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.card-md-dark .card-subtitle-md-primary {\n  color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-md-dark .card-subtitle-md-secondary {\n  color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-md-dark .card-subtitle-md-tertiary {\n  color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-md-dark .card-subtitle-md-success {\n  color: var(--ion-color-md-success, var(--ion-color-success, #10dc60)); }\n\n.card-md-dark .card-subtitle-md-warning {\n  color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-md-dark .card-subtitle-md-danger {\n  color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141)); }\n\n.card-md-dark .card-subtitle-md-light {\n  color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-md-dark .card-subtitle-md-medium {\n  color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-md-dark .card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-md-dark {\n  color: var(--ion-color-md-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "md"; }
}

class TabButton {
    constructor() {
        this.keyFocus = false;
        this.selected = false;
    }
    componentDidLoad() {
        this.ionTabButtonDidLoad.emit();
    }
    componentDidUnload() {
        this.ionTabButtonDidUnload.emit();
    }
    onClick(ev) {
        if (!this.tab.disabled) {
            this.ionTabbarClick.emit(this.tab);
        }
        ev.stopPropagation();
        ev.preventDefault();
    }
    onKeyUp() {
        this.keyFocus = true;
    }
    onBlur() {
        this.keyFocus = false;
    }
    hostData() {
        const selected = this.selected;
        const tab = this.tab;
        const hasTitle = !!tab.label;
        const hasIcon = !!tab.icon;
        const hasTitleOnly = (hasTitle && !hasIcon);
        const hasIconOnly = (hasIcon && !hasTitle);
        const hasBadge = !!tab.badge;
        return {
            'role': 'tab',
            'id': tab.btnId,
            'aria-selected': selected,
            'hidden': !tab.show,
            class: {
                'tab-selected': selected,
                'has-title': hasTitle,
                'has-icon': hasIcon,
                'has-title-only': hasTitleOnly,
                'has-icon-only': hasIconOnly,
                'has-badge': hasBadge,
                'tab-btn-disabled': tab.disabled,
                'focused': this.keyFocus
            }
        };
    }
    render() {
        const tab = this.tab;
        const href = tab.href || '#';
        return [
            h("a", { href: href, class: "tab-cover", onKeyUp: this.onKeyUp.bind(this), onBlur: this.onBlur.bind(this) },
                tab.icon && h("ion-icon", { class: "tab-button-icon", name: tab.icon }),
                tab.label && h("span", { class: "tab-button-text" }, tab.label),
                tab.badge && h("ion-badge", { class: "tab-badge", color: tab.badgeStyle }, tab.badge),
                this.mode === 'md' && h("ion-ripple-effect", { tapClick: true }))
        ];
    }
    static get is() { return "ion-tab-button"; }
    static get properties() { return {
        "el": {
            "elementRef": true
        },
        "keyFocus": {
            "state": true
        },
        "selected": {
            "type": Boolean,
            "attr": "selected"
        },
        "tab": {
            "type": "Any",
            "attr": "tab"
        }
    }; }
    static get events() { return [{
            "name": "ionTabbarClick",
            "method": "ionTabbarClick",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionTabButtonDidLoad",
            "method": "ionTabButtonDidLoad",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionTabButtonDidUnload",
            "method": "ionTabButtonDidUnload",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "click",
            "method": "onClick"
        }]; }
    static get style() { return "ion-tab-button {\n  margin: 0;\n  text-align: center;\n  border-radius: 0;\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n  position: relative;\n  z-index: 0;\n  display: block;\n  overflow: hidden;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  height: 100%;\n  border: 0;\n  text-decoration: none;\n  background: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\nion-tab-button a {\n  text-decoration: none; }\n\n.tab-cover {\n  margin: 0;\n  padding: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n  -ms-flex-direction: column;\n  flex-direction: column;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n  -ms-flex-pack: justify;\n  justify-content: space-between;\n  width: 100%;\n  height: 100%;\n  border: 0;\n  color: inherit;\n  background: transparent;\n  cursor: pointer; }\n  .tab-cover:active, .tab-cover:focus {\n    outline: none; }\n\n.tab-btn-disabled {\n  pointer-events: none; }\n  .tab-btn-disabled > .tab-cover {\n    opacity: .4; }\n\n.tab-button-text,\n.tab-button-icon {\n  display: none;\n  overflow: hidden;\n  -webkit-align-self: center;\n  -ms-flex-item-align: center;\n  align-self: center;\n  min-width: 26px;\n  max-width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap; }\n\n.has-icon .tab-button-icon,\n.has-title .tab-button-text {\n  display: block; }\n\n.has-title-only .tab-button-text {\n  white-space: normal; }\n\n.layout-icon-start .tab-button {\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: row;\n  -ms-flex-direction: row;\n  flex-direction: row; }\n\n.layout-icon-end .tab-button {\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n  -webkit-flex-direction: row-reverse;\n  -ms-flex-direction: row-reverse;\n  flex-direction: row-reverse; }\n\n.layout-icon-bottom .tab-button {\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: reverse;\n  -webkit-flex-direction: column-reverse;\n  -ms-flex-direction: column-reverse;\n  flex-direction: column-reverse; }\n\n.layout-icon-start .tab-button,\n.layout-icon-end .tab-button,\n.layout-icon-hide .tab-button,\n.layout-title-hide .tab-button {\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center; }\n\n.layout-icon-hide .tab-button-icon,\n.layout-title-hide .tab-button-text {\n  display: none; }\n\n.tab-badge {\n  right: 4%;\n  top: 6%;\n  right: calc(50% - 50px);\n  padding: 1px 6px;\n  position: absolute;\n  height: auto;\n  font-size: 12px;\n  line-height: 16px; }\n\n.has-icon .tab-badge {\n  right: calc(50% - 30px); }\n\n.layout-icon-bottom .tab-badge,\n.layout-icon-start .tab-badge,\n.layout-icon-end .tab-badge {\n  right: calc(50% - 50px); }\n\n.tab-button-md {\n  max-width: 168px;\n  font-weight: normal;\n  color: var(--ion-tabbar-md-text-color, var(--ion-text-md-color-step-400, var(--ion-text-color-step-400, #666666)));\n  fill: var(--ion-tabbar-md-text-color, var(--ion-text-md-color-step-400, var(--ion-text-color-step-400, #666666))); }\n\n.tab-button-md.focused {\n  background: var(--ion-tabbar-md-background-color-focused, var(--ion-tabbar-background-color-focused, #dadada)); }\n\n.tab-button-md .tab-cover {\n  padding: 8px 12px 10px 12px;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex; }\n\n.scrollable .tab-button-md {\n  overflow: hidden;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1 0 auto;\n  -ms-flex: 1 0 auto;\n  flex: 1 0 auto; }\n\n.tab-button-md.tab-selected {\n  color: var(--ion-tabbar-md-text-color-active, var(--ion-tabbar-text-color-active, #488aff));\n  fill: var(--ion-tabbar-md-text-color-active, var(--ion-tabbar-text-color-active, #488aff)); }\n\n.placement-top .tab-button-md.tab-selected .tab-button-icon,\n.placement-top .tab-button-md.tab-selected .tab-button-text {\n  -webkit-transform: inherit;\n  transform: inherit; }\n\n.tab-button-md .tab-button-text {\n  margin: 0;\n  -webkit-transform-origin: center bottom;\n  transform-origin: center bottom;\n  font-size: 12px;\n  text-transform: none;\n  -webkit-transition: -webkit-transform 0.3s ease-in-out;\n  transition: -webkit-transform 0.3s ease-in-out;\n  transition: transform 0.3s ease-in-out;\n  transition: transform 0.3s ease-in-out, -webkit-transform 0.3s ease-in-out; }\n\n.tab-button-md.tab-selected .tab-button-text {\n  -webkit-transform: scale3d(1.16667, 1.16667, 1);\n  transform: scale3d(1.16667, 1.16667, 1);\n  -webkit-transition: -webkit-transform 0.3s ease-in-out;\n  transition: -webkit-transform 0.3s ease-in-out;\n  transition: transform 0.3s ease-in-out;\n  transition: transform 0.3s ease-in-out, -webkit-transform 0.3s ease-in-out; }\n\n.layout-icon-top .tab-button-md .has-icon .tab-button-text {\n  margin-bottom: -2px; }\n\n.layout-icon-bottom .tab-button-md .tab-button-text {\n  -webkit-transform-origin: center top;\n  transform-origin: center top;\n  margin-top: -2px; }\n\n.tab-button-md .tab-button-icon {\n  -webkit-transform-origin: center center;\n  transform-origin: center center;\n  width: 24px;\n  height: 24px;\n  font-size: 24px;\n  -webkit-transition: -webkit-transform 0.3s ease-in-out;\n  transition: -webkit-transform 0.3s ease-in-out;\n  transition: transform 0.3s ease-in-out;\n  transition: transform 0.3s ease-in-out, -webkit-transform 0.3s ease-in-out; }\n\n.tab-button-md.tab-selected .tab-button-icon {\n  -webkit-transform: translate3d(0,  -2px,  0);\n  transform: translate3d(0,  -2px,  0); }\n\n.layout-icon-end .tab-button-md.tab-selected .tab-button-icon {\n  -webkit-transform: translate3d(2px,  0,  0);\n  transform: translate3d(2px,  0,  0); }\n\n.layout-icon-bottom .tab-button-md.tab-selected .tab-button-icon {\n  -webkit-transform: translate3d(0,  2px,  0);\n  transform: translate3d(0,  2px,  0); }\n\n.layout-icon-start .tab-button-md.tab-selected .tab-button-icon {\n  -webkit-transform: translate3d(-2px,  0,  0);\n  transform: translate3d(-2px,  0,  0); }\n\n.layout-icon-hide .tab-button-md,\n.layout-title-hide .tab-button-md,\n.tab-button-md.icon-only,\n.tab-button-md.has-title-only {\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center; }"; }
    static get styleMode() { return "md"; }
}

class Tabbar {
    constructor() {
        this.canScrollLeft = false;
        this.canScrollRight = false;
        this.hidden = false;
        this.layout = 'icon-top';
        this.placement = 'bottom';
        this.scrollable = false;
        this.tabs = [];
        this.highlight = false;
        /**
         * If true, the tabbar will be translucent. Defaults to `false`.
         */
        this.translucent = false;
    }
    selectedTabChanged() {
        this.scrollable && this.scrollToSelectedButton();
        this.highlight && this.updateHighlight();
    }
    onKeyboardWillHide() {
        setTimeout(() => this.hidden = false, 50);
    }
    onKeyboardWillShow() {
        if (this.placement === 'bottom') {
            this.hidden = true;
        }
    }
    onResize() {
        this.highlight && this.updateHighlight();
    }
    onTabButtonLoad() {
        this.scrollable && this.updateBoundaries();
        this.highlight && this.updateHighlight();
    }
    analyzeTabs() {
        const tabs = Array.from(this.doc.querySelectorAll('ion-tab-button'));
        const scrollLeft = this.scrollEl.scrollLeft;
        const tabsWidth = this.scrollEl.clientWidth;
        let previous = undefined;
        let next = undefined;
        for (const tab of tabs) {
            const left = tab.offsetLeft;
            const right = left + tab.offsetWidth;
            if (left < scrollLeft) {
                previous = { tab, amount: left };
            }
            if (!next && right > (tabsWidth + scrollLeft)) {
                const amount = right - tabsWidth;
                next = { tab, amount };
            }
        }
        return { previous, next };
    }
    getSelectedButton() {
        return Array.from(this.el.querySelectorAll('ion-tab-button'))
            .find(btn => btn.selected);
    }
    scrollToSelectedButton() {
        if (!this.scrollEl) {
            return;
        }
        this.queue.read(() => {
            const activeTabButton = this.getSelectedButton();
            if (activeTabButton) {
                const scrollLeft = this.scrollEl.scrollLeft, tabsWidth = this.scrollEl.clientWidth, left = activeTabButton.offsetLeft, right = left + activeTabButton.offsetWidth;
                let amount = 0;
                if (right > (tabsWidth + scrollLeft)) {
                    amount = right - tabsWidth;
                }
                else if (left < scrollLeft) {
                    amount = left;
                }
                if (amount !== 0) {
                    this.queue.write(() => {
                        this.scrollEl.scrollToPoint(amount, 0, 250).then(() => {
                            this.updateBoundaries();
                        });
                    });
                }
            }
        });
    }
    scrollByTab(direction) {
        this.queue.read(() => {
            const { previous, next } = this.analyzeTabs();
            const info = direction === 'right' ? next : previous;
            const amount = info && info.amount;
            if (info && amount) {
                this.scrollEl.scrollToPoint(amount, 0, 250).then(() => {
                    this.updateBoundaries();
                });
            }
        });
    }
    updateBoundaries() {
        this.canScrollLeft = this.scrollEl.scrollLeft !== 0;
        this.canScrollRight = this.scrollEl.scrollLeft < (this.scrollEl.scrollWidth - this.scrollEl.offsetWidth);
    }
    updateHighlight() {
        if (!this.highlight) {
            return;
        }
        this.queue.read(() => {
            const btn = this.getSelectedButton();
            const highlight = this.el.querySelector('div.tabbar-highlight');
            if (btn && highlight) {
                highlight.style.transform = `translate3d(${btn.offsetLeft}px,0,0) scaleX(${btn.offsetWidth})`;
            }
        });
    }
    hostData() {
        const themedClasses = this.translucent ? createThemedClasses(this.mode, this.color, 'tabbar-translucent') : {};
        return {
            role: 'tablist',
            class: Object.assign({}, themedClasses, { [`layout-${this.layout}`]: true, [`placement-${this.placement}`]: true, 'tabbar-hidden': this.hidden, 'scrollable': this.scrollable })
        };
    }
    render() {
        const selectedTab = this.selectedTab;
        const ionTabbarHighlight = this.highlight ? h("div", { class: "animated tabbar-highlight" }) : null;
        const buttonClasses = createThemedClasses(this.mode, this.color, 'tab-button');
        const tabButtons = this.tabs.map(tab => h("ion-tab-button", { class: buttonClasses, tab: tab, selected: selectedTab === tab }));
        if (this.scrollable) {
            return [
                h("ion-button", { onClick: () => this.scrollByTab('left'), fill: "clear", class: { inactive: !this.canScrollLeft } },
                    h("ion-icon", { name: "arrow-dropleft" })),
                h("ion-scroll", { forceOverscroll: false, ref: (scrollEl) => this.scrollEl = scrollEl },
                    tabButtons,
                    ionTabbarHighlight),
                h("ion-button", { onClick: () => this.scrollByTab('right'), fill: "clear", class: { inactive: !this.canScrollRight } },
                    h("ion-icon", { name: "arrow-dropright" }))
            ];
        }
        else {
            return [
                ...tabButtons,
                ionTabbarHighlight
            ];
        }
    }
    static get is() { return "ion-tabbar"; }
    static get host() { return {
        "theme": "tabbar"
    }; }
    static get properties() { return {
        "canScrollLeft": {
            "state": true
        },
        "canScrollRight": {
            "state": true
        },
        "doc": {
            "context": "document"
        },
        "el": {
            "elementRef": true
        },
        "hidden": {
            "state": true
        },
        "highlight": {
            "type": Boolean,
            "attr": "highlight"
        },
        "layout": {
            "type": String,
            "attr": "layout"
        },
        "placement": {
            "type": String,
            "attr": "placement"
        },
        "queue": {
            "context": "queue"
        },
        "scrollable": {
            "type": Boolean,
            "attr": "scrollable"
        },
        "selectedTab": {
            "type": "Any",
            "attr": "selected-tab",
            "watchCallbacks": ["selectedTabChanged"]
        },
        "tabs": {
            "type": "Any",
            "attr": "tabs"
        },
        "translucent": {
            "type": Boolean,
            "attr": "translucent"
        }
    }; }
    static get listeners() { return [{
            "name": "body:keyboardWillHide",
            "method": "onKeyboardWillHide"
        }, {
            "name": "body:keyboardWillShow",
            "method": "onKeyboardWillShow"
        }, {
            "name": "window:resize",
            "method": "onResize",
            "passive": true
        }, {
            "name": "ionTabButtonDidLoad",
            "method": "onTabButtonLoad"
        }, {
            "name": "ionTabButtonDidUnload",
            "method": "onTabButtonLoad"
        }]; }
    static get style() { return "ion-tabbar {\n  position: relative;\n  z-index: 10;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  -webkit-box-ordinal-group: 2;\n  -webkit-order: 1;\n  -ms-flex-order: 1;\n  order: 1;\n  width: 100%; }\n\nion-tabbar.tabbar-hidden {\n  display: none; }\n\nion-tabbar.placement-top {\n  -webkit-box-ordinal-group: 0;\n  -webkit-order: -1;\n  -ms-flex-order: -1;\n  order: -1; }\n\n.tabbar-highlight {\n  left: 0;\n  bottom: 0;\n  -webkit-transform-origin: 0 0;\n  transform-origin: 0 0;\n  position: absolute;\n  display: block;\n  width: 1px;\n  height: 2px;\n  -webkit-transform: translateZ(0);\n  transform: translateZ(0); }\n  .tabbar-highlight.animated {\n    -webkit-transition-duration: 300ms;\n    transition-duration: 300ms;\n    -webkit-transition-property: -webkit-transform;\n    transition-property: -webkit-transform;\n    transition-property: transform;\n    transition-property: transform, -webkit-transform;\n    -webkit-transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);\n    transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);\n    will-change: transform; }\n\n.placement-top .tabbar-highlight {\n  bottom: 0; }\n\n.placement-bottom .tabbar-highlight {\n  top: 0; }\n\nion-tabbar.scrollable ion-scroll {\n  overflow: hidden; }\n\nion-tabbar.scrollable .scroll-inner {\n  position: relative;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: row;\n  -ms-flex-direction: row;\n  flex-direction: row; }\n\nion-tabbar.scrollable ion-button.inactive {\n  visibility: hidden; }\n\n.tabbar-md {\n  height: 56px;\n  border-top: 1px solid rgba(var(--ion-tabbar-md-border-color-rgb, var(--ion-tabbar-border-color-rgb, 0, 0, 0)), 0.07);\n  background: var(--ion-tabbar-md-background-color, var(--ion-tabbar-background-color, #f8f8f8));\n  contain: strict; }\n\n.tabbar-md .tabbar-highlight {\n  background: var(--ion-tabbar-md-text-color-active, var(--ion-tabbar-text-color-active, #488aff)); }\n\n.tabbar-md.scrollable ion-scroll {\n  margin: 0 8px;\n  max-width: 650px; }\n\n.tabbar-md-primary {\n  color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff));\n  fill: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.tabbar-md-primary .tab-button.focused {\n  background: var(--ion-color-md-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n\n.enable-hover .tabbar-md-primary .tab-button:hover,\n.tabbar-md-primary .tab-button.tab-selected {\n  color: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  fill: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.tabbar-md-primary .tabbar-highlight {\n  background: var(--ion-color-md-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.tabbar-md-secondary {\n  color: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-md-secondary, var(--ion-color-secondary, #0cd1e8));\n  fill: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.tabbar-md-secondary .tab-button.focused {\n  background: var(--ion-color-md-secondary-shade, var(--ion-color-secondary-shade, #0bb8cc)); }\n\n.enable-hover .tabbar-md-secondary .tab-button:hover,\n.tabbar-md-secondary .tab-button.tab-selected {\n  color: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  fill: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.tabbar-md-secondary .tabbar-highlight {\n  background: var(--ion-color-md-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.tabbar-md-tertiary {\n  color: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-md-tertiary, var(--ion-color-tertiary, #7044ff));\n  fill: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.tabbar-md-tertiary .tab-button.focused {\n  background: var(--ion-color-md-tertiary-shade, var(--ion-color-tertiary-shade, #633ce0)); }\n\n.enable-hover .tabbar-md-tertiary .tab-button:hover,\n.tabbar-md-tertiary .tab-button.tab-selected {\n  color: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  fill: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.tabbar-md-tertiary .tabbar-highlight {\n  background: var(--ion-color-md-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.tabbar-md-success {\n  color: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-md-success, var(--ion-color-success, #10dc60));\n  fill: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.tabbar-md-success .tab-button.focused {\n  background: var(--ion-color-md-success-shade, var(--ion-color-success-shade, #0ec254)); }\n\n.enable-hover .tabbar-md-success .tab-button:hover,\n.tabbar-md-success .tab-button.tab-selected {\n  color: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff));\n  fill: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.tabbar-md-success .tabbar-highlight {\n  background: var(--ion-color-md-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.tabbar-md-warning {\n  color: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-md-warning, var(--ion-color-warning, #ffce00));\n  fill: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.tabbar-md-warning .tab-button.focused {\n  background: var(--ion-color-md-warning-shade, var(--ion-color-warning-shade, #e0b500)); }\n\n.enable-hover .tabbar-md-warning .tab-button:hover,\n.tabbar-md-warning .tab-button.tab-selected {\n  color: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000));\n  fill: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.tabbar-md-warning .tabbar-highlight {\n  background: var(--ion-color-md-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.tabbar-md-danger {\n  color: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-md-danger, var(--ion-color-danger, #f04141));\n  fill: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.tabbar-md-danger .tab-button.focused {\n  background: var(--ion-color-md-danger-shade, var(--ion-color-danger-shade, #d33939)); }\n\n.enable-hover .tabbar-md-danger .tab-button:hover,\n.tabbar-md-danger .tab-button.tab-selected {\n  color: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  fill: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.tabbar-md-danger .tabbar-highlight {\n  background: var(--ion-color-md-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.tabbar-md-light {\n  color: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-md-light, var(--ion-color-light, #f4f5f8));\n  fill: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.tabbar-md-light .tab-button.focused {\n  background: var(--ion-color-md-light-shade, var(--ion-color-light-shade, #d7d8da)); }\n\n.enable-hover .tabbar-md-light .tab-button:hover,\n.tabbar-md-light .tab-button.tab-selected {\n  color: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000));\n  fill: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.tabbar-md-light .tabbar-highlight {\n  background: var(--ion-color-md-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.tabbar-md-medium {\n  color: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-md-medium, var(--ion-color-medium, #989aa2));\n  fill: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.tabbar-md-medium .tab-button.focused {\n  background: var(--ion-color-md-medium-shade, var(--ion-color-medium-shade, #86888f)); }\n\n.enable-hover .tabbar-md-medium .tab-button:hover,\n.tabbar-md-medium .tab-button.tab-selected {\n  color: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000));\n  fill: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.tabbar-md-medium .tabbar-highlight {\n  background: var(--ion-color-md-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.tabbar-md-dark {\n  color: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-md-dark, var(--ion-color-dark, #222428));\n  fill: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.tabbar-md-dark .tab-button.focused {\n  background: var(--ion-color-md-dark-shade, var(--ion-color-dark-shade, #1e2023)); }\n\n.enable-hover .tabbar-md-dark .tab-button:hover,\n.tabbar-md-dark .tab-button.tab-selected {\n  color: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  fill: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.tabbar-md-dark .tabbar-highlight {\n  background: var(--ion-color-md-dark-contrast, var(--ion-color-dark-contrast, #fff)); }"; }
    static get styleMode() { return "md"; }
}

class Tabs {
    constructor() {
        this.ids = -1;
        this.transitioning = false;
        this.tabsId = (++tabIds);
        this.tabs = [];
        /**
         * If true, the tabbar
         */
        this.tabbarHidden = false;
        /**
         * If true, the tabs will be translucent.
         * Note: In order to scroll content behind the tabs, the `fullscreen`
         * attribute needs to be set on the content.
         * Defaults to `false`.
         */
        this.translucent = false;
        this.scrollable = false;
        this.useRouter = false;
    }
    componentWillLoad() {
        if (!this.useRouter) {
            this.useRouter = !!this.doc.querySelector('ion-router') && !this.el.closest('[no-router]');
        }
        this.loadConfig('tabbarLayout', 'bottom');
        this.loadConfig('tabbarLayout', 'icon-top');
        this.loadConfig('tabbarHighlight', false);
    }
    async componentDidLoad() {
        await this.initTabs();
        await this.initSelect();
    }
    componentDidUnload() {
        this.tabs.length = 0;
        this.selectedTab = this.leavingTab = undefined;
    }
    tabChange(ev) {
        const selectedTab = ev.detail;
        if (this.useRouter && selectedTab.href != null) {
            const router = this.doc.querySelector('ion-router');
            if (router) {
                router.push(selectedTab.href);
            }
            return;
        }
        this.select(selectedTab);
    }
    /**
     * @param {number|Tab} tabOrIndex Index, or the Tab instance, of the tab to select.
     */
    async select(tabOrIndex) {
        const selectedTab = this.getTab(tabOrIndex);
        if (!this.shouldSwitch(selectedTab)) {
            return false;
        }
        await this.setActive(selectedTab);
        await this.notifyRouter();
        this.tabSwitch();
        return true;
    }
    async setRouteId(id) {
        const selectedTab = this.getTab(id);
        if (!this.shouldSwitch(selectedTab)) {
            return { changed: false, element: this.selectedTab };
        }
        await this.setActive(selectedTab);
        return {
            changed: true,
            element: this.selectedTab,
            markVisible: () => this.tabSwitch(),
        };
    }
    getRouteId() {
        const id = this.selectedTab && this.selectedTab.getTabId();
        return id ? { id, element: this.selectedTab } : undefined;
    }
    getTab(tabOrIndex) {
        if (typeof tabOrIndex === 'string') {
            return this.tabs.find(tab => tab.getTabId() === tabOrIndex);
        }
        if (typeof tabOrIndex === 'number') {
            return this.tabs[tabOrIndex];
        }
        return tabOrIndex;
    }
    /**
     * @return {HTMLIonTabElement} Returns the currently selected tab
     */
    getSelected() {
        return this.selectedTab;
    }
    initTabs() {
        const tabs = this.tabs = Array.from(this.el.querySelectorAll('ion-tab'));
        const tabPromises = tabs.map(tab => {
            const id = `t-${this.tabsId}-${++this.ids}`;
            tab.btnId = 'tab-' + id;
            tab.id = 'tabpanel-' + id;
            return tab.componentOnReady();
        });
        return Promise.all(tabPromises);
    }
    async initSelect() {
        if (this.useRouter) {
            return;
        }
        // find pre-selected tabs
        const selectedTab = this.tabs.find(t => t.selected) ||
            this.tabs.find(t => t.show && !t.disabled);
        // reset all tabs none is selected
        for (const tab of this.tabs) {
            if (tab !== selectedTab) {
                tab.selected = false;
            }
        }
        if (selectedTab) {
            await selectedTab.setActive();
        }
        this.selectedTab = selectedTab;
        if (selectedTab) {
            selectedTab.selected = true;
            selectedTab.active = true;
        }
    }
    loadConfig(attrKey, fallback) {
        const val = this[attrKey];
        if (typeof val === 'undefined') {
            this[attrKey] = this.config.get(attrKey, fallback);
        }
    }
    setActive(selectedTab) {
        if (this.transitioning) {
            return Promise.reject('transitioning already happening');
        }
        if (!selectedTab) {
            return Promise.reject('no tab is selected');
        }
        // Reset rest of tabs
        for (const tab of this.tabs) {
            if (selectedTab !== tab) {
                tab.selected = false;
            }
        }
        this.transitioning = true;
        this.leavingTab = this.selectedTab;
        this.selectedTab = selectedTab;
        this.ionNavWillChange.emit();
        return selectedTab.setActive();
    }
    tabSwitch() {
        const selectedTab = this.selectedTab;
        const leavingTab = this.leavingTab;
        this.leavingTab = undefined;
        this.transitioning = false;
        if (!selectedTab) {
            return;
        }
        selectedTab.selected = true;
        if (leavingTab !== selectedTab) {
            if (leavingTab) {
                leavingTab.active = false;
            }
            this.ionChange.emit({ tab: selectedTab });
            this.ionNavDidChange.emit();
        }
    }
    notifyRouter() {
        if (this.useRouter) {
            const router = this.doc.querySelector('ion-router');
            if (router) {
                return router.navChanged(1 /* Forward */);
            }
        }
        return Promise.resolve(false);
    }
    shouldSwitch(selectedTab) {
        const leavingTab = this.selectedTab;
        return !!(selectedTab && selectedTab !== leavingTab && !this.transitioning);
    }
    render() {
        const dom = [
            h("div", { class: "tabs-inner" },
                h("slot", null))
        ];
        if (!this.tabbarHidden) {
            dom.push(h("ion-tabbar", { tabs: this.tabs, color: this.color, selectedTab: this.selectedTab, highlight: this.tabbarHighlight, placement: this.tabbarPlacement, layout: this.tabbarLayout, translucent: this.translucent, scrollable: this.scrollable }));
        }
        return dom;
    }
    static get is() { return "ion-tabs"; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "config": {
            "context": "config"
        },
        "doc": {
            "context": "document"
        },
        "el": {
            "elementRef": true
        },
        "getRouteId": {
            "method": true
        },
        "getSelected": {
            "method": true
        },
        "getTab": {
            "method": true
        },
        "name": {
            "type": String,
            "attr": "name"
        },
        "scrollable": {
            "type": Boolean,
            "attr": "scrollable"
        },
        "select": {
            "method": true
        },
        "selectedTab": {
            "state": true
        },
        "setRouteId": {
            "method": true
        },
        "tabbarHidden": {
            "type": Boolean,
            "attr": "tabbar-hidden"
        },
        "tabbarHighlight": {
            "type": Boolean,
            "attr": "tabbar-highlight",
            "mutable": true
        },
        "tabbarLayout": {
            "type": String,
            "attr": "tabbar-layout",
            "mutable": true
        },
        "tabbarPlacement": {
            "type": String,
            "attr": "tabbar-placement",
            "mutable": true
        },
        "tabs": {
            "state": true
        },
        "translucent": {
            "type": Boolean,
            "attr": "translucent"
        },
        "useRouter": {
            "type": Boolean,
            "attr": "use-router",
            "mutable": true
        }
    }; }
    static get events() { return [{
            "name": "ionChange",
            "method": "ionChange",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionNavWillChange",
            "method": "ionNavWillChange",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionNavDidChange",
            "method": "ionNavDidChange",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "ionTabbarClick",
            "method": "tabChange"
        }]; }
    static get style() { return "ion-tabs {\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  position: absolute;\n  z-index: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n  -ms-flex-direction: column;\n  flex-direction: column;\n  width: 100%;\n  height: 100%;\n  contain: layout size style; }\n\n.tabs-inner {\n  position: relative;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  contain: layout size style; }"; }
}
let tabIds = -1;

export { AppHeader, Badge as IonBadge, Button as IonButton, Card as IonCard, CardContent as IonCardContent, CardSubtitle as IonCardSubtitle, TabButton as IonTabButton, Tabbar as IonTabbar, Tabs as IonTabs };
