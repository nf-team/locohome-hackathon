/*! Built with http://stenciljs.com */
const { h } = window.App;

import { a as createThemedClasses, c as getElementClassMap, d as openURL } from './chunk-e901a817.js';

function urlB64ToUint8Array(base64String) {
    const padding = '='.repeat((4 - base64String.length % 4) % 4);
    const base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');
    const rawData = window.atob(base64);
    const outputArray = new Uint8Array(rawData.length);
    for (let i = 0; i < rawData.length; ++i) {
        outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
}

class AppProfile {
    constructor() {
        // demo key from https://web-push-codelab.glitch.me/
        // replace with your key in production
        this.publicServerKey = urlB64ToUint8Array('BBsb4au59pTKF4IKi-aJkEAGPXxtzs-lbtL58QxolsT2T-3dVQIXTUCCE1TSY8hyUvXLhJFEUmH7b5SJfSTcT-E');
    }
    componentWillLoad() {
        if ('serviceWorker' in navigator && 'PushManager' in window) {
            this.swSupport = true;
        }
        else {
            this.swSupport = false;
        }
    }
    subscribeToNotify($event) {
        console.log($event.detail.checked);
        if ($event.detail.checked === true) {
            this.handleSub();
        }
    }
    handleSub() {
        // get our service worker registration
        navigator.serviceWorker.getRegistration().then((reg) => {
            // check if service worker is registered
            if (reg) {
                // get push subscription
                reg.pushManager.getSubscription().then((sub) => {
                    // if there is no subscription that means
                    // the user has not subscribed before
                    if (sub === null) {
                        // user is not subscribed
                        reg.pushManager.subscribe({
                            userVisibleOnly: true,
                            applicationServerKey: this.publicServerKey
                        })
                            .then((sub) => {
                            // our user is now subscribed
                            // lets reflect this in our UI
                            console.log('web push subscription: ', sub);
                            this.notify = true;
                        });
                    }
                });
            }
        });
    }
    render() {
        return [
            h("ion-header", null,
                h("ion-toolbar", { color: 'primary' },
                    h("ion-buttons", { slot: "start" },
                        h("ion-back-button", { defaultHref: '/' })),
                    h("ion-title", null, "SkyQR"))),
            h("ion-content", null,
                h("qr-scanner", null))
        ];
    }
    static get is() { return "app-profile"; }
    static get properties() { return {
        "name": {
            "type": String,
            "attr": "name"
        },
        "notify": {
            "state": true
        },
        "swSupport": {
            "state": true
        },
        "toastCtrl": {
            "connect": "ion-toast-controller"
        }
    }; }
    static get listeners() { return [{
            "name": "ionChange",
            "method": "subscribeToNotify"
        }]; }
    static get style() { return "app-profile ion-scroll {\n  padding: 15px; }"; }
}

class QRReader {
    constructor(elements) {
        this.captureConstraints = {
            video: {
                facingMode: { exact: 'environment' },
                mandatory: {
                    width: 320,
                    height: 240
                }
            },
            audio: false
        };
        this.baseurl = '/';
        this.workerFilename = 'decoder.min.js';
        this.active = false;
        this.streaming = false;
        this.canvas = elements.canvas;
        this.webcam = elements.video;
    }
    setCanvas() {
        this.ctx = this.canvas.getContext('2d');
    }
    setCanvasProperties() {
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
    }
    async startCapture(constraints = this.captureConstraints) {
        let stream = await navigator.mediaDevices.getUserMedia(constraints);
        this.webcam.srcObject = stream;
        this.webcam.play();
    }
    async init() {
        this.decoder = new Worker(this.baseurl + this.workerFilename);
        this.setCanvas();
        this.setCanvasProperties();
        this.webcam.addEventListener('play', () => {
            if (!this.streaming) {
                this.setCanvasProperties();
                this.streaming = true;
            }
        }, false);
        await this.startCapture();
    }
    destroy() {
        this.decoder.terminate();
        this.active = false;
        this.webcam.pause();
    }
    scan(callback) {
        this.active = true;
        let onDecoderMessage = async (e) => {
            if (e.data.length > 0) {
                let qrid = e.data[0][2];
                await callback(qrid);
            }
            setTimeout(newDecoderFrame, 0);
        };
        this.decoder.onmessage = onDecoderMessage;
        let newDecoderFrame = () => {
            if (!this.active)
                return;
            try {
                this.ctx.drawImage(this.webcam, 0, 0, this.canvas.width, this.canvas.height);
                let imgData = this.ctx.getImageData(0, 0, this.canvas.width, this.canvas.height);
                if (imgData.data) {
                    this.decoder.postMessage(imgData);
                }
            }
            catch (err) {
                if (err.name == 'NS_ERROR_NOT_AVAILABLE')
                    setTimeout(newDecoderFrame, 0);
            }
        };
        newDecoderFrame();
    }
}

class QRScanner {
    constructor() {
        this.elemId = Math.floor(Math.random() * 1000 * 1000 * 1000).toString(32);
    }
    setComponentElement() {
        this.element = document.getElementById(this.elemId).parentElement;
    }
    async componentDidLoad() {
        this.setComponentElement();
        console.log(this.element);
        this.qrReader = new QRReader({
            video: this.element.querySelector('video'),
            canvas: document.createElement('canvas')
        });
        await this.qrReader.init();
        let processResult = async (data) => {
            console.log(data);
            // if (data.indexOf('https://skyqr.com') != 0) {
            //   console.warn('Wrong code')
            //   this.qr.scan(processResult);
            //   return;
            // }
            // this.$router.push(data.replace("https://skyqr.com", ""));
        };
        this.qrReader.scan(processResult);
    }
    componentDidUnload() {
        console.log('unload');
        this.qrReader.destroy();
    }
    render() {
        return h("div", { id: this.elemId },
            h("video", { playsinline: true, autoplay: true }),
            h("div", { class: 'overflow' },
                h("img", { src: '/assets/qr_code.png' }),
                h("div", { class: 'text' }, "Scan QR code")));
    }
    static get is() { return "qr-scanner"; }
    static get style() { return "qr-scanner {\n  -webkit-box-flex: 1;\n  -webkit-flex: 1 1;\n  -ms-flex: 1 1;\n  flex: 1 1;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n  -ms-flex-direction: column;\n  flex-direction: column;\n  width: 100%;\n  min-height: 100%; }\n  qr-scanner div {\n    -webkit-box-flex: 1;\n    -webkit-flex: 1 1;\n    -ms-flex: 1 1;\n    flex: 1 1; }\n    qr-scanner div video {\n      position: absolute;\n      top: 50%;\n      left: 50%;\n      min-width: 100vw;\n      min-height: 100vh;\n      -webkit-transform: translate(-50%, -50%) scale(1.1);\n      transform: translate(-50%, -50%) scale(1.1); }\n    qr-scanner div .overflow {\n      position: absolute;\n      top: 50%;\n      left: 50%;\n      -webkit-transform: translate(-50%, -50%);\n      transform: translate(-50%, -50%);\n      margin: auto;\n      opacity: .8;\n      width: 100%;\n      text-align: center;\n      color: white;\n      font-weight: 200;\n      text-transform: uppercase; }\n      qr-scanner div .overflow img {\n        width: 35%;\n        margin-bottom: 1.2rem; }"; }
}

class BackButton {
    onClick(ev) {
        const nav = this.el.closest('ion-nav');
        if (nav && nav.canGoBack()) {
            ev.preventDefault();
            nav.pop();
        }
        else if (this.defaultHref) {
            openURL(this.win, this.defaultHref, ev, 'back');
        }
    }
    hostData() {
        return {
            class: {
                'show-back-button': !!this.defaultHref
            }
        };
    }
    render() {
        const backButtonIcon = this.icon || this.config.get('backButtonIcon', 'arrow-back');
        const backButtonText = this.text != null ? this.text : this.config.get('backButtonText', 'Back');
        const themedClasses = createThemedClasses(this.mode, this.color, 'back-button');
        const backButtonClasses = Object.assign({}, themedClasses, getElementClassMap(this.el.classList));
        return (h("button", { class: backButtonClasses, onClick: (ev) => this.onClick(ev) },
            h("span", { class: "back-button-inner" },
                backButtonIcon && h("ion-icon", { name: backButtonIcon }),
                this.mode === 'ios' && backButtonText && h("span", { class: "button-text" }, backButtonText),
                this.mode === 'md' && h("ion-ripple-effect", { tapClick: true }))));
    }
    static get is() { return "ion-back-button"; }
    static get host() { return {
        "theme": "back-button"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "config": {
            "context": "config"
        },
        "defaultHref": {
            "type": String,
            "attr": "default-href"
        },
        "el": {
            "elementRef": true
        },
        "icon": {
            "type": String,
            "attr": "icon"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        },
        "text": {
            "type": String,
            "attr": "text"
        },
        "win": {
            "context": "window"
        }
    }; }
    static get style() { return ".back-button {\n  display: none; }\n\n.can-go-back > ion-header .back-button,\n.back-button.show-back-button {\n  display: inline-block; }\n\n.back-button button {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  text-align: center;\n  -moz-appearance: none;\n  -ms-appearance: none;\n  -webkit-appearance: none;\n  appearance: none;\n  position: relative;\n  z-index: 0;\n  display: -webkit-inline-box;\n  display: -webkit-inline-flex;\n  display: -ms-inline-flexbox;\n  display: inline-flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -webkit-flex-flow: row nowrap;\n  -ms-flex-flow: row nowrap;\n  flex-flow: row nowrap;\n  -webkit-flex-shrink: 0;\n  -ms-flex-negative: 0;\n  flex-shrink: 0;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  border: 0;\n  outline: none;\n  line-height: 1;\n  text-decoration: none;\n  text-overflow: ellipsis;\n  text-transform: none;\n  white-space: nowrap;\n  cursor: pointer;\n  vertical-align: top;\n  vertical-align: -webkit-baseline-middle;\n  -webkit-transition: background-color, opacity 100ms linear;\n  transition: background-color, opacity 100ms linear;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  -webkit-font-kerning: none;\n  font-kerning: none; }\n\n.back-button-inner {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -webkit-flex-flow: row nowrap;\n  -ms-flex-flow: row nowrap;\n  flex-flow: row nowrap;\n  -webkit-flex-shrink: 0;\n  -ms-flex-negative: 0;\n  flex-shrink: 0;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  width: 100%;\n  height: 100%; }\n\n.back-button-text {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center; }\n\n.back-button-ios {\n  padding: 0;\n  margin: 2px 0 0;\n  z-index: 99;\n  overflow: visible;\n  min-height: 32px;\n  border: 0;\n  font-size: 17px;\n  line-height: 1;\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  background-color: transparent;\n  -webkit-transform: translateZ(0);\n  transform: translateZ(0); }\n  .back-button-ios.activated {\n    opacity: .4; }\n\n.back-button-ios ion-icon {\n  padding: 0;\n  margin: 0 -5px 0 -4px;\n  display: inherit;\n  font-size: 1.85em;\n  pointer-events: none; }\n\n.back-button-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.back-button-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.back-button-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.back-button-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.back-button-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.back-button-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.back-button-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.back-button-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.back-button-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "ios"; }
}

export { AppProfile, QRScanner as QrScanner, BackButton as IonBackButton };
