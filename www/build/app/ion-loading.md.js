/*! Built with http://stenciljs.com */
const { h } = window.App;

import { a as createThemedClasses, b as getClassMap } from './chunk-e901a817.js';
import { a as BACKDROP, b as dismiss, c as eventMethod, e as present } from './chunk-3b0e1c7b.js';

/**
 * iOS Loading Enter Animation
 */
function iosEnterAnimation(Animation, baseEl) {
    const baseAnimation = new Animation();
    const backdropAnimation = new Animation();
    backdropAnimation.addElement(baseEl.querySelector('ion-backdrop'));
    const wrapperAnimation = new Animation();
    wrapperAnimation.addElement(baseEl.querySelector('.loading-wrapper'));
    backdropAnimation.fromTo('opacity', 0.01, 0.3);
    wrapperAnimation.fromTo('opacity', 0.01, 1)
        .fromTo('scale', 1.1, 1);
    return Promise.resolve(baseAnimation
        .addElement(baseEl)
        .easing('ease-in-out')
        .duration(200)
        .add(backdropAnimation)
        .add(wrapperAnimation));
}

/**
 * iOS Loading Leave Animation
 */
function iosLeaveAnimation(Animation, baseEl) {
    const baseAnimation = new Animation();
    const backdropAnimation = new Animation();
    backdropAnimation.addElement(baseEl.querySelector('ion-backdrop'));
    const wrapperAnimation = new Animation();
    wrapperAnimation.addElement(baseEl.querySelector('.loading-wrapper'));
    backdropAnimation.fromTo('opacity', 0.3, 0);
    wrapperAnimation.fromTo('opacity', 0.99, 0)
        .fromTo('scale', 1, 0.9);
    return Promise.resolve(baseAnimation
        .addElement(baseEl)
        .easing('ease-in-out')
        .duration(200)
        .add(backdropAnimation)
        .add(wrapperAnimation));
}

/**
 * Md Loading Enter Animation
 */
function mdEnterAnimation(Animation, baseEl) {
    const baseAnimation = new Animation();
    const backdropAnimation = new Animation();
    backdropAnimation.addElement(baseEl.querySelector('ion-backdrop'));
    const wrapperAnimation = new Animation();
    wrapperAnimation.addElement(baseEl.querySelector('.loading-wrapper'));
    backdropAnimation.fromTo('opacity', 0.01, 0.5);
    wrapperAnimation.fromTo('opacity', 0.01, 1).fromTo('scale', 1.1, 1);
    return Promise.resolve(baseAnimation
        .addElement(baseEl)
        .easing('ease-in-out')
        .duration(200)
        .add(backdropAnimation)
        .add(wrapperAnimation));
}

/**
 * Md Loading Leave Animation
 */
function mdLeaveAnimation(Animation, baseEl) {
    const baseAnimation = new Animation();
    const backdropAnimation = new Animation();
    backdropAnimation.addElement(baseEl.querySelector('ion-backdrop'));
    const wrapperAnimation = new Animation();
    wrapperAnimation.addElement(baseEl.querySelector('.loading-wrapper'));
    backdropAnimation.fromTo('opacity', 0.5, 0);
    wrapperAnimation.fromTo('opacity', 0.99, 0).fromTo('scale', 1, 0.9);
    return Promise.resolve(baseAnimation
        .addElement(baseEl)
        .easing('ease-in-out')
        .duration(200)
        .add(backdropAnimation)
        .add(wrapperAnimation));
}

class Loading {
    constructor() {
        this.presented = false;
        this.keyboardClose = true;
        /**
         * If true, the loading indicator will dismiss when the page changes. Defaults to `false`.
         */
        this.dismissOnPageChange = false;
        /**
         * If true, the loading indicator will be dismissed when the backdrop is clicked. Defaults to `false`.
         */
        this.enableBackdropDismiss = false;
        /**
         * If true, a backdrop will be displayed behind the loading indicator. Defaults to `true`.
         */
        this.showBackdrop = true;
        /**
         * If true, the loading indicator will be translucent. Defaults to `false`.
         */
        this.translucent = false;
        /**
         * If true, the loading indicator will animate. Defaults to `true`.
         */
        this.willAnimate = true;
    }
    componentWillLoad() {
        if (!this.spinner) {
            this.spinner = this.config.get('loadingSpinner', this.mode === 'ios' ? 'lines' : 'crescent');
        }
    }
    componentDidLoad() {
        this.ionLoadingDidLoad.emit();
    }
    componentDidUnload() {
        this.ionLoadingDidUnload.emit();
    }
    onBackdropTap() {
        this.dismiss(null, BACKDROP);
    }
    /**
     * Present the loading overlay after it has been created.
     */
    async present() {
        await present(this, 'loadingEnter', iosEnterAnimation, mdEnterAnimation, undefined);
        if (this.duration) {
            this.durationTimeout = setTimeout(() => this.dismiss(), this.duration + 10);
        }
    }
    /**
     * Dismiss the loading overlay after it has been presented.
     */
    dismiss(data, role) {
        if (this.durationTimeout) {
            clearTimeout(this.durationTimeout);
        }
        return dismiss(this, data, role, 'loadingLeave', iosLeaveAnimation, mdLeaveAnimation);
    }
    /**
     * Returns a promise that resolves when the loading did dismiss. It also accepts a callback
     * that is called in the same circustances.
     *
     * ```
     * const {data, role} = await loading.onDidDismiss();
     * ```
     */
    onDidDismiss(callback) {
        return eventMethod(this.el, 'ionLoadingDidDismiss', callback);
    }
    /**
     * Returns a promise that resolves when the loading will dismiss. It also accepts a callback
     * that is called in the same circustances.
     *
     * ```
     * const {data, role} = await loading.onWillDismiss();
     * ```
     */
    onWillDismiss(callback) {
        return eventMethod(this.el, 'ionLoadingWillDismiss', callback);
    }
    hostData() {
        const themedClasses = this.translucent ? createThemedClasses(this.mode, this.color, 'loading-translucent') : {};
        return {
            style: {
                zIndex: 20000 + this.overlayId,
            },
            class: Object.assign({}, themedClasses, getClassMap(this.cssClass))
        };
    }
    render() {
        return [
            h("ion-backdrop", { visible: this.showBackdrop, tappable: false }),
            h("div", { class: "loading-wrapper", role: "dialog" },
                this.spinner !== 'hide' &&
                    h("div", { class: "loading-spinner" },
                        h("ion-spinner", { name: this.spinner })),
                this.content && h("div", { class: "loading-content" }, this.content))
        ];
    }
    static get is() { return "ion-loading"; }
    static get host() { return {
        "theme": "loading"
    }; }
    static get properties() { return {
        "animationCtrl": {
            "connect": "ion-animation-controller"
        },
        "config": {
            "context": "config"
        },
        "content": {
            "type": String,
            "attr": "content"
        },
        "cssClass": {
            "type": String,
            "attr": "css-class"
        },
        "dismiss": {
            "method": true
        },
        "dismissOnPageChange": {
            "type": Boolean,
            "attr": "dismiss-on-page-change"
        },
        "duration": {
            "type": Number,
            "attr": "duration"
        },
        "el": {
            "elementRef": true
        },
        "enableBackdropDismiss": {
            "type": Boolean,
            "attr": "enable-backdrop-dismiss"
        },
        "enterAnimation": {
            "type": "Any",
            "attr": "enter-animation"
        },
        "keyboardClose": {
            "type": Boolean,
            "attr": "keyboard-close"
        },
        "leaveAnimation": {
            "type": "Any",
            "attr": "leave-animation"
        },
        "onDidDismiss": {
            "method": true
        },
        "onWillDismiss": {
            "method": true
        },
        "overlayId": {
            "type": Number,
            "attr": "overlay-id"
        },
        "present": {
            "method": true
        },
        "showBackdrop": {
            "type": Boolean,
            "attr": "show-backdrop"
        },
        "spinner": {
            "type": String,
            "attr": "spinner"
        },
        "translucent": {
            "type": Boolean,
            "attr": "translucent"
        },
        "willAnimate": {
            "type": Boolean,
            "attr": "will-animate"
        }
    }; }
    static get events() { return [{
            "name": "ionLoadingDidUnload",
            "method": "ionLoadingDidUnload",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionLoadingDidLoad",
            "method": "ionLoadingDidLoad",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionLoadingDidPresent",
            "method": "didPresent",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionLoadingWillPresent",
            "method": "willPresent",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionLoadingWillDismiss",
            "method": "willDismiss",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionLoadingDidDismiss",
            "method": "didDismiss",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "ionBackdropTap",
            "method": "onBackdropTap"
        }]; }
    static get style() { return "ion-loading {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  position: fixed;\n  z-index: 1000;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  -ms-touch-action: none;\n  touch-action: none;\n  contain: strict; }\n\nion-loading-controller {\n  display: none; }\n\n.loading-wrapper {\n  z-index: 10;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  opacity: 0; }\n\n.loading-md {\n  font-family: \"Roboto\", \"Helvetica Neue\", sans-serif;\n  font-size: 14px; }\n\n.loading-md .loading-wrapper {\n  border-radius: 2px;\n  padding: 24px;\n  max-width: 280px;\n  max-height: 90%;\n  color: var(--ion-text-md-color-step-150, var(--ion-text-color-step-150, #262626));\n  background: var(--ion-background-md-color-step-50, var(--ion-background-color-step-50, #f2f2f2));\n  -webkit-box-shadow: 0 16px 20px rgba(0, 0, 0, 0.4);\n  box-shadow: 0 16px 20px rgba(0, 0, 0, 0.4); }\n\n.loading-md .loading-spinner + .loading-content {\n  margin-left: 16px; }\n\n.loading-md .spinner-lines-md line,\n.loading-md .spinner-lines-small-md line {\n  stroke: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.loading-md .spinner-bubbles circle {\n  fill: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.loading-md .spinner-circles circle {\n  fill: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.loading-md .spinner-crescent circle {\n  stroke: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }\n\n.loading-md .spinner-dots circle {\n  fill: var(--ion-color-md-primary, var(--ion-color-primary, #3880ff)); }"; }
    static get styleMode() { return "md"; }
}

export { Loading as IonLoading };
