/*! Built with http://stenciljs.com */
const { h } = window.App;

import { b as Apartment } from './chunk-12551cbb.js';

const PANOLENS = window['PANOLENS'];
class AppPano {
    constructor() {
        this.currentApartmentInd = -1;
    }
    handler() {
        console.log(this);
        this.loadPano();
    }
    loadPano() {
        var elem = document.querySelector('.panolens-container');
        if (elem)
            elem.remove();
        this.currentApartmentInd = (this.currentApartmentInd + 1) % this.apartment.panos.length;
        var panorama, viewer;
        panorama = new PANOLENS.ImagePanorama('/assets/' + this.apartment.panos[this.currentApartmentInd]);
        viewer = new PANOLENS.Viewer({ controlBar: false });
        viewer.control = viewer.DeviceOrientationControls;
        viewer.DeviceOrientationControls.enabled = true;
        viewer.add(panorama);
    }
    componentWillLoad() {
        this.apartment = Apartment.get(this.id);
        if (!window['hb'])
            return location.href = `/apartment/${this.id}`;
        window['hb']['handler'] = () => this.handler();
        PANOLENS['Controls']['ORBIT'] = PANOLENS['Controls']['ORBIT'];
        PANOLENS['Modes']['NORMAL'] = PANOLENS['Modes']['CARDBOARD'];
        this.loadPano();
    }
    render() {
        return h("div", { class: "loader-wrapper" },
            h("div", { class: "loader" }));
    }
    static get is() { return "app-pano"; }
    static get properties() { return {
        "id": {
            "type": String,
            "attr": "id"
        }
    }; }
}

export { AppPano };
