/*! Built with http://stenciljs.com */
const { h } = window.App;

import { e as getButtonClassMap, c as getElementClassMap, d as openURL, a as createThemedClasses } from './chunk-e901a817.js';

class AppHeader {
    render() {
        return h("ion-header", null,
            h("ion-toolbar", { color: 'primary' },
                h("ion-title", null,
                    h("ion-icon", { name: "planet", class: "logo-icon", size: "lg" }),
                    "LocoHome")));
    }
    static get is() { return "app-header"; }
    static get style() { return ".logo-icon {\n  -webkit-transform: scale(1.5) translateY(1px);\n  transform: scale(1.5) translateY(1px);\n  padding: 0 6px; }"; }
}

class Badge {
    static get is() { return "ion-badge"; }
    static get host() { return {
        "theme": "badge"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        }
    }; }
    static get style() { return "ion-badge {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  padding: 3px 8px;\n  text-align: center;\n  display: inline-block;\n  min-width: 10px;\n  font-size: 13px;\n  font-weight: bold;\n  line-height: 1;\n  white-space: nowrap;\n  vertical-align: baseline;\n  contain: content; }\n\nion-badge:empty {\n  display: none; }\n\n.badge-ios {\n  border-radius: 10px;\n  font-family: -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", \"Roboto\", sans-serif;\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.badge-ios-primary {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.badge-ios-secondary {\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.badge-ios-tertiary {\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.badge-ios-success {\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.badge-ios-warning {\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.badge-ios-danger {\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.badge-ios-light {\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.badge-ios-medium {\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.badge-ios-dark {\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "ios"; }
}

class Button {
    constructor() {
        this.keyFocus = false;
        /**
         * The type of button.
         * Possible values are: `"button"`, `"bar-button"`.
         */
        this.buttonType = 'button';
        /**
         * If true, the user cannot interact with the button. Defaults to `false`.
         */
        this.disabled = false;
        /**
         * Set to `"clear"` for a transparent button, to `"outline"` for a transparent
         * button with a border, or to `"solid"`. The default style is `"solid"` except inside of
         * a toolbar, where the default is `"clear"`.
         */
        this.fill = 'default';
        /**
         * If true, activates a button with rounded corners.
         */
        this.round = false;
        /**
         * If true, activates a button with a heavier font weight.
         */
        this.strong = false;
        /**
         * The type of the button.
         * Possible values are: `"submit"`, `"reset"` and `"button"`.
         * Default value is: `"button"`
         */
        this.type = 'button';
    }
    componentWillLoad() {
        if (this.el.closest('ion-buttons')) {
            this.buttonType = 'bar-button';
        }
    }
    onFocus() {
        this.ionFocus.emit();
    }
    onKeyUp() {
        this.keyFocus = true;
    }
    onBlur() {
        this.keyFocus = false;
        this.ionBlur.emit();
    }
    render() {
        const { buttonType, color, expand, fill, mode, round, size, strong } = this;
        const TagType = this.href ? 'a' : 'button';
        const buttonClasses = Object.assign({}, getButtonClassMap(buttonType, mode), getButtonTypeClassMap(buttonType, expand, mode), getButtonTypeClassMap(buttonType, size, mode), getButtonTypeClassMap(buttonType, round ? 'round' : undefined, mode), getButtonTypeClassMap(buttonType, strong ? 'strong' : undefined, mode), getColorClassMap(buttonType, color, fill, mode), getElementClassMap(this.el.classList), { 'focused': this.keyFocus });
        const attrs = (TagType === 'button')
            ? { type: this.type }
            : { href: this.href };
        return (h(TagType, Object.assign({}, attrs, { class: buttonClasses, disabled: this.disabled, onFocus: this.onFocus.bind(this), onKeyUp: this.onKeyUp.bind(this), onBlur: this.onBlur.bind(this), onClick: (ev) => openURL(this.win, this.href, ev, this.routerDirection) }),
            h("span", { class: "button-inner" },
                h("slot", { name: "icon-only" }),
                h("slot", { name: "start" }),
                h("slot", null),
                h("slot", { name: "end" })),
            this.mode === 'md' && h("ion-ripple-effect", { tapClick: true })));
    }
    static get is() { return "ion-button"; }
    static get properties() { return {
        "buttonType": {
            "type": String,
            "attr": "button-type",
            "mutable": true
        },
        "color": {
            "type": String,
            "attr": "color"
        },
        "disabled": {
            "type": Boolean,
            "attr": "disabled"
        },
        "el": {
            "elementRef": true
        },
        "expand": {
            "type": String,
            "attr": "expand"
        },
        "fill": {
            "type": String,
            "attr": "fill"
        },
        "href": {
            "type": String,
            "attr": "href"
        },
        "keyFocus": {
            "state": true
        },
        "mode": {
            "type": String,
            "attr": "mode"
        },
        "round": {
            "type": Boolean,
            "attr": "round"
        },
        "routerDirection": {
            "type": String,
            "attr": "router-direction"
        },
        "size": {
            "type": String,
            "attr": "size"
        },
        "strong": {
            "type": Boolean,
            "attr": "strong"
        },
        "type": {
            "type": String,
            "attr": "type"
        },
        "win": {
            "context": "window"
        }
    }; }
    static get events() { return [{
            "name": "ionFocus",
            "method": "ionFocus",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionBlur",
            "method": "ionBlur",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get style() { return ".button {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  text-align: center;\n  -moz-appearance: none;\n  -ms-appearance: none;\n  -webkit-appearance: none;\n  appearance: none;\n  position: relative;\n  z-index: 0;\n  display: inline-block;\n  border: 0;\n  line-height: 1;\n  text-decoration: none;\n  text-overflow: ellipsis;\n  text-transform: none;\n  white-space: nowrap;\n  cursor: pointer;\n  vertical-align: top;\n  vertical-align: -webkit-baseline-middle;\n  -webkit-transition: background-color, opacity 100ms linear;\n  transition: background-color, opacity 100ms linear;\n  -webkit-font-kerning: none;\n  font-kerning: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none;\n  contain: content; }\n\n.button:active,\n.button:focus {\n  outline: none; }\n\n.button-inner {\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -webkit-flex-flow: row nowrap;\n  -ms-flex-flow: row nowrap;\n  flex-flow: row nowrap;\n  -webkit-flex-shrink: 0;\n  -ms-flex-negative: 0;\n  flex-shrink: 0;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  width: 100%;\n  height: 100%; }\n\na[disabled],\nbutton[disabled],\n.button[disabled] {\n  cursor: default;\n  pointer-events: none; }\n\n.button-block {\n  display: block;\n  clear: both;\n  width: 100%;\n  contain: strict; }\n\n.button-block::after {\n  clear: both; }\n\n.button-full {\n  display: block;\n  width: 100%;\n  contain: strict; }\n\n.button-full.button-outline {\n  border-radius: 0;\n  border-right-width: 0;\n  border-left-width: 0; }\n\n.button ion-icon {\n  font-size: 1.4em;\n  pointer-events: none; }\n\n.button ion-icon[slot=\"start\"] {\n  margin: 0 0.3em 0 -0.3em; }\n\n.button ion-icon[slot=\"end\"] {\n  margin: 0 -0.2em 0 0.3em; }\n\n.button ion-icon[slot=\"icon-only\"] {\n  font-size: 1.8em; }\n\n.button-ios {\n  border-radius: 8px;\n  margin: 4px 2px;\n  padding: 0 1em;\n  height: 2.8em;\n  font-family: -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", \"Roboto\", sans-serif;\n  font-size: 16px;\n  font-weight: 500;\n  letter-spacing: -0.03em;\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.button-ios.activated {\n  background-color: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0));\n  opacity: 1; }\n\n.button-ios.focused {\n  background-color: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n\n.enable-hover .button-ios:hover {\n  opacity: 0.8; }\n\na[disabled],\nbutton[disabled],\n.button[disabled] {\n  opacity: 0.5; }\n\n.button-large-ios {\n  padding: 0 1em;\n  height: 2.8em;\n  font-size: 20px; }\n\n.button-small-ios {\n  padding: 0 0.9em;\n  height: 2.1em;\n  font-size: 13px; }\n\n.button-block-ios {\n  margin-left: 0;\n  margin-right: 0; }\n\n.button-full-ios {\n  margin-left: 0;\n  margin-right: 0;\n  border-radius: 0;\n  border-right-width: 0;\n  border-left-width: 0; }\n\n.button-outline-ios {\n  border-radius: 8px;\n  border-width: 1px;\n  border-style: solid;\n  border-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  background-color: transparent; }\n\n.button-outline-ios.activated {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  opacity: 1; }\n\n.button-outline-ios.focused {\n  background-color: rgba(var(--ion-color-ios-primary-rgb, var(--ion-color-primary-rgb, 56, 128, 255)), 0.25); }\n\n.button-outline-ios.activated.focused {\n  border-color: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0));\n  background-color: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n\n.button-clear-ios {\n  border-color: transparent;\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  background-color: transparent; }\n\n.button-clear-ios.activated {\n  background-color: transparent;\n  opacity: 0.4; }\n\n.button-clear-ios.focused {\n  background-color: rgba(var(--ion-color-ios-primary-rgb, var(--ion-color-primary-rgb, 56, 128, 255)), 0.25); }\n\n.enable-hover .button-clear-ios:hover {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  opacity: 0.6; }\n\n.button-round-ios {\n  border-radius: 64px;\n  padding: 0 26px; }\n\n.button-ios-primary {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.button-ios-primary.activated {\n  background-color: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n\n.button-ios-primary.focused {\n  background-color: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n\n.button-outline-ios-primary {\n  border-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  background-color: transparent; }\n\n.button-outline-ios-primary.activated {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.button-outline-ios-primary.focused {\n  background-color: rgba(var(--ion-color-ios-primary-rgb, var(--ion-color-primary-rgb, 56, 128, 255)), 0.25); }\n\n.button-outline-ios-primary.activated.focused {\n  border-color: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0));\n  background-color: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n\n.button-clear-ios-primary {\n  border-color: transparent;\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  background-color: transparent; }\n\n.button-clear-ios-primary.activated {\n  opacity: 0.4; }\n\n.button-clear-ios-primary.focused {\n  background-color: rgba(var(--ion-color-ios-primary-rgb, var(--ion-color-primary-rgb, 56, 128, 255)), 0.25); }\n\n.enable-hover .button-clear-ios-primary:hover {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.button-ios-secondary {\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.button-ios-secondary.activated {\n  background-color: var(--ion-color-ios-secondary-shade, var(--ion-color-secondary-shade, #0bb8cc)); }\n\n.button-ios-secondary.focused {\n  background-color: var(--ion-color-ios-secondary-shade, var(--ion-color-secondary-shade, #0bb8cc)); }\n\n.button-outline-ios-secondary {\n  border-color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8));\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8));\n  background-color: transparent; }\n\n.button-outline-ios-secondary.activated {\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.button-outline-ios-secondary.focused {\n  background-color: rgba(var(--ion-color-ios-secondary-rgb, var(--ion-color-secondary-rgb, 12, 209, 232)), 0.25); }\n\n.button-outline-ios-secondary.activated.focused {\n  border-color: var(--ion-color-ios-secondary-shade, var(--ion-color-secondary-shade, #0bb8cc));\n  background-color: var(--ion-color-ios-secondary-shade, var(--ion-color-secondary-shade, #0bb8cc)); }\n\n.button-clear-ios-secondary {\n  border-color: transparent;\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8));\n  background-color: transparent; }\n\n.button-clear-ios-secondary.activated {\n  opacity: 0.4; }\n\n.button-clear-ios-secondary.focused {\n  background-color: rgba(var(--ion-color-ios-secondary-rgb, var(--ion-color-secondary-rgb, 12, 209, 232)), 0.25); }\n\n.enable-hover .button-clear-ios-secondary:hover {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.button-ios-tertiary {\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.button-ios-tertiary.activated {\n  background-color: var(--ion-color-ios-tertiary-shade, var(--ion-color-tertiary-shade, #633ce0)); }\n\n.button-ios-tertiary.focused {\n  background-color: var(--ion-color-ios-tertiary-shade, var(--ion-color-tertiary-shade, #633ce0)); }\n\n.button-outline-ios-tertiary {\n  border-color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff));\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff));\n  background-color: transparent; }\n\n.button-outline-ios-tertiary.activated {\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.button-outline-ios-tertiary.focused {\n  background-color: rgba(var(--ion-color-ios-tertiary-rgb, var(--ion-color-tertiary-rgb, 112, 68, 255)), 0.25); }\n\n.button-outline-ios-tertiary.activated.focused {\n  border-color: var(--ion-color-ios-tertiary-shade, var(--ion-color-tertiary-shade, #633ce0));\n  background-color: var(--ion-color-ios-tertiary-shade, var(--ion-color-tertiary-shade, #633ce0)); }\n\n.button-clear-ios-tertiary {\n  border-color: transparent;\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff));\n  background-color: transparent; }\n\n.button-clear-ios-tertiary.activated {\n  opacity: 0.4; }\n\n.button-clear-ios-tertiary.focused {\n  background-color: rgba(var(--ion-color-ios-tertiary-rgb, var(--ion-color-tertiary-rgb, 112, 68, 255)), 0.25); }\n\n.enable-hover .button-clear-ios-tertiary:hover {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.button-ios-success {\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.button-ios-success.activated {\n  background-color: var(--ion-color-ios-success-shade, var(--ion-color-success-shade, #0ec254)); }\n\n.button-ios-success.focused {\n  background-color: var(--ion-color-ios-success-shade, var(--ion-color-success-shade, #0ec254)); }\n\n.button-outline-ios-success {\n  border-color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60));\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60));\n  background-color: transparent; }\n\n.button-outline-ios-success.activated {\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.button-outline-ios-success.focused {\n  background-color: rgba(var(--ion-color-ios-success-rgb, var(--ion-color-success-rgb, 16, 220, 96)), 0.25); }\n\n.button-outline-ios-success.activated.focused {\n  border-color: var(--ion-color-ios-success-shade, var(--ion-color-success-shade, #0ec254));\n  background-color: var(--ion-color-ios-success-shade, var(--ion-color-success-shade, #0ec254)); }\n\n.button-clear-ios-success {\n  border-color: transparent;\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60));\n  background-color: transparent; }\n\n.button-clear-ios-success.activated {\n  opacity: 0.4; }\n\n.button-clear-ios-success.focused {\n  background-color: rgba(var(--ion-color-ios-success-rgb, var(--ion-color-success-rgb, 16, 220, 96)), 0.25); }\n\n.enable-hover .button-clear-ios-success:hover {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.button-ios-warning {\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.button-ios-warning.activated {\n  background-color: var(--ion-color-ios-warning-shade, var(--ion-color-warning-shade, #e0b500)); }\n\n.button-ios-warning.focused {\n  background-color: var(--ion-color-ios-warning-shade, var(--ion-color-warning-shade, #e0b500)); }\n\n.button-outline-ios-warning {\n  border-color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00));\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00));\n  background-color: transparent; }\n\n.button-outline-ios-warning.activated {\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.button-outline-ios-warning.focused {\n  background-color: rgba(var(--ion-color-ios-warning-rgb, var(--ion-color-warning-rgb, 255, 206, 0)), 0.25); }\n\n.button-outline-ios-warning.activated.focused {\n  border-color: var(--ion-color-ios-warning-shade, var(--ion-color-warning-shade, #e0b500));\n  background-color: var(--ion-color-ios-warning-shade, var(--ion-color-warning-shade, #e0b500)); }\n\n.button-clear-ios-warning {\n  border-color: transparent;\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00));\n  background-color: transparent; }\n\n.button-clear-ios-warning.activated {\n  opacity: 0.4; }\n\n.button-clear-ios-warning.focused {\n  background-color: rgba(var(--ion-color-ios-warning-rgb, var(--ion-color-warning-rgb, 255, 206, 0)), 0.25); }\n\n.enable-hover .button-clear-ios-warning:hover {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.button-ios-danger {\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.button-ios-danger.activated {\n  background-color: var(--ion-color-ios-danger-shade, var(--ion-color-danger-shade, #d33939)); }\n\n.button-ios-danger.focused {\n  background-color: var(--ion-color-ios-danger-shade, var(--ion-color-danger-shade, #d33939)); }\n\n.button-outline-ios-danger {\n  border-color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141));\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141));\n  background-color: transparent; }\n\n.button-outline-ios-danger.activated {\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.button-outline-ios-danger.focused {\n  background-color: rgba(var(--ion-color-ios-danger-rgb, var(--ion-color-danger-rgb, 240, 65, 65)), 0.25); }\n\n.button-outline-ios-danger.activated.focused {\n  border-color: var(--ion-color-ios-danger-shade, var(--ion-color-danger-shade, #d33939));\n  background-color: var(--ion-color-ios-danger-shade, var(--ion-color-danger-shade, #d33939)); }\n\n.button-clear-ios-danger {\n  border-color: transparent;\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141));\n  background-color: transparent; }\n\n.button-clear-ios-danger.activated {\n  opacity: 0.4; }\n\n.button-clear-ios-danger.focused {\n  background-color: rgba(var(--ion-color-ios-danger-rgb, var(--ion-color-danger-rgb, 240, 65, 65)), 0.25); }\n\n.enable-hover .button-clear-ios-danger:hover {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.button-ios-light {\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.button-ios-light.activated {\n  background-color: var(--ion-color-ios-light-shade, var(--ion-color-light-shade, #d7d8da)); }\n\n.button-ios-light.focused {\n  background-color: var(--ion-color-ios-light-shade, var(--ion-color-light-shade, #d7d8da)); }\n\n.button-outline-ios-light {\n  border-color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8));\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8));\n  background-color: transparent; }\n\n.button-outline-ios-light.activated {\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.button-outline-ios-light.focused {\n  background-color: rgba(var(--ion-color-ios-light-rgb, var(--ion-color-light-rgb, 244, 245, 248)), 0.25); }\n\n.button-outline-ios-light.activated.focused {\n  border-color: var(--ion-color-ios-light-shade, var(--ion-color-light-shade, #d7d8da));\n  background-color: var(--ion-color-ios-light-shade, var(--ion-color-light-shade, #d7d8da)); }\n\n.button-clear-ios-light {\n  border-color: transparent;\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8));\n  background-color: transparent; }\n\n.button-clear-ios-light.activated {\n  opacity: 0.4; }\n\n.button-clear-ios-light.focused {\n  background-color: rgba(var(--ion-color-ios-light-rgb, var(--ion-color-light-rgb, 244, 245, 248)), 0.25); }\n\n.enable-hover .button-clear-ios-light:hover {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.button-ios-medium {\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.button-ios-medium.activated {\n  background-color: var(--ion-color-ios-medium-shade, var(--ion-color-medium-shade, #86888f)); }\n\n.button-ios-medium.focused {\n  background-color: var(--ion-color-ios-medium-shade, var(--ion-color-medium-shade, #86888f)); }\n\n.button-outline-ios-medium {\n  border-color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2));\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2));\n  background-color: transparent; }\n\n.button-outline-ios-medium.activated {\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.button-outline-ios-medium.focused {\n  background-color: rgba(var(--ion-color-ios-medium-rgb, var(--ion-color-medium-rgb, 152, 154, 162)), 0.25); }\n\n.button-outline-ios-medium.activated.focused {\n  border-color: var(--ion-color-ios-medium-shade, var(--ion-color-medium-shade, #86888f));\n  background-color: var(--ion-color-ios-medium-shade, var(--ion-color-medium-shade, #86888f)); }\n\n.button-clear-ios-medium {\n  border-color: transparent;\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2));\n  background-color: transparent; }\n\n.button-clear-ios-medium.activated {\n  opacity: 0.4; }\n\n.button-clear-ios-medium.focused {\n  background-color: rgba(var(--ion-color-ios-medium-rgb, var(--ion-color-medium-rgb, 152, 154, 162)), 0.25); }\n\n.enable-hover .button-clear-ios-medium:hover {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.button-ios-dark {\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.button-ios-dark.activated {\n  background-color: var(--ion-color-ios-dark-shade, var(--ion-color-dark-shade, #1e2023)); }\n\n.button-ios-dark.focused {\n  background-color: var(--ion-color-ios-dark-shade, var(--ion-color-dark-shade, #1e2023)); }\n\n.button-outline-ios-dark {\n  border-color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428));\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428));\n  background-color: transparent; }\n\n.button-outline-ios-dark.activated {\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.button-outline-ios-dark.focused {\n  background-color: rgba(var(--ion-color-ios-dark-rgb, var(--ion-color-dark-rgb, 34, 36, 40)), 0.25); }\n\n.button-outline-ios-dark.activated.focused {\n  border-color: var(--ion-color-ios-dark-shade, var(--ion-color-dark-shade, #1e2023));\n  background-color: var(--ion-color-ios-dark-shade, var(--ion-color-dark-shade, #1e2023)); }\n\n.button-clear-ios-dark {\n  border-color: transparent;\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428));\n  background-color: transparent; }\n\n.button-clear-ios-dark.activated {\n  opacity: 0.4; }\n\n.button-clear-ios-dark.focused {\n  background-color: rgba(var(--ion-color-ios-dark-rgb, var(--ion-color-dark-rgb, 34, 36, 40)), 0.25); }\n\n.enable-hover .button-clear-ios-dark:hover {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.button-strong-ios {\n  font-weight: 600; }"; }
    static get styleMode() { return "ios"; }
}
/**
 * Get the classes based on the type
 * e.g. block, full, round, large
 */
function getButtonTypeClassMap(buttonType, type, mode) {
    if (!type) {
        return {};
    }
    type = type.toLocaleLowerCase();
    return {
        [`${buttonType}-${type}`]: true,
        [`${buttonType}-${type}-${mode}`]: true
    };
}
function getColorClassMap(buttonType, color, fill, mode) {
    let className = buttonType;
    if (buttonType !== 'bar-button' && fill === 'solid') {
        fill = 'default';
    }
    if (fill && fill !== 'default') {
        className += `-${fill.toLowerCase()}`;
    }
    // special case for a default bar button
    // if the bar button is default it should get the fill
    // but if a color is passed the fill shouldn't be added
    if (buttonType === 'bar-button' && fill === 'default') {
        className = buttonType;
        if (!color) {
            className += '-' + fill.toLowerCase();
        }
    }
    const map = {
        [className]: true,
        [`${className}-${mode}`]: true,
    };
    if (color) {
        map[`${className}-${mode}-${color}`] = true;
    }
    return map;
}

class Card {
    static get is() { return "ion-card"; }
    static get host() { return {
        "theme": "card"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        }
    }; }
    static get style() { return "ion-card {\n  -moz-osx-font-smoothing: grayscale;\n  -webkit-font-smoothing: antialiased;\n  position: relative;\n  display: block;\n  overflow: hidden; }\n\nion-card img {\n  display: block;\n  width: 100%; }\n\n.card-ios {\n  margin: 30px 20px;\n  border-radius: 8px;\n  width: calc(100% - 40px);\n  font-family: -apple-system, BlinkMacSystemFont, \"Helvetica Neue\", \"Roboto\", sans-serif;\n  font-size: 14px;\n  color: var(--ion-text-ios-color-step-400, var(--ion-text-color-step-400, #666666));\n  background-color: var(--ion-item-ios-background-color, var(--ion-background-ios-color, var(--ion-background-color, #fff)));\n  -webkit-box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n  box-shadow: 0 4px 16px rgba(0, 0, 0, 0.12);\n  -webkit-transform: translateZ(0);\n  transform: translateZ(0); }\n\n.card-ios ion-list {\n  margin-bottom: 0; }\n\n.card-ios > .item:last-child,\n.card-ios > .item:last-child .item-inner,\n.card-ios > .item-sliding:last-child .item {\n  border-bottom: 0; }\n\n.card-ios .item-ios .item-inner {\n  border: 0; }\n\n.card .note-ios {\n  font-size: 13px; }\n\n.card-ios h1 {\n  margin: 0 0 2px;\n  font-size: 24px;\n  font-weight: normal; }\n\n.card-ios h2 {\n  margin: 2px 0;\n  font-size: 16px;\n  font-weight: normal; }\n\n.card-ios h3,\n.card-ios h4,\n.card-ios h5,\n.card-ios h6 {\n  margin: 2px 0;\n  font-size: 14px;\n  font-weight: normal; }\n\n.card-ios p {\n  margin: 0 0 2px;\n  font-size: 14px;\n  color: var(--ion-text-ios-color-step-400, var(--ion-text-color-step-400, #666666)); }\n\n.card-ios .text-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-primary {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-primary p {\n    color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n  .card-ios-primary .text-ios-primary {\n    color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-primary .text-ios-secondary {\n    color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-primary .text-ios-tertiary {\n    color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-primary .text-ios-success {\n    color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-primary .text-ios-warning {\n    color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-primary .text-ios-danger {\n    color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-primary .text-ios-light {\n    color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-primary .text-ios-medium {\n    color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-primary .text-ios-dark {\n    color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios .text-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-secondary {\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-secondary p {\n    color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n  .card-ios-secondary .text-ios-primary {\n    color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-secondary .text-ios-secondary {\n    color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-secondary .text-ios-tertiary {\n    color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-secondary .text-ios-success {\n    color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-secondary .text-ios-warning {\n    color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-secondary .text-ios-danger {\n    color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-secondary .text-ios-light {\n    color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-secondary .text-ios-medium {\n    color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-secondary .text-ios-dark {\n    color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios .text-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-tertiary {\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-tertiary p {\n    color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n  .card-ios-tertiary .text-ios-primary {\n    color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-tertiary .text-ios-secondary {\n    color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-tertiary .text-ios-tertiary {\n    color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-tertiary .text-ios-success {\n    color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-tertiary .text-ios-warning {\n    color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-tertiary .text-ios-danger {\n    color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-tertiary .text-ios-light {\n    color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-tertiary .text-ios-medium {\n    color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-tertiary .text-ios-dark {\n    color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios .text-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-success {\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-success p {\n    color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff)); }\n  .card-ios-success .text-ios-primary {\n    color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-success .text-ios-secondary {\n    color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-success .text-ios-tertiary {\n    color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-success .text-ios-success {\n    color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-success .text-ios-warning {\n    color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-success .text-ios-danger {\n    color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-success .text-ios-light {\n    color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-success .text-ios-medium {\n    color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-success .text-ios-dark {\n    color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios .text-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-warning {\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-warning p {\n    color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n  .card-ios-warning .text-ios-primary {\n    color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-warning .text-ios-secondary {\n    color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-warning .text-ios-tertiary {\n    color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-warning .text-ios-success {\n    color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-warning .text-ios-warning {\n    color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-warning .text-ios-danger {\n    color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-warning .text-ios-light {\n    color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-warning .text-ios-medium {\n    color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-warning .text-ios-dark {\n    color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios .text-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-danger {\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-danger p {\n    color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n  .card-ios-danger .text-ios-primary {\n    color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-danger .text-ios-secondary {\n    color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-danger .text-ios-tertiary {\n    color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-danger .text-ios-success {\n    color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-danger .text-ios-warning {\n    color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-danger .text-ios-danger {\n    color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-danger .text-ios-light {\n    color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-danger .text-ios-medium {\n    color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-danger .text-ios-dark {\n    color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios .text-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-light {\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-light p {\n    color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000)); }\n  .card-ios-light .text-ios-primary {\n    color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-light .text-ios-secondary {\n    color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-light .text-ios-tertiary {\n    color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-light .text-ios-success {\n    color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-light .text-ios-warning {\n    color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-light .text-ios-danger {\n    color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-light .text-ios-light {\n    color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-light .text-ios-medium {\n    color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-light .text-ios-dark {\n    color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios .text-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-medium {\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-medium p {\n    color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n  .card-ios-medium .text-ios-primary {\n    color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-medium .text-ios-secondary {\n    color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-medium .text-ios-tertiary {\n    color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-medium .text-ios-success {\n    color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-medium .text-ios-warning {\n    color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-medium .text-ios-danger {\n    color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-medium .text-ios-light {\n    color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-medium .text-ios-medium {\n    color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-medium .text-ios-dark {\n    color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios .text-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-dark {\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n  .card-ios-dark p {\n    color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n  .card-ios-dark .text-ios-primary {\n    color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n  .card-ios-dark .text-ios-secondary {\n    color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n  .card-ios-dark .text-ios-tertiary {\n    color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n  .card-ios-dark .text-ios-success {\n    color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n  .card-ios-dark .text-ios-warning {\n    color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n  .card-ios-dark .text-ios-danger {\n    color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n  .card-ios-dark .text-ios-light {\n    color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n  .card-ios-dark .text-ios-medium {\n    color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n  .card-ios-dark .text-ios-dark {\n    color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "ios"; }
}

class CardContent {
    static get is() { return "ion-card-content"; }
    static get host() { return {
        "theme": "card-content"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        }
    }; }
    static get style() { return "ion-card-content {\n  position: relative;\n  display: block; }\n\n.card-content-ios {\n  padding: 20px;\n  font-size: 16px;\n  line-height: 1.4; }\n\n.card-ios-primary .card-content-ios {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.card-ios-primary .card-content-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-primary .card-content-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-primary .card-content-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-primary .card-content-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-primary .card-content-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-primary .card-content-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-primary .card-content-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-primary .card-content-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-primary .card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-ios-primary {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-secondary .card-content-ios {\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.card-ios-secondary .card-content-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-secondary .card-content-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-secondary .card-content-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-secondary .card-content-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-secondary .card-content-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-secondary .card-content-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-secondary .card-content-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-secondary .card-content-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-secondary .card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-ios-secondary {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-tertiary .card-content-ios {\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.card-ios-tertiary .card-content-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-tertiary .card-content-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-tertiary .card-content-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-tertiary .card-content-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-tertiary .card-content-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-tertiary .card-content-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-tertiary .card-content-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-tertiary .card-content-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-tertiary .card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-ios-tertiary {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-success .card-content-ios {\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.card-ios-success .card-content-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-success .card-content-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-success .card-content-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-success .card-content-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-success .card-content-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-success .card-content-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-success .card-content-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-success .card-content-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-success .card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-ios-success {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-warning .card-content-ios {\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.card-ios-warning .card-content-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-warning .card-content-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-warning .card-content-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-warning .card-content-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-warning .card-content-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-warning .card-content-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-warning .card-content-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-warning .card-content-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-warning .card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-ios-warning {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-danger .card-content-ios {\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.card-ios-danger .card-content-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-danger .card-content-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-danger .card-content-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-danger .card-content-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-danger .card-content-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-danger .card-content-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-danger .card-content-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-danger .card-content-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-danger .card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-ios-danger {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-light .card-content-ios {\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.card-ios-light .card-content-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-light .card-content-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-light .card-content-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-light .card-content-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-light .card-content-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-light .card-content-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-light .card-content-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-light .card-content-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-light .card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-ios-light {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-medium .card-content-ios {\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.card-ios-medium .card-content-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-medium .card-content-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-medium .card-content-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-medium .card-content-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-medium .card-content-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-medium .card-content-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-medium .card-content-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-medium .card-content-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-medium .card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-ios-medium {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-dark .card-content-ios {\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.card-ios-dark .card-content-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-dark .card-content-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-dark .card-content-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-dark .card-content-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-dark .card-content-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-dark .card-content-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-dark .card-content-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-dark .card-content-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-dark .card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-content-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "ios"; }
}

class CardSubtitle {
    hostData() {
        return {
            'role': 'heading',
            'aria-level': '3'
        };
    }
    static get is() { return "ion-card-subtitle"; }
    static get host() { return {
        "theme": "card-subtitle"
    }; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "mode": {
            "type": String,
            "attr": "mode"
        }
    }; }
    static get style() { return "ion-card-subtitle {\n  position: relative;\n  display: block; }\n\n.card-subtitle-ios {\n  margin: 0 0 4px;\n  padding: 0;\n  font-size: 12px;\n  font-weight: 700;\n  letter-spacing: 0.4px;\n  text-transform: uppercase;\n  color: var(--ion-text-ios-color-step-400, var(--ion-text-color-step-400, #666666)); }\n\n.card-ios-primary .card-subtitle-ios {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.card-ios-primary .card-subtitle-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-primary .card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-primary .card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-primary .card-subtitle-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-primary .card-subtitle-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-primary .card-subtitle-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-primary .card-subtitle-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-primary .card-subtitle-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-primary .card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-ios-primary {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-secondary .card-subtitle-ios {\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.card-ios-secondary .card-subtitle-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-secondary .card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-secondary .card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-secondary .card-subtitle-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-secondary .card-subtitle-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-secondary .card-subtitle-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-secondary .card-subtitle-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-secondary .card-subtitle-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-secondary .card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-tertiary .card-subtitle-ios {\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.card-ios-tertiary .card-subtitle-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-tertiary .card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-tertiary .card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-tertiary .card-subtitle-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-tertiary .card-subtitle-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-tertiary .card-subtitle-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-tertiary .card-subtitle-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-tertiary .card-subtitle-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-tertiary .card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-success .card-subtitle-ios {\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.card-ios-success .card-subtitle-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-success .card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-success .card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-success .card-subtitle-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-success .card-subtitle-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-success .card-subtitle-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-success .card-subtitle-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-success .card-subtitle-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-success .card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-ios-success {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-warning .card-subtitle-ios {\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.card-ios-warning .card-subtitle-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-warning .card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-warning .card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-warning .card-subtitle-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-warning .card-subtitle-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-warning .card-subtitle-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-warning .card-subtitle-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-warning .card-subtitle-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-warning .card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-ios-warning {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-danger .card-subtitle-ios {\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.card-ios-danger .card-subtitle-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-danger .card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-danger .card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-danger .card-subtitle-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-danger .card-subtitle-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-danger .card-subtitle-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-danger .card-subtitle-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-danger .card-subtitle-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-danger .card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-ios-danger {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-light .card-subtitle-ios {\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.card-ios-light .card-subtitle-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-light .card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-light .card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-light .card-subtitle-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-light .card-subtitle-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-light .card-subtitle-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-light .card-subtitle-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-light .card-subtitle-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-light .card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-ios-light {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-medium .card-subtitle-ios {\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.card-ios-medium .card-subtitle-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-medium .card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-medium .card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-medium .card-subtitle-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-medium .card-subtitle-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-medium .card-subtitle-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-medium .card-subtitle-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-medium .card-subtitle-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-medium .card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-ios-medium {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-ios-dark .card-subtitle-ios {\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.card-ios-dark .card-subtitle-ios-primary {\n  color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)); }\n\n.card-ios-dark .card-subtitle-ios-secondary {\n  color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8)); }\n\n.card-ios-dark .card-subtitle-ios-tertiary {\n  color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff)); }\n\n.card-ios-dark .card-subtitle-ios-success {\n  color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60)); }\n\n.card-ios-dark .card-subtitle-ios-warning {\n  color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00)); }\n\n.card-ios-dark .card-subtitle-ios-danger {\n  color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141)); }\n\n.card-ios-dark .card-subtitle-ios-light {\n  color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8)); }\n\n.card-ios-dark .card-subtitle-ios-medium {\n  color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2)); }\n\n.card-ios-dark .card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }\n\n.card-subtitle-ios-dark {\n  color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428)); }"; }
    static get styleMode() { return "ios"; }
}

class TabButton {
    constructor() {
        this.keyFocus = false;
        this.selected = false;
    }
    componentDidLoad() {
        this.ionTabButtonDidLoad.emit();
    }
    componentDidUnload() {
        this.ionTabButtonDidUnload.emit();
    }
    onClick(ev) {
        if (!this.tab.disabled) {
            this.ionTabbarClick.emit(this.tab);
        }
        ev.stopPropagation();
        ev.preventDefault();
    }
    onKeyUp() {
        this.keyFocus = true;
    }
    onBlur() {
        this.keyFocus = false;
    }
    hostData() {
        const selected = this.selected;
        const tab = this.tab;
        const hasTitle = !!tab.label;
        const hasIcon = !!tab.icon;
        const hasTitleOnly = (hasTitle && !hasIcon);
        const hasIconOnly = (hasIcon && !hasTitle);
        const hasBadge = !!tab.badge;
        return {
            'role': 'tab',
            'id': tab.btnId,
            'aria-selected': selected,
            'hidden': !tab.show,
            class: {
                'tab-selected': selected,
                'has-title': hasTitle,
                'has-icon': hasIcon,
                'has-title-only': hasTitleOnly,
                'has-icon-only': hasIconOnly,
                'has-badge': hasBadge,
                'tab-btn-disabled': tab.disabled,
                'focused': this.keyFocus
            }
        };
    }
    render() {
        const tab = this.tab;
        const href = tab.href || '#';
        return [
            h("a", { href: href, class: "tab-cover", onKeyUp: this.onKeyUp.bind(this), onBlur: this.onBlur.bind(this) },
                tab.icon && h("ion-icon", { class: "tab-button-icon", name: tab.icon }),
                tab.label && h("span", { class: "tab-button-text" }, tab.label),
                tab.badge && h("ion-badge", { class: "tab-badge", color: tab.badgeStyle }, tab.badge),
                this.mode === 'md' && h("ion-ripple-effect", { tapClick: true }))
        ];
    }
    static get is() { return "ion-tab-button"; }
    static get properties() { return {
        "el": {
            "elementRef": true
        },
        "keyFocus": {
            "state": true
        },
        "selected": {
            "type": Boolean,
            "attr": "selected"
        },
        "tab": {
            "type": "Any",
            "attr": "tab"
        }
    }; }
    static get events() { return [{
            "name": "ionTabbarClick",
            "method": "ionTabbarClick",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionTabButtonDidLoad",
            "method": "ionTabButtonDidLoad",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionTabButtonDidUnload",
            "method": "ionTabButtonDidUnload",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "click",
            "method": "onClick"
        }]; }
    static get style() { return "ion-tab-button {\n  margin: 0;\n  text-align: center;\n  border-radius: 0;\n  -webkit-box-sizing: border-box;\n  box-sizing: border-box;\n  position: relative;\n  z-index: 0;\n  display: block;\n  overflow: hidden;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  height: 100%;\n  border: 0;\n  text-decoration: none;\n  background: none;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\nion-tab-button a {\n  text-decoration: none; }\n\n.tab-cover {\n  margin: 0;\n  padding: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n  -ms-flex-direction: column;\n  flex-direction: column;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: justify;\n  -webkit-justify-content: space-between;\n  -ms-flex-pack: justify;\n  justify-content: space-between;\n  width: 100%;\n  height: 100%;\n  border: 0;\n  color: inherit;\n  background: transparent;\n  cursor: pointer; }\n  .tab-cover:active, .tab-cover:focus {\n    outline: none; }\n\n.tab-btn-disabled {\n  pointer-events: none; }\n  .tab-btn-disabled > .tab-cover {\n    opacity: .4; }\n\n.tab-button-text,\n.tab-button-icon {\n  display: none;\n  overflow: hidden;\n  -webkit-align-self: center;\n  -ms-flex-item-align: center;\n  align-self: center;\n  min-width: 26px;\n  max-width: 100%;\n  text-overflow: ellipsis;\n  white-space: nowrap; }\n\n.has-icon .tab-button-icon,\n.has-title .tab-button-text {\n  display: block; }\n\n.has-title-only .tab-button-text {\n  white-space: normal; }\n\n.layout-icon-start .tab-button {\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: row;\n  -ms-flex-direction: row;\n  flex-direction: row; }\n\n.layout-icon-end .tab-button {\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: reverse;\n  -webkit-flex-direction: row-reverse;\n  -ms-flex-direction: row-reverse;\n  flex-direction: row-reverse; }\n\n.layout-icon-bottom .tab-button {\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: reverse;\n  -webkit-flex-direction: column-reverse;\n  -ms-flex-direction: column-reverse;\n  flex-direction: column-reverse; }\n\n.layout-icon-start .tab-button,\n.layout-icon-end .tab-button,\n.layout-icon-hide .tab-button,\n.layout-title-hide .tab-button {\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center; }\n\n.layout-icon-hide .tab-button-icon,\n.layout-title-hide .tab-button-text {\n  display: none; }\n\n.tab-badge {\n  right: 4%;\n  top: 6%;\n  right: calc(50% - 50px);\n  padding: 1px 6px;\n  position: absolute;\n  height: auto;\n  font-size: 12px;\n  line-height: 16px; }\n\n.has-icon .tab-badge {\n  right: calc(50% - 30px); }\n\n.layout-icon-bottom .tab-badge,\n.layout-icon-start .tab-badge,\n.layout-icon-end .tab-badge {\n  right: calc(50% - 50px); }\n\n.tab-button-ios {\n  max-width: 240px;\n  font-size: 10px;\n  color: var(--ion-tabbar-ios-text-color, var(--ion-tabbar-text-color, #8c8c8c));\n  fill: var(--ion-tabbar-ios-text-color, var(--ion-tabbar-text-color, #8c8c8c)); }\n\n.tab-button-ios.focused {\n  background: var(--ion-tabbar-ios-background-color-focused, var(--ion-tabbar-background-color-focused, #dadada)); }\n\n.tab-button-ios .tab-cover {\n  padding: 0 2px; }\n\n.enable-hover .tab-button-ios:hover,\n.tab-button-ios.tab-selected {\n  color: var(--ion-tabbar-ios-text-color-active, var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff)));\n  fill: var(--ion-tabbar-ios-text-color-active, var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff))); }\n\n.tab-button-ios .tab-button-text {\n  margin-top: 0;\n  margin-bottom: 1px;\n  min-height: 11px; }\n\n.tab-button-ios.has-title-only .tab-button-text {\n  font-size: 12px; }\n\n.tab-button-ios .tab-button-icon {\n  margin-top: 4px;\n  font-size: 30px; }\n\n.tabbar-ios .tab-button-icon::before {\n  vertical-align: top; }\n\n.layout-icon-end .tab-button-ios .tab-button-text,\n.layout-icon-start .tab-button-ios .tab-button-text,\n.layout-icon-hide .tab-button-ios .tab-button-text,\n.tab-button-ios.has-title-only .tab-button-text {\n  margin: 2px 0;\n  font-size: 14px;\n  line-height: 1.1; }\n\n.layout-icon-end .tab-button-ios ion-icon,\n.layout-icon-start .tab-button-ios ion-icon {\n  margin-top: 2px;\n  margin-bottom: 1px;\n  min-width: 24px;\n  height: 26px;\n  font-size: 24px; }\n\n.layout-title-hide .tab-button-ios ion-icon {\n  margin: 0; }"; }
    static get styleMode() { return "ios"; }
}

class Tabbar {
    constructor() {
        this.canScrollLeft = false;
        this.canScrollRight = false;
        this.hidden = false;
        this.layout = 'icon-top';
        this.placement = 'bottom';
        this.scrollable = false;
        this.tabs = [];
        this.highlight = false;
        /**
         * If true, the tabbar will be translucent. Defaults to `false`.
         */
        this.translucent = false;
    }
    selectedTabChanged() {
        this.scrollable && this.scrollToSelectedButton();
        this.highlight && this.updateHighlight();
    }
    onKeyboardWillHide() {
        setTimeout(() => this.hidden = false, 50);
    }
    onKeyboardWillShow() {
        if (this.placement === 'bottom') {
            this.hidden = true;
        }
    }
    onResize() {
        this.highlight && this.updateHighlight();
    }
    onTabButtonLoad() {
        this.scrollable && this.updateBoundaries();
        this.highlight && this.updateHighlight();
    }
    analyzeTabs() {
        const tabs = Array.from(this.doc.querySelectorAll('ion-tab-button'));
        const scrollLeft = this.scrollEl.scrollLeft;
        const tabsWidth = this.scrollEl.clientWidth;
        let previous = undefined;
        let next = undefined;
        for (const tab of tabs) {
            const left = tab.offsetLeft;
            const right = left + tab.offsetWidth;
            if (left < scrollLeft) {
                previous = { tab, amount: left };
            }
            if (!next && right > (tabsWidth + scrollLeft)) {
                const amount = right - tabsWidth;
                next = { tab, amount };
            }
        }
        return { previous, next };
    }
    getSelectedButton() {
        return Array.from(this.el.querySelectorAll('ion-tab-button'))
            .find(btn => btn.selected);
    }
    scrollToSelectedButton() {
        if (!this.scrollEl) {
            return;
        }
        this.queue.read(() => {
            const activeTabButton = this.getSelectedButton();
            if (activeTabButton) {
                const scrollLeft = this.scrollEl.scrollLeft, tabsWidth = this.scrollEl.clientWidth, left = activeTabButton.offsetLeft, right = left + activeTabButton.offsetWidth;
                let amount = 0;
                if (right > (tabsWidth + scrollLeft)) {
                    amount = right - tabsWidth;
                }
                else if (left < scrollLeft) {
                    amount = left;
                }
                if (amount !== 0) {
                    this.queue.write(() => {
                        this.scrollEl.scrollToPoint(amount, 0, 250).then(() => {
                            this.updateBoundaries();
                        });
                    });
                }
            }
        });
    }
    scrollByTab(direction) {
        this.queue.read(() => {
            const { previous, next } = this.analyzeTabs();
            const info = direction === 'right' ? next : previous;
            const amount = info && info.amount;
            if (info && amount) {
                this.scrollEl.scrollToPoint(amount, 0, 250).then(() => {
                    this.updateBoundaries();
                });
            }
        });
    }
    updateBoundaries() {
        this.canScrollLeft = this.scrollEl.scrollLeft !== 0;
        this.canScrollRight = this.scrollEl.scrollLeft < (this.scrollEl.scrollWidth - this.scrollEl.offsetWidth);
    }
    updateHighlight() {
        if (!this.highlight) {
            return;
        }
        this.queue.read(() => {
            const btn = this.getSelectedButton();
            const highlight = this.el.querySelector('div.tabbar-highlight');
            if (btn && highlight) {
                highlight.style.transform = `translate3d(${btn.offsetLeft}px,0,0) scaleX(${btn.offsetWidth})`;
            }
        });
    }
    hostData() {
        const themedClasses = this.translucent ? createThemedClasses(this.mode, this.color, 'tabbar-translucent') : {};
        return {
            role: 'tablist',
            class: Object.assign({}, themedClasses, { [`layout-${this.layout}`]: true, [`placement-${this.placement}`]: true, 'tabbar-hidden': this.hidden, 'scrollable': this.scrollable })
        };
    }
    render() {
        const selectedTab = this.selectedTab;
        const ionTabbarHighlight = this.highlight ? h("div", { class: "animated tabbar-highlight" }) : null;
        const buttonClasses = createThemedClasses(this.mode, this.color, 'tab-button');
        const tabButtons = this.tabs.map(tab => h("ion-tab-button", { class: buttonClasses, tab: tab, selected: selectedTab === tab }));
        if (this.scrollable) {
            return [
                h("ion-button", { onClick: () => this.scrollByTab('left'), fill: "clear", class: { inactive: !this.canScrollLeft } },
                    h("ion-icon", { name: "arrow-dropleft" })),
                h("ion-scroll", { forceOverscroll: false, ref: (scrollEl) => this.scrollEl = scrollEl },
                    tabButtons,
                    ionTabbarHighlight),
                h("ion-button", { onClick: () => this.scrollByTab('right'), fill: "clear", class: { inactive: !this.canScrollRight } },
                    h("ion-icon", { name: "arrow-dropright" }))
            ];
        }
        else {
            return [
                ...tabButtons,
                ionTabbarHighlight
            ];
        }
    }
    static get is() { return "ion-tabbar"; }
    static get host() { return {
        "theme": "tabbar"
    }; }
    static get properties() { return {
        "canScrollLeft": {
            "state": true
        },
        "canScrollRight": {
            "state": true
        },
        "doc": {
            "context": "document"
        },
        "el": {
            "elementRef": true
        },
        "hidden": {
            "state": true
        },
        "highlight": {
            "type": Boolean,
            "attr": "highlight"
        },
        "layout": {
            "type": String,
            "attr": "layout"
        },
        "placement": {
            "type": String,
            "attr": "placement"
        },
        "queue": {
            "context": "queue"
        },
        "scrollable": {
            "type": Boolean,
            "attr": "scrollable"
        },
        "selectedTab": {
            "type": "Any",
            "attr": "selected-tab",
            "watchCallbacks": ["selectedTabChanged"]
        },
        "tabs": {
            "type": "Any",
            "attr": "tabs"
        },
        "translucent": {
            "type": Boolean,
            "attr": "translucent"
        }
    }; }
    static get listeners() { return [{
            "name": "body:keyboardWillHide",
            "method": "onKeyboardWillHide"
        }, {
            "name": "body:keyboardWillShow",
            "method": "onKeyboardWillShow"
        }, {
            "name": "window:resize",
            "method": "onResize",
            "passive": true
        }, {
            "name": "ionTabButtonDidLoad",
            "method": "onTabButtonLoad"
        }, {
            "name": "ionTabButtonDidUnload",
            "method": "onTabButtonLoad"
        }]; }
    static get style() { return "ion-tabbar {\n  position: relative;\n  z-index: 10;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-align: center;\n  -webkit-align-items: center;\n  -ms-flex-align: center;\n  align-items: center;\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  -webkit-box-ordinal-group: 2;\n  -webkit-order: 1;\n  -ms-flex-order: 1;\n  order: 1;\n  width: 100%; }\n\nion-tabbar.tabbar-hidden {\n  display: none; }\n\nion-tabbar.placement-top {\n  -webkit-box-ordinal-group: 0;\n  -webkit-order: -1;\n  -ms-flex-order: -1;\n  order: -1; }\n\n.tabbar-highlight {\n  left: 0;\n  bottom: 0;\n  -webkit-transform-origin: 0 0;\n  transform-origin: 0 0;\n  position: absolute;\n  display: block;\n  width: 1px;\n  height: 2px;\n  -webkit-transform: translateZ(0);\n  transform: translateZ(0); }\n  .tabbar-highlight.animated {\n    -webkit-transition-duration: 300ms;\n    transition-duration: 300ms;\n    -webkit-transition-property: -webkit-transform;\n    transition-property: -webkit-transform;\n    transition-property: transform;\n    transition-property: transform, -webkit-transform;\n    -webkit-transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);\n    transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1);\n    will-change: transform; }\n\n.placement-top .tabbar-highlight {\n  bottom: 0; }\n\n.placement-bottom .tabbar-highlight {\n  top: 0; }\n\nion-tabbar.scrollable ion-scroll {\n  overflow: hidden; }\n\nion-tabbar.scrollable .scroll-inner {\n  position: relative;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: horizontal;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: row;\n  -ms-flex-direction: row;\n  flex-direction: row; }\n\nion-tabbar.scrollable ion-button.inactive {\n  visibility: hidden; }\n\n.tabbar-ios {\n  -webkit-box-pack: center;\n  -webkit-justify-content: center;\n  -ms-flex-pack: center;\n  justify-content: center;\n  height: 50px;\n  border-top: 0.55px solid rgba(var(--ion-tabbar-ios-border-color-rgb, var(--ion-tabbar-border-color-rgb, 0, 0, 0)), 0.2);\n  background-color: var(--ion-tabbar-ios-background-color, var(--ion-tabbar-background-color, #f8f8f8));\n  contain: strict; }\n\n.tabbar-ios.placement-top {\n  border-top: 0;\n  border-bottom: 0.55px solid rgba(var(--ion-tabbar-ios-border-color-rgb, var(--ion-tabbar-border-color-rgb, 0, 0, 0)), 0.2); }\n\n.tabbar-ios .tabbar-highlight {\n  background: var(--ion-tabbar-ios-text-color-active, var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff))); }\n\n.tabbar-translucent-ios {\n  background-color: rgba(var(--ion-tabbar-ios-translucent-background-color-rgb, var(--ion-tabbar-translucent-background-color-rgb, 248, 248, 248)), 0.8);\n  -webkit-backdrop-filter: saturate(210%) blur(20px);\n  backdrop-filter: saturate(210%) blur(20px); }\n\n.tabbar-ios-primary {\n  border-color: var(--ion-background-ios-color-step-400, var(--ion-background-color-step-400, #999999));\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  background-color: var(--ion-color-ios-primary, var(--ion-color-primary, #3880ff));\n  fill: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.tabbar-ios-primary .tab-button {\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.tabbar-ios-primary .tab-button.focused {\n  background: var(--ion-color-ios-primary-shade, var(--ion-color-primary-shade, #3171e0)); }\n\n.tabbar-ios-primary .tab-button:hover:not(.disable-hover),\n.tabbar-ios-primary .tab-selected {\n  font-weight: bold;\n  color: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff));\n  fill: var(--ion-color-ios-primary-contrast, var(--ion-color-primary-contrast, #fff)); }\n\n.tabbar-translucent-ios-primary {\n  background-color: rgba(var(--ion-color-ios-primary-rgb, var(--ion-color-primary-rgb, 56, 128, 255)), 0.8); }\n\n.tabbar-ios-secondary {\n  border-color: var(--ion-background-ios-color-step-400, var(--ion-background-color-step-400, #999999));\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  background-color: var(--ion-color-ios-secondary, var(--ion-color-secondary, #0cd1e8));\n  fill: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.tabbar-ios-secondary .tab-button {\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.tabbar-ios-secondary .tab-button.focused {\n  background: var(--ion-color-ios-secondary-shade, var(--ion-color-secondary-shade, #0bb8cc)); }\n\n.tabbar-ios-secondary .tab-button:hover:not(.disable-hover),\n.tabbar-ios-secondary .tab-selected {\n  font-weight: bold;\n  color: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff));\n  fill: var(--ion-color-ios-secondary-contrast, var(--ion-color-secondary-contrast, #fff)); }\n\n.tabbar-translucent-ios-secondary {\n  background-color: rgba(var(--ion-color-ios-secondary-rgb, var(--ion-color-secondary-rgb, 12, 209, 232)), 0.8); }\n\n.tabbar-ios-tertiary {\n  border-color: var(--ion-background-ios-color-step-400, var(--ion-background-color-step-400, #999999));\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  background-color: var(--ion-color-ios-tertiary, var(--ion-color-tertiary, #7044ff));\n  fill: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.tabbar-ios-tertiary .tab-button {\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.tabbar-ios-tertiary .tab-button.focused {\n  background: var(--ion-color-ios-tertiary-shade, var(--ion-color-tertiary-shade, #633ce0)); }\n\n.tabbar-ios-tertiary .tab-button:hover:not(.disable-hover),\n.tabbar-ios-tertiary .tab-selected {\n  font-weight: bold;\n  color: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff));\n  fill: var(--ion-color-ios-tertiary-contrast, var(--ion-color-tertiary-contrast, #fff)); }\n\n.tabbar-translucent-ios-tertiary {\n  background-color: rgba(var(--ion-color-ios-tertiary-rgb, var(--ion-color-tertiary-rgb, 112, 68, 255)), 0.8); }\n\n.tabbar-ios-success {\n  border-color: var(--ion-background-ios-color-step-400, var(--ion-background-color-step-400, #999999));\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff));\n  background-color: var(--ion-color-ios-success, var(--ion-color-success, #10dc60));\n  fill: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.tabbar-ios-success .tab-button {\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.tabbar-ios-success .tab-button.focused {\n  background: var(--ion-color-ios-success-shade, var(--ion-color-success-shade, #0ec254)); }\n\n.tabbar-ios-success .tab-button:hover:not(.disable-hover),\n.tabbar-ios-success .tab-selected {\n  font-weight: bold;\n  color: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff));\n  fill: var(--ion-color-ios-success-contrast, var(--ion-color-success-contrast, #fff)); }\n\n.tabbar-translucent-ios-success {\n  background-color: rgba(var(--ion-color-ios-success-rgb, var(--ion-color-success-rgb, 16, 220, 96)), 0.8); }\n\n.tabbar-ios-warning {\n  border-color: var(--ion-background-ios-color-step-400, var(--ion-background-color-step-400, #999999));\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000));\n  background-color: var(--ion-color-ios-warning, var(--ion-color-warning, #ffce00));\n  fill: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.tabbar-ios-warning .tab-button {\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.tabbar-ios-warning .tab-button.focused {\n  background: var(--ion-color-ios-warning-shade, var(--ion-color-warning-shade, #e0b500)); }\n\n.tabbar-ios-warning .tab-button:hover:not(.disable-hover),\n.tabbar-ios-warning .tab-selected {\n  font-weight: bold;\n  color: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000));\n  fill: var(--ion-color-ios-warning-contrast, var(--ion-color-warning-contrast, #000)); }\n\n.tabbar-translucent-ios-warning {\n  background-color: rgba(var(--ion-color-ios-warning-rgb, var(--ion-color-warning-rgb, 255, 206, 0)), 0.8); }\n\n.tabbar-ios-danger {\n  border-color: var(--ion-background-ios-color-step-400, var(--ion-background-color-step-400, #999999));\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  background-color: var(--ion-color-ios-danger, var(--ion-color-danger, #f04141));\n  fill: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.tabbar-ios-danger .tab-button {\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.tabbar-ios-danger .tab-button.focused {\n  background: var(--ion-color-ios-danger-shade, var(--ion-color-danger-shade, #d33939)); }\n\n.tabbar-ios-danger .tab-button:hover:not(.disable-hover),\n.tabbar-ios-danger .tab-selected {\n  font-weight: bold;\n  color: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff));\n  fill: var(--ion-color-ios-danger-contrast, var(--ion-color-danger-contrast, #fff)); }\n\n.tabbar-translucent-ios-danger {\n  background-color: rgba(var(--ion-color-ios-danger-rgb, var(--ion-color-danger-rgb, 240, 65, 65)), 0.8); }\n\n.tabbar-ios-light {\n  border-color: var(--ion-background-ios-color-step-400, var(--ion-background-color-step-400, #999999));\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000));\n  background-color: var(--ion-color-ios-light, var(--ion-color-light, #f4f5f8));\n  fill: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.tabbar-ios-light .tab-button {\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.tabbar-ios-light .tab-button.focused {\n  background: var(--ion-color-ios-light-shade, var(--ion-color-light-shade, #d7d8da)); }\n\n.tabbar-ios-light .tab-button:hover:not(.disable-hover),\n.tabbar-ios-light .tab-selected {\n  font-weight: bold;\n  color: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000));\n  fill: var(--ion-color-ios-light-contrast, var(--ion-color-light-contrast, #000)); }\n\n.tabbar-translucent-ios-light {\n  background-color: rgba(var(--ion-color-ios-light-rgb, var(--ion-color-light-rgb, 244, 245, 248)), 0.8); }\n\n.tabbar-ios-medium {\n  border-color: var(--ion-background-ios-color-step-400, var(--ion-background-color-step-400, #999999));\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000));\n  background-color: var(--ion-color-ios-medium, var(--ion-color-medium, #989aa2));\n  fill: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.tabbar-ios-medium .tab-button {\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.tabbar-ios-medium .tab-button.focused {\n  background: var(--ion-color-ios-medium-shade, var(--ion-color-medium-shade, #86888f)); }\n\n.tabbar-ios-medium .tab-button:hover:not(.disable-hover),\n.tabbar-ios-medium .tab-selected {\n  font-weight: bold;\n  color: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000));\n  fill: var(--ion-color-ios-medium-contrast, var(--ion-color-medium-contrast, #000)); }\n\n.tabbar-translucent-ios-medium {\n  background-color: rgba(var(--ion-color-ios-medium-rgb, var(--ion-color-medium-rgb, 152, 154, 162)), 0.8); }\n\n.tabbar-ios-dark {\n  border-color: var(--ion-background-ios-color-step-400, var(--ion-background-color-step-400, #999999));\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  background-color: var(--ion-color-ios-dark, var(--ion-color-dark, #222428));\n  fill: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.tabbar-ios-dark .tab-button {\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.tabbar-ios-dark .tab-button.focused {\n  background: var(--ion-color-ios-dark-shade, var(--ion-color-dark-shade, #1e2023)); }\n\n.tabbar-ios-dark .tab-button:hover:not(.disable-hover),\n.tabbar-ios-dark .tab-selected {\n  font-weight: bold;\n  color: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff));\n  fill: var(--ion-color-ios-dark-contrast, var(--ion-color-dark-contrast, #fff)); }\n\n.tabbar-translucent-ios-dark {\n  background-color: rgba(var(--ion-color-ios-dark-rgb, var(--ion-color-dark-rgb, 34, 36, 40)), 0.8); }"; }
    static get styleMode() { return "ios"; }
}

class Tabs {
    constructor() {
        this.ids = -1;
        this.transitioning = false;
        this.tabsId = (++tabIds);
        this.tabs = [];
        /**
         * If true, the tabbar
         */
        this.tabbarHidden = false;
        /**
         * If true, the tabs will be translucent.
         * Note: In order to scroll content behind the tabs, the `fullscreen`
         * attribute needs to be set on the content.
         * Defaults to `false`.
         */
        this.translucent = false;
        this.scrollable = false;
        this.useRouter = false;
    }
    componentWillLoad() {
        if (!this.useRouter) {
            this.useRouter = !!this.doc.querySelector('ion-router') && !this.el.closest('[no-router]');
        }
        this.loadConfig('tabbarLayout', 'bottom');
        this.loadConfig('tabbarLayout', 'icon-top');
        this.loadConfig('tabbarHighlight', false);
    }
    async componentDidLoad() {
        await this.initTabs();
        await this.initSelect();
    }
    componentDidUnload() {
        this.tabs.length = 0;
        this.selectedTab = this.leavingTab = undefined;
    }
    tabChange(ev) {
        const selectedTab = ev.detail;
        if (this.useRouter && selectedTab.href != null) {
            const router = this.doc.querySelector('ion-router');
            if (router) {
                router.push(selectedTab.href);
            }
            return;
        }
        this.select(selectedTab);
    }
    /**
     * @param {number|Tab} tabOrIndex Index, or the Tab instance, of the tab to select.
     */
    async select(tabOrIndex) {
        const selectedTab = this.getTab(tabOrIndex);
        if (!this.shouldSwitch(selectedTab)) {
            return false;
        }
        await this.setActive(selectedTab);
        await this.notifyRouter();
        this.tabSwitch();
        return true;
    }
    async setRouteId(id) {
        const selectedTab = this.getTab(id);
        if (!this.shouldSwitch(selectedTab)) {
            return { changed: false, element: this.selectedTab };
        }
        await this.setActive(selectedTab);
        return {
            changed: true,
            element: this.selectedTab,
            markVisible: () => this.tabSwitch(),
        };
    }
    getRouteId() {
        const id = this.selectedTab && this.selectedTab.getTabId();
        return id ? { id, element: this.selectedTab } : undefined;
    }
    getTab(tabOrIndex) {
        if (typeof tabOrIndex === 'string') {
            return this.tabs.find(tab => tab.getTabId() === tabOrIndex);
        }
        if (typeof tabOrIndex === 'number') {
            return this.tabs[tabOrIndex];
        }
        return tabOrIndex;
    }
    /**
     * @return {HTMLIonTabElement} Returns the currently selected tab
     */
    getSelected() {
        return this.selectedTab;
    }
    initTabs() {
        const tabs = this.tabs = Array.from(this.el.querySelectorAll('ion-tab'));
        const tabPromises = tabs.map(tab => {
            const id = `t-${this.tabsId}-${++this.ids}`;
            tab.btnId = 'tab-' + id;
            tab.id = 'tabpanel-' + id;
            return tab.componentOnReady();
        });
        return Promise.all(tabPromises);
    }
    async initSelect() {
        if (this.useRouter) {
            return;
        }
        // find pre-selected tabs
        const selectedTab = this.tabs.find(t => t.selected) ||
            this.tabs.find(t => t.show && !t.disabled);
        // reset all tabs none is selected
        for (const tab of this.tabs) {
            if (tab !== selectedTab) {
                tab.selected = false;
            }
        }
        if (selectedTab) {
            await selectedTab.setActive();
        }
        this.selectedTab = selectedTab;
        if (selectedTab) {
            selectedTab.selected = true;
            selectedTab.active = true;
        }
    }
    loadConfig(attrKey, fallback) {
        const val = this[attrKey];
        if (typeof val === 'undefined') {
            this[attrKey] = this.config.get(attrKey, fallback);
        }
    }
    setActive(selectedTab) {
        if (this.transitioning) {
            return Promise.reject('transitioning already happening');
        }
        if (!selectedTab) {
            return Promise.reject('no tab is selected');
        }
        // Reset rest of tabs
        for (const tab of this.tabs) {
            if (selectedTab !== tab) {
                tab.selected = false;
            }
        }
        this.transitioning = true;
        this.leavingTab = this.selectedTab;
        this.selectedTab = selectedTab;
        this.ionNavWillChange.emit();
        return selectedTab.setActive();
    }
    tabSwitch() {
        const selectedTab = this.selectedTab;
        const leavingTab = this.leavingTab;
        this.leavingTab = undefined;
        this.transitioning = false;
        if (!selectedTab) {
            return;
        }
        selectedTab.selected = true;
        if (leavingTab !== selectedTab) {
            if (leavingTab) {
                leavingTab.active = false;
            }
            this.ionChange.emit({ tab: selectedTab });
            this.ionNavDidChange.emit();
        }
    }
    notifyRouter() {
        if (this.useRouter) {
            const router = this.doc.querySelector('ion-router');
            if (router) {
                return router.navChanged(1 /* Forward */);
            }
        }
        return Promise.resolve(false);
    }
    shouldSwitch(selectedTab) {
        const leavingTab = this.selectedTab;
        return !!(selectedTab && selectedTab !== leavingTab && !this.transitioning);
    }
    render() {
        const dom = [
            h("div", { class: "tabs-inner" },
                h("slot", null))
        ];
        if (!this.tabbarHidden) {
            dom.push(h("ion-tabbar", { tabs: this.tabs, color: this.color, selectedTab: this.selectedTab, highlight: this.tabbarHighlight, placement: this.tabbarPlacement, layout: this.tabbarLayout, translucent: this.translucent, scrollable: this.scrollable }));
        }
        return dom;
    }
    static get is() { return "ion-tabs"; }
    static get properties() { return {
        "color": {
            "type": String,
            "attr": "color"
        },
        "config": {
            "context": "config"
        },
        "doc": {
            "context": "document"
        },
        "el": {
            "elementRef": true
        },
        "getRouteId": {
            "method": true
        },
        "getSelected": {
            "method": true
        },
        "getTab": {
            "method": true
        },
        "name": {
            "type": String,
            "attr": "name"
        },
        "scrollable": {
            "type": Boolean,
            "attr": "scrollable"
        },
        "select": {
            "method": true
        },
        "selectedTab": {
            "state": true
        },
        "setRouteId": {
            "method": true
        },
        "tabbarHidden": {
            "type": Boolean,
            "attr": "tabbar-hidden"
        },
        "tabbarHighlight": {
            "type": Boolean,
            "attr": "tabbar-highlight",
            "mutable": true
        },
        "tabbarLayout": {
            "type": String,
            "attr": "tabbar-layout",
            "mutable": true
        },
        "tabbarPlacement": {
            "type": String,
            "attr": "tabbar-placement",
            "mutable": true
        },
        "tabs": {
            "state": true
        },
        "translucent": {
            "type": Boolean,
            "attr": "translucent"
        },
        "useRouter": {
            "type": Boolean,
            "attr": "use-router",
            "mutable": true
        }
    }; }
    static get events() { return [{
            "name": "ionChange",
            "method": "ionChange",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionNavWillChange",
            "method": "ionNavWillChange",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }, {
            "name": "ionNavDidChange",
            "method": "ionNavDidChange",
            "bubbles": true,
            "cancelable": true,
            "composed": true
        }]; }
    static get listeners() { return [{
            "name": "ionTabbarClick",
            "method": "tabChange"
        }]; }
    static get style() { return "ion-tabs {\n  left: 0;\n  right: 0;\n  top: 0;\n  bottom: 0;\n  position: absolute;\n  z-index: 0;\n  display: -webkit-box;\n  display: -webkit-flex;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n  -webkit-flex-direction: column;\n  -ms-flex-direction: column;\n  flex-direction: column;\n  width: 100%;\n  height: 100%;\n  contain: layout size style; }\n\n.tabs-inner {\n  position: relative;\n  -webkit-box-flex: 1;\n  -webkit-flex: 1;\n  -ms-flex: 1;\n  flex: 1;\n  contain: layout size style; }"; }
}
let tabIds = -1;

export { AppHeader, Badge as IonBadge, Button as IonButton, Card as IonCard, CardContent as IonCardContent, CardSubtitle as IonCardSubtitle, TabButton as IonTabButton, Tabbar as IonTabbar, Tabs as IonTabs };
