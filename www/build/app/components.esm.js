
var AppCardModalComponent = /** @class **/ (function() {
  function AppCardModal() {
  }
  AppCardModal.is = 'app-card-modal';
  AppCardModal.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.AppCardModal;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.AppCardModal;
      });
    }
    
  }
});

var AppCheckoutComponent = /** @class **/ (function() {
  function AppCheckout() {
  }
  AppCheckout.is = 'app-checkout';
  AppCheckout.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.AppCheckout;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.AppCheckout;
      });
    }
    
  }
});

var AppCodeComponent = /** @class **/ (function() {
  function AppCode() {
  }
  AppCode.is = 'app-code';
  AppCode.getModule = function(opts) {
    
    return import('./app-code.js').then(function(m) {
        return m.AppCode;
      });

  }
});

var AppDetailsComponent = /** @class **/ (function() {
  function AppDetails() {
  }
  AppDetails.is = 'app-details';
  AppDetails.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-details.ios.js').then(function(m) {
        return m.AppDetails;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-details.md.js').then(function(m) {
        return m.AppDetails;
      });
    }
    
  }
});

var AppFavsComponent = /** @class **/ (function() {
  function AppFavs() {
  }
  AppFavs.is = 'app-favs';
  AppFavs.getModule = function(opts) {
    
    return import('./app-favs.js').then(function(m) {
        return m.AppFavs;
      });

  }
});

var AppHeaderComponent = /** @class **/ (function() {
  function AppHeader() {
  }
  AppHeader.is = 'app-header';
  AppHeader.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-header.ios.js').then(function(m) {
        return m.AppHeader;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-header.md.js').then(function(m) {
        return m.AppHeader;
      });
    }
    
  }
});

var AppHomeComponent = /** @class **/ (function() {
  function AppHome() {
  }
  AppHome.is = 'app-home';
  AppHome.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-home.ios.js').then(function(m) {
        return m.AppHome;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-home.md.js').then(function(m) {
        return m.AppHome;
      });
    }
    
  }
});

var AppLoginComponent = /** @class **/ (function() {
  function AppLogin() {
  }
  AppLogin.is = 'app-login';
  AppLogin.getModule = function(opts) {
    
    return import('./app-login.js').then(function(m) {
        return m.AppLogin;
      });

  }
});

var AppPanoComponent = /** @class **/ (function() {
  function AppPano() {
  }
  AppPano.is = 'app-pano';
  AppPano.getModule = function(opts) {
    
    return import('./app-pano.js').then(function(m) {
        return m.AppPano;
      });

  }
});

var AppProfileComponent = /** @class **/ (function() {
  function AppProfile() {
  }
  AppProfile.is = 'app-profile';
  AppProfile.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-profile.ios.js').then(function(m) {
        return m.AppProfile;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-profile.md.js').then(function(m) {
        return m.AppProfile;
      });
    }
    
  }
});

var AlertComponent = /** @class **/ (function() {
  function Alert() {
  }
  Alert.is = 'ion-alert';
  Alert.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-alert.ios.js').then(function(m) {
        return m.Alert;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-alert.md.js').then(function(m) {
        return m.Alert;
      });
    }
    
  }
});

var AlertControllerComponent = /** @class **/ (function() {
  function AlertController() {
  }
  AlertController.is = 'ion-alert-controller';
  AlertController.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.AlertController;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.AlertController;
      });
    }
    
  }
});

var AnimationControllerImplComponent = /** @class **/ (function() {
  function AnimationControllerImpl() {
  }
  AnimationControllerImpl.is = 'ion-animation-controller';
  AnimationControllerImpl.getModule = function(opts) {
    
    return import('./ion-animation-controller.js').then(function(m) {
        return m.AnimationControllerImpl;
      });

  }
});

var AppComponent = /** @class **/ (function() {
  function App() {
  }
  App.is = 'ion-app';
  App.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.App;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.App;
      });
    }
    
  }
});

var BackButtonComponent = /** @class **/ (function() {
  function BackButton() {
  }
  BackButton.is = 'ion-back-button';
  BackButton.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-profile.ios.js').then(function(m) {
        return m.BackButton;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-profile.md.js').then(function(m) {
        return m.BackButton;
      });
    }
    
  }
});

var BackdropComponent = /** @class **/ (function() {
  function Backdrop() {
  }
  Backdrop.is = 'ion-backdrop';
  Backdrop.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-backdrop.ios.js').then(function(m) {
        return m.Backdrop;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-backdrop.md.js').then(function(m) {
        return m.Backdrop;
      });
    }
    
  }
});

var BadgeComponent = /** @class **/ (function() {
  function Badge() {
  }
  Badge.is = 'ion-badge';
  Badge.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-header.ios.js').then(function(m) {
        return m.Badge;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-header.md.js').then(function(m) {
        return m.Badge;
      });
    }
    
  }
});

var ButtonComponent = /** @class **/ (function() {
  function Button() {
  }
  Button.is = 'ion-button';
  Button.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-header.ios.js').then(function(m) {
        return m.Button;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-header.md.js').then(function(m) {
        return m.Button;
      });
    }
    
  }
});

var ButtonsComponent = /** @class **/ (function() {
  function Buttons() {
  }
  Buttons.is = 'ion-buttons';
  Buttons.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-buttons.ios.js').then(function(m) {
        return m.Buttons;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-buttons.md.js').then(function(m) {
        return m.Buttons;
      });
    }
    
  }
});

var CardComponent = /** @class **/ (function() {
  function Card() {
  }
  Card.is = 'ion-card';
  Card.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-header.ios.js').then(function(m) {
        return m.Card;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-header.md.js').then(function(m) {
        return m.Card;
      });
    }
    
  }
});

var CardContentComponent = /** @class **/ (function() {
  function CardContent() {
  }
  CardContent.is = 'ion-card-content';
  CardContent.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-header.ios.js').then(function(m) {
        return m.CardContent;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-header.md.js').then(function(m) {
        return m.CardContent;
      });
    }
    
  }
});

var CardSubtitleComponent = /** @class **/ (function() {
  function CardSubtitle() {
  }
  CardSubtitle.is = 'ion-card-subtitle';
  CardSubtitle.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-header.ios.js').then(function(m) {
        return m.CardSubtitle;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-header.md.js').then(function(m) {
        return m.CardSubtitle;
      });
    }
    
  }
});

var ContentComponent = /** @class **/ (function() {
  function Content() {
  }
  Content.is = 'ion-content';
  Content.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-buttons.ios.js').then(function(m) {
        return m.Content;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-buttons.md.js').then(function(m) {
        return m.Content;
      });
    }
    
  }
});

var GestureComponent = /** @class **/ (function() {
  function Gesture() {
  }
  Gesture.is = 'ion-gesture';
  Gesture.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.Gesture;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.Gesture;
      });
    }
    
  }
});

var GestureControllerComponent = /** @class **/ (function() {
  function GestureController() {
  }
  GestureController.is = 'ion-gesture-controller';
  GestureController.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.GestureController;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.GestureController;
      });
    }
    
  }
});

var HeaderComponent = /** @class **/ (function() {
  function Header() {
  }
  Header.is = 'ion-header';
  Header.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-buttons.ios.js').then(function(m) {
        return m.Header;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-buttons.md.js').then(function(m) {
        return m.Header;
      });
    }
    
  }
});

var IconComponent = /** @class **/ (function() {
  function Icon() {
  }
  Icon.is = 'ion-icon';
  Icon.getModule = function(opts) {
    
    return import('./ion-icon.js').then(function(m) {
        return m.Icon;
      });

  }
});

var InputComponent = /** @class **/ (function() {
  function Input() {
  }
  Input.is = 'ion-input';
  Input.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.Input;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.Input;
      });
    }
    
  }
});

var InputShimsComponent = /** @class **/ (function() {
  function InputShims() {
  }
  InputShims.is = 'ion-input-shims';
  InputShims.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.InputShims;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.InputShims;
      });
    }
    
  }
});

var ItemComponent = /** @class **/ (function() {
  function Item() {
  }
  Item.is = 'ion-item';
  Item.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.Item;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.Item;
      });
    }
    
  }
});

var LabelComponent = /** @class **/ (function() {
  function Label() {
  }
  Label.is = 'ion-label';
  Label.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.Label;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.Label;
      });
    }
    
  }
});

var ListComponent = /** @class **/ (function() {
  function List() {
  }
  List.is = 'ion-list';
  List.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.List;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.List;
      });
    }
    
  }
});

var ListHeaderComponent = /** @class **/ (function() {
  function ListHeader() {
  }
  ListHeader.is = 'ion-list-header';
  ListHeader.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.ListHeader;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.ListHeader;
      });
    }
    
  }
});

var LoadingComponent = /** @class **/ (function() {
  function Loading() {
  }
  Loading.is = 'ion-loading';
  Loading.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-loading.ios.js').then(function(m) {
        return m.Loading;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-loading.md.js').then(function(m) {
        return m.Loading;
      });
    }
    
  }
});

var LoadingControllerComponent = /** @class **/ (function() {
  function LoadingController() {
  }
  LoadingController.is = 'ion-loading-controller';
  LoadingController.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.LoadingController;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.LoadingController;
      });
    }
    
  }
});

var ModalComponent = /** @class **/ (function() {
  function Modal() {
  }
  Modal.is = 'ion-modal';
  Modal.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-modal.ios.js').then(function(m) {
        return m.Modal;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-modal.md.js').then(function(m) {
        return m.Modal;
      });
    }
    
  }
});

var ModalControllerComponent = /** @class **/ (function() {
  function ModalController() {
  }
  ModalController.is = 'ion-modal-controller';
  ModalController.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.ModalController;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.ModalController;
      });
    }
    
  }
});

var NavComponent = /** @class **/ (function() {
  function Nav() {
  }
  Nav.is = 'ion-nav';
  Nav.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.Nav;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.Nav;
      });
    }
    
  }
});

var RippleEffectComponent = /** @class **/ (function() {
  function RippleEffect() {
  }
  RippleEffect.is = 'ion-ripple-effect';
  RippleEffect.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-backdrop.ios.js').then(function(m) {
        return m.RippleEffect;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-backdrop.md.js').then(function(m) {
        return m.RippleEffect;
      });
    }
    
  }
});

var RouteComponent = /** @class **/ (function() {
  function Route() {
  }
  Route.is = 'ion-route';
  Route.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.Route;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.Route;
      });
    }
    
  }
});

var RouterComponent = /** @class **/ (function() {
  function Router() {
  }
  Router.is = 'ion-router';
  Router.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.Router;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.Router;
      });
    }
    
  }
});

var ScrollComponent = /** @class **/ (function() {
  function Scroll() {
  }
  Scroll.is = 'ion-scroll';
  Scroll.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-scroll.ios.js').then(function(m) {
        return m.Scroll;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-scroll.md.js').then(function(m) {
        return m.Scroll;
      });
    }
    
  }
});

var SearchbarComponent = /** @class **/ (function() {
  function Searchbar() {
  }
  Searchbar.is = 'ion-searchbar';
  Searchbar.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-home.ios.js').then(function(m) {
        return m.Searchbar;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-home.md.js').then(function(m) {
        return m.Searchbar;
      });
    }
    
  }
});

var SlideComponent = /** @class **/ (function() {
  function Slide() {
  }
  Slide.is = 'ion-slide';
  Slide.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-details.ios.js').then(function(m) {
        return m.Slide;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-details.md.js').then(function(m) {
        return m.Slide;
      });
    }
    
  }
});

var SlidesComponent = /** @class **/ (function() {
  function Slides() {
  }
  Slides.is = 'ion-slides';
  Slides.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-details.ios.js').then(function(m) {
        return m.Slides;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-details.md.js').then(function(m) {
        return m.Slides;
      });
    }
    
  }
});

var SpinnerComponent = /** @class **/ (function() {
  function Spinner() {
  }
  Spinner.is = 'ion-spinner';
  Spinner.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-spinner.ios.js').then(function(m) {
        return m.Spinner;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-spinner.md.js').then(function(m) {
        return m.Spinner;
      });
    }
    
  }
});

var StatusTapComponent = /** @class **/ (function() {
  function StatusTap() {
  }
  StatusTap.is = 'ion-status-tap';
  StatusTap.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.StatusTap;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.StatusTap;
      });
    }
    
  }
});

var TabComponent = /** @class **/ (function() {
  function Tab() {
  }
  Tab.is = 'ion-tab';
  Tab.getModule = function(opts) {
    
    return import('./ion-tab.js').then(function(m) {
        return m.Tab;
      });

  }
});

var TabButtonComponent = /** @class **/ (function() {
  function TabButton() {
  }
  TabButton.is = 'ion-tab-button';
  TabButton.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-header.ios.js').then(function(m) {
        return m.TabButton;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-header.md.js').then(function(m) {
        return m.TabButton;
      });
    }
    
  }
});

var TabbarComponent = /** @class **/ (function() {
  function Tabbar() {
  }
  Tabbar.is = 'ion-tabbar';
  Tabbar.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-header.ios.js').then(function(m) {
        return m.Tabbar;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-header.md.js').then(function(m) {
        return m.Tabbar;
      });
    }
    
  }
});

var TabsComponent = /** @class **/ (function() {
  function Tabs() {
  }
  Tabs.is = 'ion-tabs';
  Tabs.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-header.ios.js').then(function(m) {
        return m.Tabs;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-header.md.js').then(function(m) {
        return m.Tabs;
      });
    }
    
  }
});

var TapClickComponent = /** @class **/ (function() {
  function TapClick() {
  }
  TapClick.is = 'ion-tap-click';
  TapClick.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.TapClick;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.TapClick;
      });
    }
    
  }
});

var ThumbnailComponent = /** @class **/ (function() {
  function Thumbnail() {
  }
  Thumbnail.is = 'ion-thumbnail';
  Thumbnail.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-card-modal.ios.js').then(function(m) {
        return m.Thumbnail;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-card-modal.md.js').then(function(m) {
        return m.Thumbnail;
      });
    }
    
  }
});

var ToolbarTitleComponent = /** @class **/ (function() {
  function ToolbarTitle() {
  }
  ToolbarTitle.is = 'ion-title';
  ToolbarTitle.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-buttons.ios.js').then(function(m) {
        return m.ToolbarTitle;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-buttons.md.js').then(function(m) {
        return m.ToolbarTitle;
      });
    }
    
  }
});

var ToastComponent = /** @class **/ (function() {
  function Toast() {
  }
  Toast.is = 'ion-toast';
  Toast.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-toast.ios.js').then(function(m) {
        return m.Toast;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-toast.md.js').then(function(m) {
        return m.Toast;
      });
    }
    
  }
});

var ToastControllerComponent = /** @class **/ (function() {
  function ToastController() {
  }
  ToastController.is = 'ion-toast-controller';
  ToastController.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-buttons.ios.js').then(function(m) {
        return m.ToastController;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-buttons.md.js').then(function(m) {
        return m.ToastController;
      });
    }
    
  }
});

var ToolbarComponent = /** @class **/ (function() {
  function Toolbar() {
  }
  Toolbar.is = 'ion-toolbar';
  Toolbar.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./ion-buttons.ios.js').then(function(m) {
        return m.Toolbar;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./ion-buttons.md.js').then(function(m) {
        return m.Toolbar;
      });
    }
    
  }
});

var LazyImgComponent = /** @class **/ (function() {
  function LazyImg() {
  }
  LazyImg.is = 'lazy-img';
  LazyImg.getModule = function(opts) {
    
    return import('./lazy-img.js').then(function(m) {
        return m.LazyImg;
      });

  }
});

var MyAppComponent = /** @class **/ (function() {
  function MyApp() {
  }
  MyApp.is = 'my-app';
  MyApp.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./my-app.ios.js').then(function(m) {
        return m.MyApp;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./my-app.md.js').then(function(m) {
        return m.MyApp;
      });
    }
    
  }
});

var QRScannerComponent = /** @class **/ (function() {
  function QRScanner() {
  }
  QRScanner.is = 'qr-scanner';
  QRScanner.getModule = function(opts) {
    
    if (opts.mode === 'ios') {
      return import('./app-profile.ios.js').then(function(m) {
        return m.QRScanner;
      });
    }
    
    if (opts.mode === 'md') {
      return import('./app-profile.md.js').then(function(m) {
        return m.QRScanner;
      });
    }
    
  }
});

export {
  
  AppCardModal,
  AppCheckout,
  AppCode,
  AppDetails,
  AppFavs,
  AppHeader,
  AppHome,
  AppLogin,
  AppPano,
  AppProfile,
  Alert,
  AlertController,
  AnimationControllerImpl,
  App,
  BackButton,
  Backdrop,
  Badge,
  Button,
  Buttons,
  Card,
  CardContent,
  CardSubtitle,
  Content,
  Gesture,
  GestureController,
  Header,
  Icon,
  Input,
  InputShims,
  Item,
  Label,
  List,
  ListHeader,
  Loading,
  LoadingController,
  Modal,
  ModalController,
  Nav,
  RippleEffect,
  Route,
  Router,
  Scroll,
  Searchbar,
  Slide,
  Slides,
  Spinner,
  StatusTap,
  Tab,
  TabButton,
  Tabbar,
  Tabs,
  TapClick,
  Thumbnail,
  ToolbarTitle,
  Toast,
  ToastController,
  Toolbar,
  LazyImg,
  MyApp,
  QRScanner,
};
  