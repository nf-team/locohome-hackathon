/*! Built with http://stenciljs.com */
const { h } = window.App;

import { a as vanillaTextMask } from './chunk-4dc61ec6.js';

class AppLogin {
    constructor() {
        this.loading = false;
        this.phone = '';
    }
    onSubmit() {
        console.log(this);
        this.loading = true;
    }
    handleInput(e) {
        console.log(vanillaTextMask);
        console.log(this, arguments);
        this.phone = e.target.value;
    }
    attachMask() {
        console.log(this);
        // var phoneMask:any[] = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
        // vanillaTextMask({
        //   inputElement: this,
        //   mask: phoneMask
        // });
        // // Calling `vanillaTextMask.maskInput` adds event listeners to the input element. 
        // // If you need to remove those event listeners, you can call
        // maskedInputController.destroy()
    }
    componentDidLoad() {
        this.attachMask();
    }
    render() {
        const buttonText = this.loading ? h("ion-spinner", { name: "crescent", color: 'light' }) : 'Next';
        return [
            h("ion-header", null,
                h("ion-toolbar", { color: 'primary' },
                    h("ion-title", null, "SMS Verification"))),
            h("ion-content", { padding: true },
                h("p", { class: 'intro' }, "You need to enter your phone number here to use our service"),
                h("ion-list", null,
                    h("ion-item", null,
                        h("ion-input", { type: "tel", value: this.phone, onInput: this.handleInput.bind(this), onLoad: this.attachMask, placeholder: "+X (XXX) XXX-XX-XX" }))),
                h("ion-icon", { name: "qr-scanner" }),
                h("ion-button", { expand: 'block', onClick: this.onSubmit.bind(this), disabled: this.loading }, buttonText))
        ];
    }
    static get is() { return "app-login"; }
    static get properties() { return {
        "loading": {
            "state": true
        },
        "phone": {
            "state": true
        }
    }; }
}

export { AppLogin };
