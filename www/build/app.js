/*! Built with http://stenciljs.com */
(function(win, doc, namespace, fsNamespace, resourcesUrl, appCore, appCoreSsr, appCorePolyfilled, hydratedCssClass, components) {

    function init(win, doc, namespace, fsNamespace, resourcesUrl, appCore, appCorePolyfilled, hydratedCssClass, components, HTMLElementPrototype, App, x, y, scriptElm, orgComponentOnReady) {
    // create global namespace if it doesn't already exist
    App = win[namespace] = win[namespace] || {};
    App.components = components;
    y = components.filter(function (c) { return c[2]; }).map(function (c) { return c[0]; });
    if (y.length) {
        // auto hide components until they been fully hydrated
        // reusing the "x" and "i" variables from the args for funzies
        x = doc.createElement('style');
        x.innerHTML = y.join() + '{visibility:hidden}.' + hydratedCssClass + '{visibility:inherit}';
        x.setAttribute('data-styles', '');
        doc.head.insertBefore(x, doc.head.firstChild);
    }
    // create a temporary array to store the resolves
    // before the core file has fully loaded
    App.$r = [];
    // add componentOnReady to HTMLElement.prototype
    orgComponentOnReady = HTMLElementPrototype.componentOnReady;
    HTMLElementPrototype.componentOnReady = function componentOnReady(cb) {
        const elm = this;
        // there may be more than one app on the window so
        // call original HTMLElement.prototype.componentOnReady
        // if one exists already
        orgComponentOnReady && orgComponentOnReady.call(elm);
        function executor(resolve) {
            if (App.$r) {
                // core file hasn't loaded yet
                // so let's throw it in this temporary queue
                // and when the core does load it'll handle these
                App.$r.push([elm, resolve]);
            }
            else {
                // core has finished loading because there's no temporary queue
                // call the core's logic to handle this
                App.componentOnReady(elm, resolve);
            }
        }
        if (cb) {
            // just a callback
            return executor(cb);
        }
        // callback wasn't provided, let's return a promise
        if (win.Promise) {
            // use native/polyfilled promise
            return new Promise(executor);
        }
        // promise may not have been polyfilled yet
        return { then: executor };
    };
    // figure out the script element for this current script
    y = doc.querySelectorAll('script');
    for (x = y.length - 1; x >= 0; x--) {
        scriptElm = y[x];
        if (scriptElm.src || scriptElm.hasAttribute('data-resources-url')) {
            break;
        }
    }
    // get the resource path attribute on this script element
    y = scriptElm.getAttribute('data-resources-url');
    if (y) {
        // the script element has a data-resources-url attribute, always use that
        resourcesUrl = y;
    }
    if (!resourcesUrl && scriptElm.src) {
        // we don't have an exact resourcesUrl, so let's
        // figure it out relative to this script's src and app's filesystem namespace
        y = scriptElm.src.split('/').slice(0, -1);
        resourcesUrl = (y.join('/')) + (y.length ? '/' : '') + fsNamespace + '/';
    }
    // request the core this browser needs
    // test for native support of custom elements and fetch
    // if either of those are not supported, then use the core w/ polyfills
    // also check if the page was build with ssr or not
    x = doc.createElement('script');
    if (usePolyfills(win, win.location, x, 'import("")')) {
        // requires the es5/polyfilled core
        x.src = resourcesUrl + appCorePolyfilled;
    }
    else {
        // let's do this!
        x.src = resourcesUrl + appCore;
        x.setAttribute('type', 'module');
        x.setAttribute('crossorigin', true);
    }
    x.setAttribute('data-resources-url', resourcesUrl);
    x.setAttribute('data-namespace', fsNamespace);
    doc.head.appendChild(x);
}
function usePolyfills(win, location, scriptElm, dynamicImportTest) {
    // fyi, dev mode has verbose if/return statements
    // but it minifies to a nice 'lil one-liner ;)
    if (location.search.indexOf('core=esm') > 0) {
        // force esm build
        return false;
    }
    if ((location.search.indexOf('core=es5') > 0) ||
        (location.protocol === 'file:') ||
        (!(win.customElements && win.customElements.define)) ||
        (!win.fetch) ||
        (!(win.CSS && win.CSS.supports && win.CSS.supports('color', 'var(--c)'))) ||
        (!('noModule' in scriptElm))) {
        // es5 build w/ polyfills
        return true;
    }
    // final test to see if this browser support dynamic imports
    return doesNotSupportsDynamicImports(dynamicImportTest);
}
function doesNotSupportsDynamicImports(dynamicImportTest) {
    try {
        new Function(dynamicImportTest);
        return false;
    }
    catch (e) { }
    return true;
}


    init(win, doc, namespace, fsNamespace, resourcesUrl, appCore, appCoreSsr, appCorePolyfilled, hydratedCssClass, components);

    })(window, document, "App","app",0,"app.core.js","es5-build-disabled.js","hydrated",[["app-card-modal",{"ios":"app-card-modal.ios","md":"app-card-modal.md"}],["app-checkout",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},1,[["alertCtrl",4,0,0,0,"ion-alert-controller"],["canMakePayment",5],["id",1,0,1,2],["loadingCtrl",4,0,0,0,"ion-loading-controller"],["modalCtrl",4,0,0,0,"ion-modal-controller"],["product",5],["transaction",5],["transactionLoading",5]]],["app-code","app-code",0,[["loading",5],["phone",5]]],["app-details",{"ios":"app-details.ios","md":"app-details.md"},1,[["favorites",5],["id",1,0,1,2]]],["app-favs","app-favs",1,[["favCounter",5]]],["app-header",{"ios":"app-header.ios","md":"app-header.md"},1],["app-home",{"ios":"app-home.ios","md":"app-home.md"},1],["app-login","app-login",0,[["loading",5],["phone",5]]],["app-pano","app-pano",0,[["id",1,0,1,2]]],["app-profile",{"ios":"app-profile.ios","md":"app-profile.md"},1,[["name",1,0,1,2],["notify",5],["swSupport",5],["toastCtrl",4,0,0,0,"ion-toast-controller"]],0,[["ionChange","subscribeToNotify"]]],["ion-alert",{"ios":"ion-alert.ios","md":"ion-alert.md"},1,[["animationCtrl",4,0,0,0,"ion-animation-controller"],["buttons",1],["config",3,0,0,0,"config"],["cssClass",1,0,"css-class",2],["dismiss",6],["el",7],["enableBackdropDismiss",1,0,"enable-backdrop-dismiss",3],["enterAnimation",1],["header",1,0,1,2],["inputs",2],["keyboardClose",1,0,"keyboard-close",3],["leaveAnimation",1],["message",1,0,1,2],["onDidDismiss",6],["onWillDismiss",6],["overlayId",1,0,"overlay-id",4],["present",6],["subHeader",1,0,"sub-header",2],["translucent",1,0,1,3],["willAnimate",1,0,"will-animate",3]],0,[["ionAlertWillDismiss","dispatchCancelHandler"],["ionBackdropTap","onBackdropTap"]]],["ion-alert-controller",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},0,[["create",6],["dismiss",6],["doc",3,0,0,0,"document"],["getTop",6]],0,[["body:ionAlertDidUnload","alertWillDismiss"],["body:ionAlertWillDismiss","alertWillDismiss"],["body:ionAlertWillPresent","alertWillPresent"],["body:keyup.escape","escapeKeyUp"]]],["ion-animation-controller","ion-animation-controller",0,[["create",6]]],["ion-app",{"ios":"my-app.ios","md":"my-app.md"},1,[["config",3,0,0,0,"config"],["el",7],["win",3,0,0,0,"window"]]],["ion-back-button",{"ios":"app-profile.ios","md":"app-profile.md"},1,[["color",1,0,1,2],["config",3,0,0,0,"config"],["defaultHref",1,0,"default-href",2],["el",7],["icon",1,0,1,2],["mode",1,0,1,2],["text",1,0,1,2],["win",3,0,0,0,"window"]]],["ion-backdrop",{"ios":"ion-backdrop.ios","md":"ion-backdrop.md"},1,[["doc",3,0,0,0,"document"],["stopPropagation",1,0,"stop-propagation",3],["tappable",1,0,1,3],["visible",1,0,1,3]],0,[["mousedown","onMouseDown",0,0,1],["touchstart","onTouchStart",0,0,1]]],["ion-badge",{"ios":"app-header.ios","md":"app-header.md"},1,[["color",1,0,1,2],["mode",1,0,1,2]]],["ion-button",{"ios":"app-header.ios","md":"app-header.md"},1,[["buttonType",2,0,"button-type",2],["color",1,0,1,2],["disabled",1,0,1,3],["el",7],["expand",1,0,1,2],["fill",1,0,1,2],["href",1,0,1,2],["keyFocus",5],["mode",1,0,1,2],["round",1,0,1,3],["routerDirection",1,0,"router-direction",2],["size",1,0,1,2],["strong",1,0,1,3],["type",1,0,1,2],["win",3,0,0,0,"window"]]],["ion-buttons",{"ios":"ion-buttons.ios","md":"ion-buttons.md"}],["ion-card",{"ios":"app-header.ios","md":"app-header.md"},1,[["color",1,0,1,2],["mode",1,0,1,2]]],["ion-card-content",{"ios":"app-header.ios","md":"app-header.md"},1,[["color",1,0,1,2],["mode",1,0,1,2]]],["ion-card-subtitle",{"ios":"app-header.ios","md":"app-header.md"},1,[["color",1,0,1,2],["mode",1,0,1,2]]],["ion-content",{"ios":"ion-buttons.ios","md":"ion-buttons.md"},1,[["config",3,0,0,0,"config"],["el",7],["forceOverscroll",1,0,"force-overscroll",3],["fullscreen",1,0,1,3],["queue",3,0,0,0,"queue"],["scrollByPoint",6],["scrollEnabled",1,0,"scroll-enabled",3],["scrollEvents",1,0,"scroll-events",3],["scrollToBottom",6],["scrollToPoint",6],["scrollToTop",6]],0,[["body:ionNavDidChange","onNavChanged"]]],["ion-gesture",{"ios":"my-app.ios","md":"my-app.md"},0,[["attachTo",1,0,"attach-to",2],["autoBlockAll",1,0,"auto-block-all",3],["canStart",1],["direction",1,0,1,2],["disableScroll",1,0,"disable-scroll",3],["disabled",1,0,1,3],["enableListener",3,0,0,0,"enableListener"],["gestureCtrl",4,0,0,0,"ion-gesture-controller"],["gestureName",1,0,"gesture-name",2],["gesturePriority",1,0,"gesture-priority",4],["maxAngle",1,0,"max-angle",4],["notCaptured",1],["onEnd",1],["onMove",1],["onStart",1],["onWillStart",1],["passive",1,0,1,3],["queue",3,0,0,0,"queue"],["threshold",1,0,1,4]],0,[["document:mousemove","onMoveMove",1,1],["document:mouseup","onMouseUp",1,1],["mousedown","onMouseDown",1,1],["touchcancel","onTouchCancel",1,1],["touchend","onTouchCancel",1,1],["touchmove","onTouchMove",1,1],["touchstart","onTouchStart",1,1]]],["ion-gesture-controller",{"ios":"my-app.ios","md":"my-app.md"},0,[["create",6],["createBlocker",6]]],["ion-header",{"ios":"ion-buttons.ios","md":"ion-buttons.md"},1,[["translucent",1,0,1,3]]],["ion-icon","ion-icon",1,[["ariaLabel",1,0,"aria-label",2],["color",1,0,1,2],["ios",1,0,1,2],["isServer",3,0,0,0,"isServer"],["md",1,0,1,2],["mode",3,0,0,0,"mode"],["name",1,0,1,2],["publicPath",3,0,0,0,"publicPath"],["size",1,0,1,2],["svgContent",5]]],["ion-input",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},1,[["accept",1,0,1,2],["autocapitalize",1,0,1,2],["autocomplete",1,0,1,2],["autocorrect",1,0,1,2],["autofocus",1,0,1,3],["clearInput",1,0,"clear-input",3],["clearOnEdit",2,0,"clear-on-edit",3],["debounce",1,0,1,4],["disabled",1,0,1,3],["el",7],["inputmode",1,0,1,2],["max",1,0,1,2],["maxlength",1,0,1,4],["min",1,0,1,2],["minlength",1,0,1,4],["multiple",1,0,1,3],["name",1,0,1,2],["pattern",1,0,1,2],["placeholder",1,0,1,2],["readonly",1,0,1,3],["required",1,0,1,3],["results",1,0,1,4],["size",1,0,1,4],["spellcheck",1,0,1,3],["step",1,0,1,2],["type",1,0,1,2],["value",2,0,1,2]]],["ion-input-shims",{"ios":"my-app.ios","md":"my-app.md"},0,[["config",3,0,0,0,"config"],["doc",3,0,0,0,"document"]],0,[["body:ionInputDidLoad","onInputDidLoad"],["body:ionInputDidUnload","onInputDidUnload"]]],["ion-item",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},1,[["button",1,0,1,3],["color",1,0,1,2],["detail",1,0,1,3],["disabled",1,0,1,3],["el",7],["href",1,0,1,2],["mode",1,0,1,2],["routerDirection",1,0,"router-direction",2],["win",3,0,0,0,"window"]],0,[["ionStyle","itemStyle"]]],["ion-label",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},1,[["color",1,0,1,2],["el",7],["getText",6],["mode",1,0,1,2],["position",1,0,1,2]]],["ion-list",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},1,[["closeSlidingItems",6],["getOpenItem",6],["setOpenItem",6]]],["ion-list-header",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},1,[["color",1,0,1,2],["mode",1,0,1,2]]],["ion-loading",{"ios":"ion-loading.ios","md":"ion-loading.md"},1,[["animationCtrl",4,0,0,0,"ion-animation-controller"],["config",3,0,0,0,"config"],["content",1,0,1,2],["cssClass",1,0,"css-class",2],["dismiss",6],["dismissOnPageChange",1,0,"dismiss-on-page-change",3],["duration",1,0,1,4],["el",7],["enableBackdropDismiss",1,0,"enable-backdrop-dismiss",3],["enterAnimation",1],["keyboardClose",1,0,"keyboard-close",3],["leaveAnimation",1],["onDidDismiss",6],["onWillDismiss",6],["overlayId",1,0,"overlay-id",4],["present",6],["showBackdrop",1,0,"show-backdrop",3],["spinner",1,0,1,2],["translucent",1,0,1,3],["willAnimate",1,0,"will-animate",3]],0,[["ionBackdropTap","onBackdropTap"]]],["ion-loading-controller",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},0,[["create",6],["dismiss",6],["doc",3,0,0,0,"document"],["getTop",6]],0,[["body:ionLoadingDidUnload","loadingWillDismiss"],["body:ionLoadingWillDismiss","loadingWillDismiss"],["body:ionLoadingWillPresent","loadingWillPresent"],["body:keyup.escape","escapeKeyUp"]]],["ion-modal",{"ios":"ion-modal.ios","md":"ion-modal.md"},1,[["animationCtrl",4,0,0,0,"ion-animation-controller"],["color",1,0,1,2],["component",1,0,1,2],["componentProps",1],["config",3,0,0,0,"config"],["cssClass",1,0,"css-class",2],["delegate",1],["dismiss",6],["el",7],["enableBackdropDismiss",1,0,"enable-backdrop-dismiss",3],["enterAnimation",1],["keyboardClose",1,0,"keyboard-close",3],["leaveAnimation",1],["mode",1,0,1,2],["onDidDismiss",6],["onWillDismiss",6],["overlayId",1,0,"overlay-id",4],["present",6],["showBackdrop",1,0,"show-backdrop",3],["willAnimate",1,0,"will-animate",3]],0,[["ionBackdropTap","onBackdropTap"],["ionDismiss","onDismiss"],["ionModalDidDismiss","lifecycle"],["ionModalDidPresent","lifecycle"],["ionModalWillDismiss","lifecycle"],["ionModalWillPresent","lifecycle"]]],["ion-modal-controller",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},0,[["create",6],["dismiss",6],["doc",3,0,0,0,"document"],["getTop",6]],0,[["body:ionModalDidUnload","modalWillDismiss"],["body:ionModalWillDismiss","modalWillDismiss"],["body:ionModalWillPresent","modalWillPresent"],["body:keyup.escape","escapeKeyUp"]]],["ion-nav",{"ios":"my-app.ios","md":"my-app.md"},0,[["animated",2,0,1,3],["animationCtrl",4,0,0,0,"ion-animation-controller"],["canGoBack",6],["config",3,0,0,0,"config"],["delegate",1],["el",7],["getActive",6],["getByIndex",6],["getPrevious",6],["getRouteId",6],["insert",6],["insertPages",6],["length",6],["pop",6],["popTo",6],["popToRoot",6],["push",6],["queue",3,0,0,0,"queue"],["removeIndex",6],["root",1,0,1,2],["rootParams",1],["setPages",6],["setRoot",6],["setRouteId",6],["swipeBackEnabled",2,0,"swipe-back-enabled",3],["win",3,0,0,0,"window"]]],["ion-ripple-effect",{"ios":"ion-backdrop.ios","md":"ion-backdrop.md"},1,[["addRipple",6],["doc",3,0,0,0,"document"],["el",7],["enableListener",3,0,0,0,"enableListener"],["queue",3,0,0,0,"queue"],["tapClick",1,0,"tap-click",3]],0,[["mousedown","mouseDown",1,1],["parent:ionActivated","ionActivated",1],["touchstart","touchStart",1,1]]],["ion-route",{"ios":"my-app.ios","md":"my-app.md"},0,[["component",1,0,1,2],["componentProps",1],["url",1,0,1,2]]],["ion-router",{"ios":"my-app.ios","md":"my-app.md"},0,[["base",1,0,1,2],["config",3,0,0,0,"config"],["el",7],["navChanged",6],["push",6],["queue",3,0,0,0,"queue"],["useHash",1,0,"use-hash",3],["win",3,0,0,0,"window"]],0,[["ionRouteDataChanged","onRoutesChanged"],["ionRouteRedirectChanged","onRedirectChanged"],["window:popstate","onPopState"]]],["ion-scroll",{"ios":"ion-scroll.ios","md":"ion-scroll.md"},1,[["config",3,0,0,0,"config"],["el",7],["forceOverscroll",2,0,"force-overscroll",3],["mode",1,0,1,2],["queue",3,0,0,0,"queue"],["scrollByPoint",6],["scrollEvents",1,0,"scroll-events",3],["scrollToBottom",6],["scrollToPoint",6],["scrollToTop",6],["win",3,0,0,0,"window"]],0,[["scroll","onScroll",0,1]]],["ion-searchbar",{"ios":"app-home.ios","md":"app-home.md"},1,[["activated",5],["animated",1,0,1,3],["autocomplete",1,0,1,2],["autocorrect",1,0,1,2],["cancelButtonText",1,0,"cancel-button-text",2],["color",1,0,1,2],["debounce",1,0,1,4],["doc",3,0,0,0,"document"],["el",7],["focused",5],["mode",1,0,1,2],["placeholder",1,0,1,2],["showCancelButton",1,0,"show-cancel-button",3],["spellcheck",1,0,1,3],["type",1,0,1,2],["value",2,0,1,2]]],["ion-slide",{"ios":"app-details.ios","md":"app-details.md"},1],["ion-slides",{"ios":"app-details.ios","md":"app-details.md"},1,[["el",7],["getActiveIndex",6],["getPreviousIndex",6],["isBeginning",6],["isEnd",6],["length",6],["lockSwipeToNext",6],["lockSwipeToPrev",6],["lockSwipes",6],["options",1,0,1,1],["pager",1,0,1,3],["slideNext",6],["slidePrev",6],["slideTo",6],["startAutoplay",6],["stopAutoplay",6],["update",6]]],["ion-spinner",{"ios":"ion-spinner.ios","md":"ion-spinner.md"},1,[["color",1,0,1,2],["config",3,0,0,0,"config"],["duration",1,0,1,4],["mode",1,0,1,2],["name",1,0,1,2],["paused",1,0,1,3]]],["ion-status-tap",{"ios":"my-app.ios","md":"my-app.md"},0,[["duration",1,0,1,4],["queue",3,0,0,0,"queue"],["win",3,0,0,0,"window"]],0,[["window:statusTap","onStatusTap"]]],["ion-tab","ion-tab",0,[["active",2,0,1,3],["badge",1,0,1,2],["badgeStyle",1,0,"badge-style",2],["btnId",1,0,"btn-id",2],["component",1,0,1,2],["delegate",1],["disabled",1,0,1,3],["el",7],["getTabId",6],["href",1,0,1,2],["icon",1,0,1,2],["label",1,0,1,2],["name",1,0,1,2],["selected",2,0,1,3],["setActive",6],["show",1,0,1,3],["tabsHideOnSubPages",1,0,"tabs-hide-on-sub-pages",3]]],["ion-tab-button",{"ios":"app-header.ios","md":"app-header.md"},1,[["el",7],["keyFocus",5],["selected",1,0,1,3],["tab",1]],0,[["click","onClick"]]],["ion-tabbar",{"ios":"app-header.ios","md":"app-header.md"},1,[["canScrollLeft",5],["canScrollRight",5],["doc",3,0,0,0,"document"],["el",7],["hidden",5],["highlight",1,0,1,3],["layout",1,0,1,2],["placement",1,0,1,2],["queue",3,0,0,0,"queue"],["scrollable",1,0,1,3],["selectedTab",1],["tabs",1],["translucent",1,0,1,3]],0,[["body:keyboardWillHide","onKeyboardWillHide"],["body:keyboardWillShow","onKeyboardWillShow"],["ionTabButtonDidLoad","onTabButtonLoad"],["ionTabButtonDidUnload","onTabButtonLoad"],["window:resize","onResize",0,1]]],["ion-tabs",{"ios":"app-header.ios","md":"app-header.md"},1,[["color",1,0,1,2],["config",3,0,0,0,"config"],["doc",3,0,0,0,"document"],["el",7],["getRouteId",6],["getSelected",6],["getTab",6],["name",1,0,1,2],["scrollable",1,0,1,3],["select",6],["selectedTab",5],["setRouteId",6],["tabbarHidden",1,0,"tabbar-hidden",3],["tabbarHighlight",2,0,"tabbar-highlight",3],["tabbarLayout",2,0,"tabbar-layout",2],["tabbarPlacement",2,0,"tabbar-placement",2],["tabs",5],["translucent",1,0,1,3],["useRouter",2,0,"use-router",3]],0,[["ionTabbarClick","tabChange"]]],["ion-tap-click",{"ios":"my-app.ios","md":"my-app.md"},0,[["el",7],["enableListener",3,0,0,0,"enableListener"],["isServer",3,0,0,0,"isServer"]],0,[["body:click","onBodyClick",0,0,1],["body:ionGestureCaptured","cancelActive"],["body:ionScrollStart","cancelActive"],["document:mousedown","onMouseDown",0,1,1],["document:mouseup","onMouseUp",0,0,1],["document:touchcancel","onTouchEnd",0,0,1],["document:touchend","onTouchEnd",0,0,1],["document:touchstart","onTouchStart",0,1,1]]],["ion-thumbnail",{"ios":"app-card-modal.ios","md":"app-card-modal.md"},1],["ion-title",{"ios":"ion-buttons.ios","md":"ion-buttons.md"},1],["ion-toast",{"ios":"ion-toast.ios","md":"ion-toast.md"},1,[["animationCtrl",4,0,0,0,"ion-animation-controller"],["closeButtonText",1,0,"close-button-text",2],["config",3,0,0,0,"config"],["cssClass",1,0,"css-class",2],["dismiss",6],["duration",1,0,1,4],["el",7],["enterAnimation",1],["keyboardClose",1,0,"keyboard-close",3],["leaveAnimation",1],["message",1,0,1,2],["onDidDismiss",6],["onWillDismiss",6],["overlayId",1,0,"overlay-id",4],["position",1,0,1,2],["present",6],["showCloseButton",1,0,"show-close-button",3],["translucent",1,0,1,3],["willAnimate",1,0,"will-animate",3]],0,[["ionDismiss","onDismiss"]]],["ion-toast-controller",{"ios":"ion-buttons.ios","md":"ion-buttons.md"},0,[["create",6],["dismiss",6],["doc",3,0,0,0,"document"],["getTop",6]],0,[["body:ionToastDidUnload","toastWillDismiss"],["body:ionToastWillDismiss","toastWillDismiss"],["body:ionToastWillPresent","toastWillPresent"],["body:keyup.escape","escapeKeyUp"]]],["ion-toolbar",{"ios":"ion-buttons.ios","md":"ion-buttons.md"},1,[["color",1,0,1,2],["config",3,0,0,0,"config"],["mode",1,0,1,2],["translucent",1,0,1,3]]],["lazy-img","lazy-img",1,[["alt",1,0,1,2],["el",7],["isServer",3,0,0,0,"isServer"],["oldSrc",5],["src",1,0,1,2],["width",1,0,1,4]]],["my-app",{"ios":"my-app.ios","md":"my-app.md"},1,[["toastCtrl",4,0,0,0,"ion-toast-controller"]],0,[["window:swUpdate","onSWUpdate"]]],["qr-scanner",{"ios":"app-profile.ios","md":"app-profile.md"},1]],HTMLElement.prototype);