const resolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');
const sass = require('@stencil/sass');
const replace = require('rollup-plugin-replace');

exports.config = {
  nodeResolve: {
    browser: true
  },
  outputTargets: [
    {
      type: 'www',
      serviceWorker: {
        swSrc: 'src/sw.js'
      }
    }
  ],
  empty: false,
  globalStyle: 'src/global/app.css',
  plugins: [
    sass(),
    resolve({
      browser: true
    })
    // commonjs()
  ],
  collections: ['ionicons']
};

exports.devServer = {
  root: 'www',
  watchGlob: '**/**'
};
